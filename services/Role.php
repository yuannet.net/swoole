<?php

namespace app\services;

class Role extends \app\models\Role
{
    public static function getStatus()
    {
        $status = [
            10 => '正常',
            20 => '停用',
        ];
        return $status;
    }
}
