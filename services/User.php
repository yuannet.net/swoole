<?php

namespace app\services;

use Yii;
use yii\helpers\ArrayHelper;

class User extends \app\models\User implements \yii\web\IdentityInterface
{
    public static function getStatus()
    {
        $status = [
            10 => '正常',
            20 => '停用',
        ];
        return $status;
    }

    /**
     * 关联产品用户表
     * @method getProductUser
     * @author ZhangYuan 574368565@qq.com
     * @time 2021/1/26 13:57
     */
    public function getProductUser()
    {
        return $this->hasMany(ProductUser::class, ['user_id' => 'id'])->where(['product_user.status' => 10]);
    }

    /**
     * 返回realname/username
     * @method getName
     * @return mixed|string|null
     * @author ZhangYuan 574368565@qq.com
     * @time 2021/1/27 13:29
     */
    public function getName()
    {
        return $this->realname ? $this->realname : $this->username;
    }

    /**
     * 关联获取作为成员的产品ID
     * @method getSelfProductId
     * @return array|\yii\db\ActiveRecord[]
     * @author ZhangYuan 574368565@qq.com
     * @time 2021/1/26 15:24
     */
    public function getSelfProductIdArray()
    {
        $where[] = 'and';
        $where[] = ['product_user.user_id' => $this->id];
        $where[] = ['product_user.status' => 10];
        $where[] = ['product.status' => 10];

        $product_list = ProductUser::find()->joinWith(['product'])->where($where)->all();
        $product_array = ArrayHelper::getColumn(ArrayHelper::toArray($product_list), 'product_id');
        return $product_array;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => [10, 20]]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['accessToken' => $token, 'status' => [10, 20]]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        $user = User::find()
            ->where(['username' => $username, 'status' => [10, 20]])
            ->asArray()
            ->one();

        if ($user) {
            return new static($user);
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates access token
     */
    public function setAccessToken()
    {
        $this->accessToken = Yii::$app->security->generateRandomString();
    }
}
