<?php

namespace app\services;

use Yii;
use yii\helpers\ArrayHelper;

class Product extends \app\models\Product
{

    public static function getStatus()
    {
        $status = [
            10 => '正常',
            20 => '停用',
        ];
        return $status;
    }

    /**
     * 获取所有产品
     * @method getAllProduct
     * @return Product[]
     * @author ZhangYuan 574368565@qq.com
     * @time 2021/1/26 16:43
     */
    public static function getAllProduct()
    {
        return self::findAll(['status' => [10, 20]]);
    }

    /**
     * 获取当前登录用户可访问的产品ID列表，超管获取所有产品，否则只获取作为成员的产品
     * 只有主页展板全部产品计数和产品列表检索所有产品，否则不检索停用的（status=10正常，status=20停用）
     * @method authProductId
     * @param int[] $status
     * @return array
     * @author ZhangYuan 574368565@qq.com
     * @time 2021/1/26 16:38
     */
    public static function authProductId($status = 10)
    {
        $is_super = \Yii::$app->user->identity->is_super ? true : false;
        if ($is_super) {
            $product_list = Product::findAll(['status' => $status]);
            $product_array = ArrayHelper::getColumn(ArrayHelper::toArray($product_list), 'id');
            return $product_array;
        } else {
            $where[] = 'and';
            $where[] = ['product_user.user_id' => Yii::$app->user->identity->id];
            $where[] = ['product_user.status' => 10];
            $where[] = ['product.status' => $status];

            $product_list = ProductUser::find()->joinWith(['product'])->where($where)->all();
            $product_array = ArrayHelper::getColumn(ArrayHelper::toArray($product_list), 'product_id');
            return $product_array;
        }
    }

    /**
     * 获取登录所有具有访问权限的产品统计信息
     * @method productStatistics
     * @author ZhangYuan 574368565@qq.com
     * @time 2021/1/26 16:45
     */
    public static function productStatistics($show_all = true)
    {
        $product_id_list = self::authProductId();

        $result = self::findAll(['id' => $product_id_list]);
        $return = ArrayHelper::toArray($result);

        foreach ($result as $key => $val) {
            $return[$key]['bug'] = [
                'total' => Bug::find()->where(['product_id' => $val['id'], 'status' => 10, 'step' => [0, 1, 2]])->count(), //未完成总数
                'step0' => Bug::find()->where(['product_id' => $val['id'], 'status' => 10, 'step' => 0])->count(), //待指派
                'step1' => Bug::find()->where(['product_id' => $val['id'], 'status' => 10, 'step' => 1])->count(), //待确认
                'step2' => Bug::find()->where(['product_id' => $val['id'], 'status' => 10, 'step' => 2])->count(), //开发中
                'step3' => Bug::find()->where(['product_id' => $val['id'], 'status' => 10, 'step' => 3])->count(), //已完成
            ];
            $return[$key]['demand'] = [
                'total' => Demand::find()->where(['product_id' => $val['id'], 'status' => 10, 'step' => [0, 1, 2]])->count(), //未完成总数
                'step0' => Demand::find()->where(['product_id' => $val['id'], 'status' => 10, 'step' => 0])->count(), //待指派
                'step1' => Demand::find()->where(['product_id' => $val['id'], 'status' => 10, 'step' => 1])->count(), //待确认
                'step2' => Demand::find()->where(['product_id' => $val['id'], 'status' => 10, 'step' => 2])->count(), //开发中
                'step3' => Demand::find()->where(['product_id' => $val['id'], 'status' => 10, 'step' => 3])->count(), //已完成
            ];
        }

        if ($show_all) {
            $return_all[] = [
                'id' => 0,
                'title' => '所有产品',
                'bug' => [
                    'total' => Bug::find()->where(['product_id' => $product_id_list, 'status' => 10, 'step' => [0, 1, 2]])->count(), //未完成总数
                    'step0' => Bug::find()->where(['product_id' => $product_id_list, 'status' => 10, 'step' => 0])->count(), //待指派
                    'step1' => Bug::find()->where(['product_id' => $product_id_list, 'status' => 10, 'step' => 1])->count(), //待确认
                    'step2' => Bug::find()->where(['product_id' => $product_id_list, 'status' => 10, 'step' => 2])->count(), //开发中
                    'step3' => Bug::find()->where(['product_id' => $product_id_list, 'status' => 10, 'step' => 3])->count(), //已完成
                ],
                'demand' => [
                    'total' => Demand::find()->where(['product_id' => $product_id_list, 'status' => 10, 'step' => [0, 1, 2]])->count(), //未完成总数
                    'step0' => Demand::find()->where(['product_id' => $product_id_list, 'status' => 10, 'step' => 0])->count(), //待指派
                    'step1' => Demand::find()->where(['product_id' => $product_id_list, 'status' => 10, 'step' => 1])->count(), //待确认
                    'step2' => Demand::find()->where(['product_id' => $product_id_list, 'status' => 10, 'step' => 2])->count(), //开发中
                    'step3' => Demand::find()->where(['product_id' => $product_id_list, 'status' => 10, 'step' => 3])->count(), //已完成
                ]
            ];
            $return = ArrayHelper::merge($return_all, $return);
        }

        return $return;
    }

    /**
     * 关联产品用户表
     * @method getProductUser
     * @author ZhangYuan 574368565@qq.com
     * @time 2021/1/26 13:57
     */
    public function getProductUser()
    {
        return $this->hasMany(ProductUser::class, ['product_id' => 'id'])->where(['product_user.status' => 10]);
    }
}
