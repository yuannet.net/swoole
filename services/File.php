<?php

namespace app\services;


class File extends \app\models\File
{
    public static function batchSave($file, $module, $data_id)
    {
        foreach ($file as $item) {
            $model = new self();
            $model->load($item, '');
            $model->module = $module;
            $model->data_id = $data_id;
            $model->save();
        }
        return true;
    }

    public static function batchRemove($remove)
    {
        self::updateAll(['status' => 0], ['id' => $remove]);
        return true;
    }
}
