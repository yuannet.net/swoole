<?php

namespace app\services;

use app\models\BaseModel;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * LoginForm is the model behind the login form.
 *
 * @property-read User|null $user This property is read-only.
 *
 */
class LoginForm extends BaseModel
{
    public $username;
    public $password;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, '用户名或密码错误');
            } elseif ($user->status == 20) {
                $this->addError($attribute, '该用户已被停用');

            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            $user = $this->getUser();

            $role = $user->role_id ? Role::findAll(['id' => explode(',', $user->role_id), 'status' => 10]) : [];
            $perm = ArrayHelper::getColumn($role, 'perm');
            $perm = implode(',', $perm);
            $perm = explode(',', $perm);
            Yii::$app->session->set('perm', $perm);

            User::updateAll(['accessToken' => Yii::$app->security->generateRandomString()], ['id' => $user->id]);

            return Yii::$app->user->login($user);
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }
}
