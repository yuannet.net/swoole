<?php

namespace app\services;


class ProductUser extends \app\models\ProductUser
{
    /**
     * 验证指派者是否为该产品项目成员
     * @method checkProductUser
     * @author ZhangYuan 574368565@qq.com
     * @time 2021/2/22 14:43
     */
    public static function checkProductUser($user_id, $product_id)
    {
        $user = User::findIdentity($user_id);
        if ($user && $user->is_super) {
            return true;
        }

        $product_auth = self::find()->where(['product_id' => $product_id, 'user_id' => $user_id, 'status' => 10])->one();
        if ($product_auth) {
            return true;
        }

        return false;
    }


    /**
     * 关联正常产品
     * @method getProduct
     * @return \yii\db\ActiveQuery
     * @author ZhangYuan 574368565@qq.com
     * @time 2021/1/26 14:00
     */
    public function getProduct()
    {
        return $this->hasOne(Product::class, ['id' => 'product_id'])->where(['product.status' => [10]]);
    }

    /**
     * 关联所有用户
     * @method getUser
     * @return \yii\db\ActiveQuery
     * @author ZhangYuan 574368565@qq.com
     * @time 2021/1/26 14:00
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id'])->where(['user.status' => [10, 20]]);
    }
}
