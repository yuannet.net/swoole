<?php

namespace app\services;


class BugRecord extends \app\models\BugRecord
{
    public function getCreateUser()
    {
        return $this->hasOne(User::class, ['id' => 'created_by'])->where(['user.status' => [10, 20]]);
    }

    public function getBug()
    {
        return $this->hasOne(Bug::class, ['id' => 'bug_id'])->where(['bug.status' => 10]);
    }
}
