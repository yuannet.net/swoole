<?php

namespace app\services;


use app\models\BaseModel;
use yii\helpers\Url;

class Menu extends BaseModel
{
    /**
     * 菜单数组拼写规范
     * @title 必填
     * @subtitle 预留字段不必填
     * @url 必填，可以为空，为空不生成菜单地址，可作为父级菜单
     * @badge 选填，徽章，可用于ajax拉取提示如数量等
     * @badge_desc 选填，徽章描述
     * @child 选填，子级菜单选项，字段如上
     * @method getMenu
     * @return array[]
     * @author ZhangYuan 574368565@qq.com
     * @time 2021/2/3 10:20
     */
    public static function getMenu()
    {
        $menu = [
            [
                'title' => '首页',
                'subtitle' => '首页',
                'url' => Url::to(['/site/main'])
            ],
            [
                'title' => 'Pchat',
                'subtitle' => 'Pchat',
                'url' => Url::to(['/common/swoole/socket'])
            ],
            [
                'title' => '产品',
                'subtitle' => '产品',
                'url' => Url::to(['/product/product/index'])
            ],
            [
                'title' => '需求',
                'subtitle' => '需求',
                'url' => Url::to(['/product/demand/list', 'search_type' => 4]),
                'badge' => Url::to(['/common/badge/demand-count']),
                'badge_desc' => '待确认需求数',
            ],
            [
                'title' => 'Bug',
                'subtitle' => 'Bug',
                'url' => Url::to(['/test/bug/list', 'search_type' => 4]),
                'badge' => Url::to(['/common/badge/bug-count']),
                'badge_desc' => '待确认Bug数',
            ],
            [
                'title' => 'M-Zone',
                'subtitle' => 'M-Zone',
                'url' => '',
                'child' => [
                    [
                        'title' => '备忘录',
                        'subtitle' => '备忘录',
                        'url' => Url::to(['/mzone/memo/list'])
                    ],
                    [
                        'title' => '最新动态',
                        'subtitle' => '最新动态',
                        'url' => Url::to(['/mzone/recent-news/list'])
                    ]
                ]
            ],
            [
                'title' => '系统',
                'subtitle' => '系统',
                'url' => '',
                'child' => [
                    [
                        'title' => '人员',
                        'subtitle' => '人员',
                        'url' => Url::to(['/system/user/list'])
                    ],
                    [
                        'title' => '角色',
                        'subtitle' => '角色',
                        'url' => Url::to(['/system/role/list'])
                    ],
                    [
                        'title' => '操作日志',
                        'subtitle' => '操作日志',
                        'url' => Url::to(['/system/op/list'])
                    ]
                ]
            ],
        ];
        return $menu;
    }
}
