<?php

namespace app\services;


class Memo extends \app\models\Memo
{
    public function getLevelColor()
    {
        switch ($this->level) {
            case 1:
                $class = 'layui-btn-danger';
                break;
            case 2:
                $class = 'layui-btn-warm';
                break;
            case 3:
                $class = 'layui-btn-normal';
                break;
            case 4:
                $class = '';
                break;
        }
        return $class;
    }

    public function canEdit()
    {
        if ($this->created_by == \Yii::$app->user->identity->id) {
            return true;
        }
        return false;
    }

    public function canDel()
    {
        if ($this->created_by == \Yii::$app->user->identity->id) {
            return true;
        }
        return false;
    }

    public function getCreateUser()
    {
        return $this->hasOne(User::class, ['id' => 'created_by'])->where(['user.status' => [10, 20]]);

    }

    public function getFile()
    {
        return $this->hasMany(File::class, ['data_id' => 'id'])->where(['file.model' => 'memo', 'status' => 10]);
    }

}
