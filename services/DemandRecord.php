<?php

namespace app\services;


class DemandRecord extends \app\models\DemandRecord
{
    public function getCreateUser()
    {
        return $this->hasOne(User::class, ['id' => 'created_by'])->where(['user.status' => [10, 20]]);
    }

    public function getDemand()
    {
        return $this->hasOne(Demand::class, ['id' => 'demand_id'])->where(['demand.status' => 10]);
    }
}
