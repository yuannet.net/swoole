<?php

namespace app\services;


use app\models\BaseModel;

class Perm extends BaseModel
{
    public static function getPerm()
    {
        $self = new self();
        $perm = [
            'product' => $self->product(), //产品、需求权限
            'test' => $self->test(), //bug权限
            'mzone' => $self->mzone(), //bug权限
            'system' => $self->system(), //系统权限
        ];
        return $perm;
    }

    private function product()
    {
        return [
            'text' => '产品',
            'product' => ['text' => '产品管理', 'index,list,view' => '产品主页/列表/详情', 'create,update' => '产品创建/修改', 'delete' => '产品删除'],
            'product-user' => ['text' => '产品成员管理', 'list,view' => '成员列表/详情', 'create' => '成员新增', 'delete' => '成员移除'],
            'demand' => ['text' => '需求管理', 'list,view' => '需求列表/详情', 'create,update' => '需求创建/修改', 'delete' => '需求删除', 'set-develop-user' => '需求指派', 'switch-step' => '需求确认/完成'],
            'demand-record' => ['text' => '需求备注', 'create,update' => '备注创建/修改', 'delete' => '备注删除']
        ];
    }

    private function test()
    {
        return [
            'text' => '测试',
            'bug' => ['text' => 'BUG管理', 'list,view' => 'BUG列表/详情', 'create,update' => 'BUG创建/修改', 'delete' => 'BUG删除', 'set-develop-user' => 'BUG指派', 'switch-step' => 'BUG确认/完成'],
            'bug-record' => ['text' => 'BUG备注', 'create,update' => '备注创建/修改', 'delete' => '备注删除']
        ];
    }

    protected function mzone()
    {
        return [
            'text' => 'M-Zone',
            'memo' => ['text' => '备忘录', 'list,view' => '备忘录列表/详情', 'create,update' => '备忘录创建/修改', 'delete' => '备忘录删除'],
            'recent-news' => ['text' => '最新动态', 'list,view' => '最新动态列表']
        ];
    }

    protected function system()
    {
        return [
            'text' => '系统',
            'user' => ['text' => '人员管理', 'list,view' => '人员列表/详情', 'create,update' => '人员创建/修改', 'delete' => '人员删除'],
            'role' => ['text' => '角色管理', 'list,view' => '角色列表/详情', 'create,update' => '角色创建/修改', 'delete' => '角色删除'],
            'op' => ['text' => '操作日志', 'list,view' => '日志列表/详情']
        ];
    }
}
