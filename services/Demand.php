<?php

namespace app\services;


class Demand extends \app\models\Demand
{
    public static function getStep()
    {
        $step = [
            0 => '待指派',
            1 => '待确认',
            2 => '开发中',
            3 => '已完成',
        ];
        return $step;
    }

    public function getStepColor()
    {
        switch ($this->step) {
            case 0:
                $class = 'layui-btn-danger';
                break;
            case 1:
                $class = 'layui-btn-warm';
                break;
            case 2:
                $class = 'layui-btn-normal';
                break;
            case 3:
                $class = '';
                break;
        }
        return $class;
    }

    public function getLevelColor()
    {
        switch ($this->level) {
            case 1:
                $class = 'layui-btn-danger';
                break;
            case 2:
                $class = 'layui-btn-warm';
                break;
            case 3:
                $class = 'layui-btn-normal';
                break;
            case 4:
                $class = '';
                break;
        }
        return $class;
    }

    /**
     * 验证指派人是否为当前需求的项目成员
     * @method validateProductUser
     * @author ZhangYuan 574368565@qq.com
     * @time 2021/2/22 14:54
     */
    public function validateProductUser($attribute, $params)
    {
        if ($this->develop_user_id && !ProductUser::checkProductUser($this->develop_user_id, $this->product_id)) {
            $this->addError($attribute, '被指派用户不是本产品的项目成员');
        }
    }

    /**
     * 发送指派邮件
     * @method sendEmail
     * @return bool
     * @author ZhangYuan 574368565@qq.com
     * @time 2021/2/22 14:43
     */
    public function sendEmail()
    {
        $user_email = $this->developUser && $this->developUser->email ? $this->developUser->email : '';
        $product_name = $this->product->title;
        if ($user_email) {
            $result = \Yii::$app->mailer->compose()->setTo($user_email)->setSubject("需求确认")->setTextBody("项目[{$product_name}]指派给您一条新需求[{$this->title}]，请您尽快确认")->send();
            return $result;
        }
        return false;
    }

    /**
     * 需求只能由发起人删除修改，需求已完成后不能删除修改
     * @method canEdit
     * @return bool
     * @author ZhangYuan 574368565@qq.com
     * @time 2021/2/18 14:24
     */
    public function canEdit()
    {
        if ($this->created_by == \Yii::$app->user->identity->id && $this->step != 3) {
            return true;
        }
        return false;
    }

    /**
     * 需求只能由发起人删除修改，需求已完成后不能删除修改
     * @method canDel
     * @return bool
     * @author ZhangYuan 574368565@qq.com
     * @time 2021/2/18 14:24
     */
    public function canDel()
    {
        if ($this->created_by == \Yii::$app->user->identity->id && $this->step != 3) {
            return true;
        }
        return false;
    }

    public function getCreateUser()
    {
        return $this->hasOne(User::class, ['id' => 'created_by'])->where(['user.status' => [10, 20]]);

    }

    public function getDevelopUser()
    {
        return $this->hasOne(User::class, ['id' => 'develop_user_id'])->where(['user.status' => [10, 20]]);
    }

    public function getProduct()
    {
        return $this->hasOne(Product::class, ['id' => 'product_id'])->where(['product.status' => 10]);
    }

    public function getRecord()
    {
        return $this->hasMany(DemandRecord::class, ['demand_id' => 'id'])->where(['demand_record.status' => 10]);
    }

    public function getFile()
    {
        return $this->hasMany(File::class, ['data_id' => 'id'])->where(['file.model' => 'demand', 'status' => 10]);
    }
}
