<?php

namespace app\services;

use yii\helpers\Json;

class OperationLog extends \app\models\OperationLog
{
    public function create($controller, $remark = '', $url = '', $is_show = false)
    {
        $this->module = $controller->module->id;
        $this->controller = $controller->id;
        $this->action = $controller->action->id;
        $this->orgin = is_array($this->orgin) ? Json::encode($this->orgin, 256) : $this->orgin;
        $this->data = is_array($this->data) ? Json::encode($this->data, 256) : $this->data;
        $this->change = is_array($this->change) ? Json::encode($this->change, 256) : $this->change;
        $this->remark = $remark;
        $this->url = $url;
        $this->is_show = $is_show ? 1 : 0;

        //储存镜像防止占用
        $clone_this = clone $this;
        $clone_this->save();
    }

    public function getCreateUser()
    {
        return $this->hasOne(User::class, ['id' => 'created_by'])->where(['user.status' => [10, 20]]);
    }

    public function getChangeInfo()
    {
        return $this->change ? Json::decode($this->change) : [];
    }

    public function getOrginInfo()
    {
        return $this->orgin ? Json::decode($this->orgin) : [];
    }

    public function getDataInfo()
    {
        return $this->data ? Json::decode($this->data) : [];
    }
}