<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'src/layui/css/layui.css',
        'src/style/admin.css',
        'src/style/iconfont.css',
    ];
    public $js = [
        'src/layui/layui.all.js',
        'src/common.js',
    ];
    public $depends = [
    ];
}
