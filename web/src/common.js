var $ = layui.$;
$.fn.loadForm = function (modelData) {
    if (Object.prototype.toString.call(modelData) === '[object String]') {
        modelData = eval('(' + modelData + ')');
    }
    var form = $(this);
    for (var params in modelData) {
        var value = modelData[params];
        var cc = form.find('input[name="' + params + '"][type=radio], input[name="' + params + '"][type=checkbox]');
        var file = form.find('input[name="' + params + '"][type=file]');
        if (file.length) {
            continue;
        }
        if (cc.length) {
            cc.each(function () {
                if (isChecked($(this).val(), value)) {
                    $(this).attr('checked', true);
                }
            });
        } else {
            form.find('input[name="' + params + '"]').val(value);
            form.find('textarea[name="' + params + '"]').val(value);
            form.find('select[name="' + params + '"]').val(value);
        }
    }

    function isChecked(val, value) {
        if (val == String(value) || $.inArray(val, $.isArray(value) ? value : [value]) >= 0) {
            return true;
        } else {
            return false;
        }
    }
}