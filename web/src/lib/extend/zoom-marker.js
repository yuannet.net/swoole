layui.define(["jquery"], function (exports) {
    var jQuery = layui.jquery;
    var hammer = (function ($) {
        /*! Hammer.JS - v2.0.8 - 2016-04-23
         * http://hammerjs.github.io/
         *
         * Copyright (c) 2016 Jorik Tangelder;
         * Licensed under the MIT license */
        !function (a, b, c, d) {
            "use strict";

            function e(a, b, c) {
                return setTimeout(j(a, c), b)
            }

            function f(a, b, c) {
                return Array.isArray(a) ? (g(a, c[b], c), !0) : !1
            }

            function g(a, b, c) {
                var e;
                if (a) if (a.forEach) a.forEach(b, c); else if (a.length !== d) for (e = 0; e < a.length;) b.call(c, a[e], e, a), e++; else for (e in a) a.hasOwnProperty(e) && b.call(c, a[e], e, a)
            }

            function h(b, c, d) {
                var e = "DEPRECATED METHOD: " + c + "\n" + d + " AT \n";
                return function () {
                    var c = new Error("get-stack-trace"),
                        d = c && c.stack ? c.stack.replace(/^[^\(]+?[\n$]/gm, "").replace(/^\s+at\s+/gm, "").replace(/^Object.<anonymous>\s*\(/gm, "{anonymous}()@") : "Unknown Stack Trace",
                        f = a.console && (a.console.warn || a.console.log);
                    return f && f.call(a.console, e, d), b.apply(this, arguments)
                }
            }

            function i(a, b, c) {
                var d, e = b.prototype;
                d = a.prototype = Object.create(e), d.constructor = a, d._super = e, c && la(d, c)
            }

            function j(a, b) {
                return function () {
                    return a.apply(b, arguments)
                }
            }

            function k(a, b) {
                return typeof a == oa ? a.apply(b ? b[0] || d : d, b) : a
            }

            function l(a, b) {
                return a === d ? b : a
            }

            function m(a, b, c) {
                g(q(b), function (b) {
                    a.addEventListener(b, c, !1)
                })
            }

            function n(a, b, c) {
                g(q(b), function (b) {
                    a.removeEventListener(b, c, !1)
                })
            }

            function o(a, b) {
                for (; a;) {
                    if (a == b) return !0;
                    a = a.parentNode
                }
                return !1
            }

            function p(a, b) {
                return a.indexOf(b) > -1
            }

            function q(a) {
                return a.trim().split(/\s+/g)
            }

            function r(a, b, c) {
                if (a.indexOf && !c) return a.indexOf(b);
                for (var d = 0; d < a.length;) {
                    if (c && a[d][c] == b || !c && a[d] === b) return d;
                    d++
                }
                return -1
            }

            function s(a) {
                return Array.prototype.slice.call(a, 0)
            }

            function t(a, b, c) {
                for (var d = [], e = [], f = 0; f < a.length;) {
                    var g = b ? a[f][b] : a[f];
                    r(e, g) < 0 && d.push(a[f]), e[f] = g, f++
                }
                return c && (d = b ? d.sort(function (a, c) {
                    return a[b] > c[b]
                }) : d.sort()), d
            }

            function u(a, b) {
                for (var c, e, f = b[0].toUpperCase() + b.slice(1), g = 0; g < ma.length;) {
                    if (c = ma[g], e = c ? c + f : b, e in a) return e;
                    g++
                }
                return d
            }

            function v() {
                return ua++
            }

            function w(b) {
                var c = b.ownerDocument || b;
                return c.defaultView || c.parentWindow || a
            }

            function x(a, b) {
                var c = this;
                this.manager = a, this.callback = b, this.element = a.element, this.target = a.options.inputTarget, this.domHandler = function (b) {
                    k(a.options.enable, [a]) && c.handler(b)
                }, this.init()
            }

            function y(a) {
                var b, c = a.options.inputClass;
                return new (b = c ? c : xa ? M : ya ? P : wa ? R : L)(a, z)
            }

            function z(a, b, c) {
                var d = c.pointers.length, e = c.changedPointers.length, f = b & Ea && d - e === 0,
                    g = b & (Ga | Ha) && d - e === 0;
                c.isFirst = !!f, c.isFinal = !!g, f && (a.session = {}), c.eventType = b, A(a, c), a.emit("hammer.input", c), a.recognize(c), a.session.prevInput = c
            }

            function A(a, b) {
                var c = a.session, d = b.pointers, e = d.length;
                c.firstInput || (c.firstInput = D(b)), e > 1 && !c.firstMultiple ? c.firstMultiple = D(b) : 1 === e && (c.firstMultiple = !1);
                var f = c.firstInput, g = c.firstMultiple, h = g ? g.center : f.center, i = b.center = E(d);
                b.timeStamp = ra(), b.deltaTime = b.timeStamp - f.timeStamp, b.angle = I(h, i), b.distance = H(h, i), B(c, b), b.offsetDirection = G(b.deltaX, b.deltaY);
                var j = F(b.deltaTime, b.deltaX, b.deltaY);
                b.overallVelocityX = j.x, b.overallVelocityY = j.y, b.overallVelocity = qa(j.x) > qa(j.y) ? j.x : j.y, b.scale = g ? K(g.pointers, d) : 1, b.rotation = g ? J(g.pointers, d) : 0, b.maxPointers = c.prevInput ? b.pointers.length > c.prevInput.maxPointers ? b.pointers.length : c.prevInput.maxPointers : b.pointers.length, C(c, b);
                var k = a.element;
                o(b.srcEvent.target, k) && (k = b.srcEvent.target), b.target = k
            }

            function B(a, b) {
                var c = b.center, d = a.offsetDelta || {}, e = a.prevDelta || {}, f = a.prevInput || {};
                b.eventType !== Ea && f.eventType !== Ga || (e = a.prevDelta = {
                    x: f.deltaX || 0,
                    y: f.deltaY || 0
                }, d = a.offsetDelta = {x: c.x, y: c.y}), b.deltaX = e.x + (c.x - d.x), b.deltaY = e.y + (c.y - d.y)
            }

            function C(a, b) {
                var c, e, f, g, h = a.lastInterval || b, i = b.timeStamp - h.timeStamp;
                if (b.eventType != Ha && (i > Da || h.velocity === d)) {
                    var j = b.deltaX - h.deltaX, k = b.deltaY - h.deltaY, l = F(i, j, k);
                    e = l.x, f = l.y, c = qa(l.x) > qa(l.y) ? l.x : l.y, g = G(j, k), a.lastInterval = b
                } else c = h.velocity, e = h.velocityX, f = h.velocityY, g = h.direction;
                b.velocity = c, b.velocityX = e, b.velocityY = f, b.direction = g
            }

            function D(a) {
                for (var b = [], c = 0; c < a.pointers.length;) b[c] = {
                    clientX: pa(a.pointers[c].clientX),
                    clientY: pa(a.pointers[c].clientY)
                }, c++;
                return {timeStamp: ra(), pointers: b, center: E(b), deltaX: a.deltaX, deltaY: a.deltaY}
            }

            function E(a) {
                var b = a.length;
                if (1 === b) return {x: pa(a[0].clientX), y: pa(a[0].clientY)};
                for (var c = 0, d = 0, e = 0; b > e;) c += a[e].clientX, d += a[e].clientY, e++;
                return {x: pa(c / b), y: pa(d / b)}
            }

            function F(a, b, c) {
                return {x: b / a || 0, y: c / a || 0}
            }

            function G(a, b) {
                return a === b ? Ia : qa(a) >= qa(b) ? 0 > a ? Ja : Ka : 0 > b ? La : Ma
            }

            function H(a, b, c) {
                c || (c = Qa);
                var d = b[c[0]] - a[c[0]], e = b[c[1]] - a[c[1]];
                return Math.sqrt(d * d + e * e)
            }

            function I(a, b, c) {
                c || (c = Qa);
                var d = b[c[0]] - a[c[0]], e = b[c[1]] - a[c[1]];
                return 180 * Math.atan2(e, d) / Math.PI
            }

            function J(a, b) {
                return I(b[1], b[0], Ra) + I(a[1], a[0], Ra)
            }

            function K(a, b) {
                return H(b[0], b[1], Ra) / H(a[0], a[1], Ra)
            }

            function L() {
                this.evEl = Ta, this.evWin = Ua, this.pressed = !1, x.apply(this, arguments)
            }

            function M() {
                this.evEl = Xa, this.evWin = Ya, x.apply(this, arguments), this.store = this.manager.session.pointerEvents = []
            }

            function N() {
                this.evTarget = $a, this.evWin = _a, this.started = !1, x.apply(this, arguments)
            }

            function O(a, b) {
                var c = s(a.touches), d = s(a.changedTouches);
                return b & (Ga | Ha) && (c = t(c.concat(d), "identifier", !0)), [c, d]
            }

            function P() {
                this.evTarget = bb, this.targetIds = {}, x.apply(this, arguments)
            }

            function Q(a, b) {
                var c = s(a.touches), d = this.targetIds;
                if (b & (Ea | Fa) && 1 === c.length) return d[c[0].identifier] = !0, [c, c];
                var e, f, g = s(a.changedTouches), h = [], i = this.target;
                if (f = c.filter(function (a) {
                    return o(a.target, i)
                }), b === Ea) for (e = 0; e < f.length;) d[f[e].identifier] = !0, e++;
                for (e = 0; e < g.length;) d[g[e].identifier] && h.push(g[e]), b & (Ga | Ha) && delete d[g[e].identifier], e++;
                return h.length ? [t(f.concat(h), "identifier", !0), h] : void 0
            }

            function R() {
                x.apply(this, arguments);
                var a = j(this.handler, this);
                this.touch = new P(this.manager, a), this.mouse = new L(this.manager, a), this.primaryTouch = null, this.lastTouches = []
            }

            function S(a, b) {
                a & Ea ? (this.primaryTouch = b.changedPointers[0].identifier, T.call(this, b)) : a & (Ga | Ha) && T.call(this, b)
            }

            function T(a) {
                var b = a.changedPointers[0];
                if (b.identifier === this.primaryTouch) {
                    var c = {x: b.clientX, y: b.clientY};
                    this.lastTouches.push(c);
                    var d = this.lastTouches, e = function () {
                        var a = d.indexOf(c);
                        a > -1 && d.splice(a, 1)
                    };
                    setTimeout(e, cb)
                }
            }

            function U(a) {
                for (var b = a.srcEvent.clientX, c = a.srcEvent.clientY, d = 0; d < this.lastTouches.length; d++) {
                    var e = this.lastTouches[d], f = Math.abs(b - e.x), g = Math.abs(c - e.y);
                    if (db >= f && db >= g) return !0
                }
                return !1
            }

            function V(a, b) {
                this.manager = a, this.set(b)
            }

            function W(a) {
                if (p(a, jb)) return jb;
                var b = p(a, kb), c = p(a, lb);
                return b && c ? jb : b || c ? b ? kb : lb : p(a, ib) ? ib : hb
            }

            function X() {
                if (!fb) return !1;
                var b = {}, c = a.CSS && a.CSS.supports;
                return ["auto", "manipulation", "pan-y", "pan-x", "pan-x pan-y", "none"].forEach(function (d) {
                    b[d] = c ? a.CSS.supports("touch-action", d) : !0
                }), b
            }

            function Y(a) {
                this.options = la({}, this.defaults, a || {}), this.id = v(), this.manager = null, this.options.enable = l(this.options.enable, !0), this.state = nb, this.simultaneous = {}, this.requireFail = []
            }

            function Z(a) {
                return a & sb ? "cancel" : a & qb ? "end" : a & pb ? "move" : a & ob ? "start" : ""
            }

            function $(a) {
                return a == Ma ? "down" : a == La ? "up" : a == Ja ? "left" : a == Ka ? "right" : ""
            }

            function _(a, b) {
                var c = b.manager;
                return c ? c.get(a) : a
            }

            function aa() {
                Y.apply(this, arguments)
            }

            function ba() {
                aa.apply(this, arguments), this.pX = null, this.pY = null
            }

            function ca() {
                aa.apply(this, arguments)
            }

            function da() {
                Y.apply(this, arguments), this._timer = null, this._input = null
            }

            function ea() {
                aa.apply(this, arguments)
            }

            function fa() {
                aa.apply(this, arguments)
            }

            function ga() {
                Y.apply(this, arguments), this.pTime = !1, this.pCenter = !1, this._timer = null, this._input = null, this.count = 0
            }

            function ha(a, b) {
                return b = b || {}, b.recognizers = l(b.recognizers, ha.defaults.preset), new ia(a, b)
            }

            function ia(a, b) {
                this.options = la({}, ha.defaults, b || {}), this.options.inputTarget = this.options.inputTarget || a, this.handlers = {}, this.session = {}, this.recognizers = [], this.oldCssProps = {}, this.element = a, this.input = y(this), this.touchAction = new V(this, this.options.touchAction), ja(this, !0), g(this.options.recognizers, function (a) {
                    var b = this.add(new a[0](a[1]));
                    a[2] && b.recognizeWith(a[2]), a[3] && b.requireFailure(a[3])
                }, this)
            }

            function ja(a, b) {
                var c = a.element;
                if (c.style) {
                    var d;
                    g(a.options.cssProps, function (e, f) {
                        d = u(c.style, f), b ? (a.oldCssProps[d] = c.style[d], c.style[d] = e) : c.style[d] = a.oldCssProps[d] || ""
                    }), b || (a.oldCssProps = {})
                }
            }

            function ka(a, c) {
                var d = b.createEvent("Event");
                d.initEvent(a, !0, !0), d.gesture = c, c.target.dispatchEvent(d)
            }

            var la, ma = ["", "webkit", "Moz", "MS", "ms", "o"], na = b.createElement("div"), oa = "function",
                pa = Math.round,
                qa = Math.abs, ra = Date.now;
            la = "function" != typeof Object.assign ? function (a) {
                if (a === d || null === a) throw new TypeError("Cannot convert undefined or null to object");
                for (var b = Object(a), c = 1; c < arguments.length; c++) {
                    var e = arguments[c];
                    if (e !== d && null !== e) for (var f in e) e.hasOwnProperty(f) && (b[f] = e[f])
                }
                return b
            } : Object.assign;
            var sa = h(function (a, b, c) {
                    for (var e = Object.keys(b), f = 0; f < e.length;) (!c || c && a[e[f]] === d) && (a[e[f]] = b[e[f]]), f++;
                    return a
                }, "extend", "Use `assign`."), ta = h(function (a, b) {
                    return sa(a, b, !0)
                }, "merge", "Use `assign`."), ua = 1, va = /mobile|tablet|ip(ad|hone|od)|android/i,
                wa = "ontouchstart" in a,
                xa = u(a, "PointerEvent") !== d, ya = wa && va.test(navigator.userAgent), za = "touch", Aa = "pen",
                Ba = "mouse", Ca = "kinect", Da = 25, Ea = 1, Fa = 2, Ga = 4, Ha = 8, Ia = 1, Ja = 2, Ka = 4, La = 8,
                Ma = 16,
                Na = Ja | Ka, Oa = La | Ma, Pa = Na | Oa, Qa = ["x", "y"], Ra = ["clientX", "clientY"];
            x.prototype = {
                handler: function () {
                }, init: function () {
                    this.evEl && m(this.element, this.evEl, this.domHandler), this.evTarget && m(this.target, this.evTarget, this.domHandler), this.evWin && m(w(this.element), this.evWin, this.domHandler)
                }, destroy: function () {
                    this.evEl && n(this.element, this.evEl, this.domHandler), this.evTarget && n(this.target, this.evTarget, this.domHandler), this.evWin && n(w(this.element), this.evWin, this.domHandler)
                }
            };
            var Sa = {mousedown: Ea, mousemove: Fa, mouseup: Ga}, Ta = "mousedown", Ua = "mousemove mouseup";
            i(L, x, {
                handler: function (a) {
                    var b = Sa[a.type];
                    b & Ea && 0 === a.button && (this.pressed = !0), b & Fa && 1 !== a.which && (b = Ga), this.pressed && (b & Ga && (this.pressed = !1), this.callback(this.manager, b, {
                        pointers: [a],
                        changedPointers: [a],
                        pointerType: Ba,
                        srcEvent: a
                    }))
                }
            });
            var Va = {pointerdown: Ea, pointermove: Fa, pointerup: Ga, pointercancel: Ha, pointerout: Ha},
                Wa = {2: za, 3: Aa, 4: Ba, 5: Ca}, Xa = "pointerdown", Ya = "pointermove pointerup pointercancel";
            a.MSPointerEvent && !a.PointerEvent && (Xa = "MSPointerDown", Ya = "MSPointerMove MSPointerUp MSPointerCancel"), i(M, x, {
                handler: function (a) {
                    var b = this.store, c = !1, d = a.type.toLowerCase().replace("ms", ""), e = Va[d],
                        f = Wa[a.pointerType] || a.pointerType, g = f == za, h = r(b, a.pointerId, "pointerId");
                    e & Ea && (0 === a.button || g) ? 0 > h && (b.push(a), h = b.length - 1) : e & (Ga | Ha) && (c = !0), 0 > h || (b[h] = a, this.callback(this.manager, e, {
                        pointers: b,
                        changedPointers: [a],
                        pointerType: f,
                        srcEvent: a
                    }), c && b.splice(h, 1))
                }
            });
            var Za = {touchstart: Ea, touchmove: Fa, touchend: Ga, touchcancel: Ha}, $a = "touchstart",
                _a = "touchstart touchmove touchend touchcancel";
            i(N, x, {
                handler: function (a) {
                    var b = Za[a.type];
                    if (b === Ea && (this.started = !0), this.started) {
                        var c = O.call(this, a, b);
                        b & (Ga | Ha) && c[0].length - c[1].length === 0 && (this.started = !1), this.callback(this.manager, b, {
                            pointers: c[0],
                            changedPointers: c[1],
                            pointerType: za,
                            srcEvent: a
                        })
                    }
                }
            });
            var ab = {touchstart: Ea, touchmove: Fa, touchend: Ga, touchcancel: Ha},
                bb = "touchstart touchmove touchend touchcancel";
            i(P, x, {
                handler: function (a) {
                    var b = ab[a.type], c = Q.call(this, a, b);
                    c && this.callback(this.manager, b, {
                        pointers: c[0],
                        changedPointers: c[1],
                        pointerType: za,
                        srcEvent: a
                    })
                }
            });
            var cb = 2500, db = 25;
            i(R, x, {
                handler: function (a, b, c) {
                    var d = c.pointerType == za, e = c.pointerType == Ba;
                    if (!(e && c.sourceCapabilities && c.sourceCapabilities.firesTouchEvents)) {
                        if (d) S.call(this, b, c); else if (e && U.call(this, c)) return;
                        this.callback(a, b, c)
                    }
                }, destroy: function () {
                    this.touch.destroy(), this.mouse.destroy()
                }
            });
            var eb = u(na.style, "touchAction"), fb = eb !== d, gb = "compute", hb = "auto", ib = "manipulation",
                jb = "none",
                kb = "pan-x", lb = "pan-y", mb = X();
            V.prototype = {
                set: function (a) {
                    a == gb && (a = this.compute()), fb && this.manager.element.style && mb[a] && (this.manager.element.style[eb] = a), this.actions = a.toLowerCase().trim()
                }, update: function () {
                    this.set(this.manager.options.touchAction)
                }, compute: function () {
                    var a = [];
                    return g(this.manager.recognizers, function (b) {
                        k(b.options.enable, [b]) && (a = a.concat(b.getTouchAction()))
                    }), W(a.join(" "))
                }, preventDefaults: function (a) {
                    var b = a.srcEvent, c = a.offsetDirection;
                    if (this.manager.session.prevented) return void b.preventDefault();
                    var d = this.actions, e = p(d, jb) && !mb[jb], f = p(d, lb) && !mb[lb], g = p(d, kb) && !mb[kb];
                    if (e) {
                        var h = 1 === a.pointers.length, i = a.distance < 2, j = a.deltaTime < 250;
                        if (h && i && j) return
                    }
                    return g && f ? void 0 : e || f && c & Na || g && c & Oa ? this.preventSrc(b) : void 0
                }, preventSrc: function (a) {
                    this.manager.session.prevented = !0, a.preventDefault()
                }
            };
            var nb = 1, ob = 2, pb = 4, qb = 8, rb = qb, sb = 16, tb = 32;
            Y.prototype = {
                defaults: {}, set: function (a) {
                    return la(this.options, a), this.manager && this.manager.touchAction.update(), this
                }, recognizeWith: function (a) {
                    if (f(a, "recognizeWith", this)) return this;
                    var b = this.simultaneous;
                    return a = _(a, this), b[a.id] || (b[a.id] = a, a.recognizeWith(this)), this
                }, dropRecognizeWith: function (a) {
                    return f(a, "dropRecognizeWith", this) ? this : (a = _(a, this), delete this.simultaneous[a.id], this)
                }, requireFailure: function (a) {
                    if (f(a, "requireFailure", this)) return this;
                    var b = this.requireFail;
                    return a = _(a, this), -1 === r(b, a) && (b.push(a), a.requireFailure(this)), this
                }, dropRequireFailure: function (a) {
                    if (f(a, "dropRequireFailure", this)) return this;
                    a = _(a, this);
                    var b = r(this.requireFail, a);
                    return b > -1 && this.requireFail.splice(b, 1), this
                }, hasRequireFailures: function () {
                    return this.requireFail.length > 0
                }, canRecognizeWith: function (a) {
                    return !!this.simultaneous[a.id]
                }, emit: function (a) {
                    function b(b) {
                        c.manager.emit(b, a)
                    }

                    var c = this, d = this.state;
                    qb > d && b(c.options.event + Z(d)), b(c.options.event), a.additionalEvent && b(a.additionalEvent), d >= qb && b(c.options.event + Z(d))
                }, tryEmit: function (a) {
                    return this.canEmit() ? this.emit(a) : void (this.state = tb)
                }, canEmit: function () {
                    for (var a = 0; a < this.requireFail.length;) {
                        if (!(this.requireFail[a].state & (tb | nb))) return !1;
                        a++
                    }
                    return !0
                }, recognize: function (a) {
                    var b = la({}, a);
                    return k(this.options.enable, [this, b]) ? (this.state & (rb | sb | tb) && (this.state = nb), this.state = this.process(b), void (this.state & (ob | pb | qb | sb) && this.tryEmit(b))) : (this.reset(), void (this.state = tb))
                }, process: function (a) {
                }, getTouchAction: function () {
                }, reset: function () {
                }
            }, i(aa, Y, {
                defaults: {pointers: 1}, attrTest: function (a) {
                    var b = this.options.pointers;
                    return 0 === b || a.pointers.length === b
                }, process: function (a) {
                    var b = this.state, c = a.eventType, d = b & (ob | pb), e = this.attrTest(a);
                    return d && (c & Ha || !e) ? b | sb : d || e ? c & Ga ? b | qb : b & ob ? b | pb : ob : tb
                }
            }), i(ba, aa, {
                defaults: {event: "pan", threshold: 10, pointers: 1, direction: Pa}, getTouchAction: function () {
                    var a = this.options.direction, b = [];
                    return a & Na && b.push(lb), a & Oa && b.push(kb), b
                }, directionTest: function (a) {
                    var b = this.options, c = !0, d = a.distance, e = a.direction, f = a.deltaX, g = a.deltaY;
                    return e & b.direction || (b.direction & Na ? (e = 0 === f ? Ia : 0 > f ? Ja : Ka, c = f != this.pX, d = Math.abs(a.deltaX)) : (e = 0 === g ? Ia : 0 > g ? La : Ma, c = g != this.pY, d = Math.abs(a.deltaY))), a.direction = e, c && d > b.threshold && e & b.direction
                }, attrTest: function (a) {
                    return aa.prototype.attrTest.call(this, a) && (this.state & ob || !(this.state & ob) && this.directionTest(a))
                }, emit: function (a) {
                    this.pX = a.deltaX, this.pY = a.deltaY;
                    var b = $(a.direction);
                    b && (a.additionalEvent = this.options.event + b), this._super.emit.call(this, a)
                }
            }), i(ca, aa, {
                defaults: {event: "pinch", threshold: 0, pointers: 2}, getTouchAction: function () {
                    return [jb]
                }, attrTest: function (a) {
                    return this._super.attrTest.call(this, a) && (Math.abs(a.scale - 1) > this.options.threshold || this.state & ob)
                }, emit: function (a) {
                    if (1 !== a.scale) {
                        var b = a.scale < 1 ? "in" : "out";
                        a.additionalEvent = this.options.event + b
                    }
                    this._super.emit.call(this, a)
                }
            }), i(da, Y, {
                defaults: {event: "press", pointers: 1, time: 251, threshold: 9}, getTouchAction: function () {
                    return [hb]
                }, process: function (a) {
                    var b = this.options, c = a.pointers.length === b.pointers, d = a.distance < b.threshold,
                        f = a.deltaTime > b.time;
                    if (this._input = a, !d || !c || a.eventType & (Ga | Ha) && !f) this.reset(); else if (a.eventType & Ea) this.reset(), this._timer = e(function () {
                        this.state = rb, this.tryEmit()
                    }, b.time, this); else if (a.eventType & Ga) return rb;
                    return tb
                }, reset: function () {
                    clearTimeout(this._timer)
                }, emit: function (a) {
                    this.state === rb && (a && a.eventType & Ga ? this.manager.emit(this.options.event + "up", a) : (this._input.timeStamp = ra(), this.manager.emit(this.options.event, this._input)))
                }
            }), i(ea, aa, {
                defaults: {event: "rotate", threshold: 0, pointers: 2}, getTouchAction: function () {
                    return [jb]
                }, attrTest: function (a) {
                    return this._super.attrTest.call(this, a) && (Math.abs(a.rotation) > this.options.threshold || this.state & ob)
                }
            }), i(fa, aa, {
                defaults: {event: "swipe", threshold: 10, velocity: .3, direction: Na | Oa, pointers: 1},
                getTouchAction: function () {
                    return ba.prototype.getTouchAction.call(this)
                },
                attrTest: function (a) {
                    var b, c = this.options.direction;
                    return c & (Na | Oa) ? b = a.overallVelocity : c & Na ? b = a.overallVelocityX : c & Oa && (b = a.overallVelocityY), this._super.attrTest.call(this, a) && c & a.offsetDirection && a.distance > this.options.threshold && a.maxPointers == this.options.pointers && qa(b) > this.options.velocity && a.eventType & Ga
                },
                emit: function (a) {
                    var b = $(a.offsetDirection);
                    b && this.manager.emit(this.options.event + b, a), this.manager.emit(this.options.event, a)
                }
            }), i(ga, Y, {
                defaults: {
                    event: "tap",
                    pointers: 1,
                    taps: 1,
                    interval: 300,
                    time: 250,
                    threshold: 9,
                    posThreshold: 10
                }, getTouchAction: function () {
                    return [ib]
                }, process: function (a) {
                    var b = this.options, c = a.pointers.length === b.pointers, d = a.distance < b.threshold,
                        f = a.deltaTime < b.time;
                    if (this.reset(), a.eventType & Ea && 0 === this.count) return this.failTimeout();
                    if (d && f && c) {
                        if (a.eventType != Ga) return this.failTimeout();
                        var g = this.pTime ? a.timeStamp - this.pTime < b.interval : !0,
                            h = !this.pCenter || H(this.pCenter, a.center) < b.posThreshold;
                        this.pTime = a.timeStamp, this.pCenter = a.center, h && g ? this.count += 1 : this.count = 1, this._input = a;
                        var i = this.count % b.taps;
                        if (0 === i) return this.hasRequireFailures() ? (this._timer = e(function () {
                            this.state = rb, this.tryEmit()
                        }, b.interval, this), ob) : rb
                    }
                    return tb
                }, failTimeout: function () {
                    return this._timer = e(function () {
                        this.state = tb
                    }, this.options.interval, this), tb
                }, reset: function () {
                    clearTimeout(this._timer)
                }, emit: function () {
                    this.state == rb && (this._input.tapCount = this.count, this.manager.emit(this.options.event, this._input))
                }
            }), ha.VERSION = "2.0.8", ha.defaults = {
                domEvents: !1,
                touchAction: gb,
                enable: !0,
                inputTarget: null,
                inputClass: null,
                preset: [[ea, {enable: !1}], [ca, {enable: !1}, ["rotate"]], [fa, {direction: Na}], [ba, {direction: Na}, ["swipe"]], [ga], [ga, {
                    event: "doubletap",
                    taps: 2
                }, ["tap"]], [da]],
                cssProps: {
                    userSelect: "none",
                    touchSelect: "none",
                    touchCallout: "none",
                    contentZooming: "none",
                    userDrag: "none",
                    tapHighlightColor: "rgba(0,0,0,0)"
                }
            };
            var ub = 1, vb = 2;
            ia.prototype = {
                set: function (a) {
                    return la(this.options, a), a.touchAction && this.touchAction.update(), a.inputTarget && (this.input.destroy(), this.input.target = a.inputTarget, this.input.init()), this
                }, stop: function (a) {
                    this.session.stopped = a ? vb : ub
                }, recognize: function (a) {
                    var b = this.session;
                    if (!b.stopped) {
                        this.touchAction.preventDefaults(a);
                        var c, d = this.recognizers, e = b.curRecognizer;
                        (!e || e && e.state & rb) && (e = b.curRecognizer = null);
                        for (var f = 0; f < d.length;) c = d[f], b.stopped === vb || e && c != e && !c.canRecognizeWith(e) ? c.reset() : c.recognize(a), !e && c.state & (ob | pb | qb) && (e = b.curRecognizer = c), f++
                    }
                }, get: function (a) {
                    if (a instanceof Y) return a;
                    for (var b = this.recognizers, c = 0; c < b.length; c++) if (b[c].options.event == a) return b[c];
                    return null
                }, add: function (a) {
                    if (f(a, "add", this)) return this;
                    var b = this.get(a.options.event);
                    return b && this.remove(b), this.recognizers.push(a), a.manager = this, this.touchAction.update(), a
                }, remove: function (a) {
                    if (f(a, "remove", this)) return this;
                    if (a = this.get(a)) {
                        var b = this.recognizers, c = r(b, a);
                        -1 !== c && (b.splice(c, 1), this.touchAction.update())
                    }
                    return this
                }, on: function (a, b) {
                    if (a !== d && b !== d) {
                        var c = this.handlers;
                        return g(q(a), function (a) {
                            c[a] = c[a] || [], c[a].push(b)
                        }), this
                    }
                }, off: function (a, b) {
                    if (a !== d) {
                        var c = this.handlers;
                        return g(q(a), function (a) {
                            b ? c[a] && c[a].splice(r(c[a], b), 1) : delete c[a]
                        }), this
                    }
                }, emit: function (a, b) {
                    this.options.domEvents && ka(a, b);
                    var c = this.handlers[a] && this.handlers[a].slice();
                    if (c && c.length) {
                        b.type = a, b.preventDefault = function () {
                            b.srcEvent.preventDefault()
                        };
                        for (var d = 0; d < c.length;) c[d](b), d++
                    }
                }, destroy: function () {
                    this.element && ja(this, !1), this.handlers = {}, this.session = {}, this.input.destroy(), this.element = null
                }
            }, la(ha, {
                INPUT_START: Ea,
                INPUT_MOVE: Fa,
                INPUT_END: Ga,
                INPUT_CANCEL: Ha,
                STATE_POSSIBLE: nb,
                STATE_BEGAN: ob,
                STATE_CHANGED: pb,
                STATE_ENDED: qb,
                STATE_RECOGNIZED: rb,
                STATE_CANCELLED: sb,
                STATE_FAILED: tb,
                DIRECTION_NONE: Ia,
                DIRECTION_LEFT: Ja,
                DIRECTION_RIGHT: Ka,
                DIRECTION_UP: La,
                DIRECTION_DOWN: Ma,
                DIRECTION_HORIZONTAL: Na,
                DIRECTION_VERTICAL: Oa,
                DIRECTION_ALL: Pa,
                Manager: ia,
                Input: x,
                TouchAction: V,
                TouchInput: P,
                MouseInput: L,
                PointerEventInput: M,
                TouchMouseInput: R,
                SingleTouchInput: N,
                Recognizer: Y,
                AttrRecognizer: aa,
                Tap: ga,
                Pan: ba,
                Swipe: fa,
                Pinch: ca,
                Rotate: ea,
                Press: da,
                on: m,
                off: n,
                each: g,
                merge: ta,
                extend: sa,
                assign: la,
                inherit: i,
                bindFn: j,
                prefixed: u
            });
            var wb = "undefined" != typeof a ? a : "undefined" != typeof self ? self : {};
            wb.Hammer = ha, "function" == typeof define && define.amd ? define(function () {
                return ha
            }) : "undefined" != typeof module && module.exports ? module.exports = ha : a[c] = ha
        }(window, document, "Hammer");
//# sourceMappingURL=hammer.min.js.map
    })(jQuery);
    /*!
        * jQuery Mousewheel 3.1.13
        *
        * Copyright 2015 jQuery Foundation and other contributors
        * Released under the MIT license.
        * http://jquery.org/license
        */
    !function (a) {
        "function" == typeof define && define.amd ? define(["jquery"], a) : "object" == typeof exports ? module.exports = a : a(jQuery)
    }(function (a) {
        function b(b) {
            var g = b || window.event, h = i.call(arguments, 1), j = 0, l = 0, m = 0, n = 0, o = 0, p = 0;
            if (b = a.event.fix(g), b.type = "mousewheel", "detail" in g && (m = -1 * g.detail), "wheelDelta" in g && (m = g.wheelDelta), "wheelDeltaY" in g && (m = g.wheelDeltaY), "wheelDeltaX" in g && (l = -1 * g.wheelDeltaX), "axis" in g && g.axis === g.HORIZONTAL_AXIS && (l = -1 * m, m = 0), j = 0 === m ? l : m, "deltaY" in g && (m = -1 * g.deltaY, j = m), "deltaX" in g && (l = g.deltaX, 0 === m && (j = -1 * l)), 0 !== m || 0 !== l) {
                if (1 === g.deltaMode) {
                    var q = a.data(this, "mousewheel-line-height");
                    j *= q, m *= q, l *= q
                } else if (2 === g.deltaMode) {
                    var r = a.data(this, "mousewheel-page-height");
                    j *= r, m *= r, l *= r
                }
                if (n = Math.max(Math.abs(m), Math.abs(l)), (!f || f > n) && (f = n, d(g, n) && (f /= 40)), d(g, n) && (j /= 40, l /= 40, m /= 40), j = Math[j >= 1 ? "floor" : "ceil"](j / f), l = Math[l >= 1 ? "floor" : "ceil"](l / f), m = Math[m >= 1 ? "floor" : "ceil"](m / f), k.settings.normalizeOffset && this.getBoundingClientRect) {
                    var s = this.getBoundingClientRect();
                    o = b.clientX - s.left, p = b.clientY - s.top
                }
                return b.deltaX = l, b.deltaY = m, b.deltaFactor = f, b.offsetX = o, b.offsetY = p, b.deltaMode = 0, h.unshift(b, j, l, m), e && clearTimeout(e), e = setTimeout(c, 200), (a.event.dispatch || a.event.handle).apply(this, h)
            }
        }

        function c() {
            f = null
        }

        function d(a, b) {
            return k.settings.adjustOldDeltas && "mousewheel" === a.type && b % 120 === 0
        }

        var e, f, g = ["wheel", "mousewheel", "DOMMouseScroll", "MozMousePixelScroll"],
            h = "onwheel" in document || document.documentMode >= 9 ? ["wheel"] : ["mousewheel", "DomMouseScroll", "MozMousePixelScroll"],
            i = Array.prototype.slice;
        if (a.event.fixHooks) for (var j = g.length; j;) a.event.fixHooks[g[--j]] = a.event.mouseHooks;
        var k = a.event.special.mousewheel = {
            version: "3.1.12", setup: function () {
                if (this.addEventListener) for (var c = h.length; c;) this.addEventListener(h[--c], b, !1); else this.onmousewheel = b;
                a.data(this, "mousewheel-line-height", k.getLineHeight(this)), a.data(this, "mousewheel-page-height", k.getPageHeight(this))
            }, teardown: function () {
                if (this.removeEventListener) for (var c = h.length; c;) this.removeEventListener(h[--c], b, !1); else this.onmousewheel = null;
                a.removeData(this, "mousewheel-line-height"), a.removeData(this, "mousewheel-page-height")
            }, getLineHeight: function (b) {
                var c = a(b), d = c["offsetParent" in a.fn ? "offsetParent" : "parent"]();
                return d.length || (d = a("body")), parseInt(d.css("fontSize"), 10) || parseInt(c.css("fontSize"), 10) || 16
            }, getPageHeight: function (b) {
                return a(b).height()
            }, settings: {adjustOldDeltas: !0, normalizeOffset: !0}
        };
        a.fn.extend({
            mousewheel: function (a) {
                return a ? this.bind("mousewheel", a) : this.trigger("mousewheel")
            }, unmousewheel: function (a) {
                return this.unbind("mousewheel", a)
            }
        })
    });

    /*!
     * @author      YeYe
     * @date        2018.11.6
     * @version     0.0.10
     * @requires
     * jQuery1.6+(http://jquery.com)
     * jquery-mousewheel(https://github.com/jquery/jquery-mousewheel)
     * Hammer.js(hammerjs.github.io)
     *
     * Happy National Day Holiday :-)
     * 图片缩放工具类，您可以拖动缩放图片，并添加标记点
     * 支持同时显示多张图片
     */

    var zoom = (function ($) {
        var GLOBAL = [];     // 全局变量集合
        var gIndex = 0;

        $.fn.extend({
            "zoomMarker": function (_options) {
                const ID = $(this).attr('id');
                initGlobalData(ID);
                var params = getGlobalParam(ID);
                var options = params.options;
                var that = params.that;
                var dialog = params.dialog;
                var isInit = params.isInit;

                if (isInit)
                    return;
                isInit = true;
                params.isInit = isInit;
                document.ondragstart = function () {
                    return false;
                }
                that = $(this);
                params.that = that;
                var offset;
                // 初始化配置
                if (typeof (_options) === 'undefined')
                    options = defaults;
                else
                    options = $.extend({}, defaults, _options);
                params.options = options;
                // 配置图片资源
                if (options.src === null) {
                    console.log('Image resources is not defined.');
                } else {
                    loadImage(ID, options.src);
                }
                // 初始化鼠标滚动监听器
                that.bind('mousewheel', function (event, delta) {
                    that.zoomMarker_Zoom({
                        x: event.pageX,
                        y: event.pageY
                    }, delta > 0 ? (1 + options.rate) : (1 - options.rate));
                    return false;
                });
                // 图片拖动监听器
                var picHamer = new Hammer(document.getElementById($(this).attr('id')));
                picHamer.on("pan", function (e) {
                    if (!(options.pinchlock || false)) {
                        dialog.hide();
                        that.zoomMarker_Move(e.deltaX + offset.left, e.deltaY + offset.top);
                        // 移动图层顺序
                        if (options.auto_index_z) {
                            moveImageTop(ID);
                        }
                    }
                });
                picHamer.on("panstart", function (e) {
                    offset = that.offset();
                });
                picHamer.on("panend", function (e) {
                    // 解除pinch事件，使能pan
                    options.pinchlock = false;
                });
                // 触屏移动事件
                picHamer.get('pinch').set({enable: true});
                picHamer.on("pinchmove", function (e) {
                    dialog.hide();
                    that.zoomMarker_Zoom({x: e.center.x, y: e.center.y}, e.scale / options.pinchscale);
                    options.pinchscale = e.scale;
                });
                picHamer.on("pinchstart", function (e) {
                    // 为了防止缩放手势结束后，两个手指抬起的速度不一致导致误判为pan事件，这里锁定pinch并在pan释放后解除
                    options.pinchlock = true;
                    options.pinchscale = 1.0;
                });
                // 添加用于显示marker悬浮内容窗的div标签
                dialog = $("<div id='zoom-marker-hover-dialog' class='zoom-marker-hover-dialog'></div>");
                params.dialog = dialog;
                that.parent().append(dialog);
                // 添加图像点击监听器，发送消息
                new Hammer(document.getElementById(that.attr('id'))).on('tap', function (e) {
                    var params = getGlobalParam(ID);
                    var options = params.options;
                    var that = params.that;
                    if (typeof (e.pointers[0].x) === 'undefined') {
                        var offset = that.offset();
                        that.trigger("zoom_marker_mouse_click", {
                            x: (e.center.x - offset.left) / that.width() * options.imgNaturalSize.width,
                            y: (e.center.y - offset.top) / that.height() * options.imgNaturalSize.height
                        });
                    } else {
                        that.trigger("zoom_marker_mouse_click", {
                            pageX: e.pointers[0].offsetX,
                            pageY: e.pointers[0].offsetY,
                            x: e.pointers[0].offsetX / that.width() * options.imgNaturalSize.width,
                            y: e.pointers[0].offsetY / that.height() * options.imgNaturalSize.height
                        });
                    }
                    // 移动图层顺序
                    if (options.auto_index_z) {
                        moveImageTop(ID);
                    }
                });
            },
            // 加载图片
            "zoomMarker_LoadImage": function (src) {
                const ID = $(this).attr('id');
                loadImage(ID, src, true);
            },
            /**
             * 图片缩放
             * @param center    缩放中心坐标，{x:1, y:2}
             * @param scale     缩放比例，>0为放大，<0为缩小
             */
            "zoomMarker_Zoom": function (center, scale) {
                const ID = $(this).attr('id');
                var params = getGlobalParam(ID);
                var options = params.options;
                var that = params.that;

                //var that = $(this);
                //var options = document.options;
                var offset = that.offset();
                const hwRatio = options.imgNaturalSize.height / options.imgNaturalSize.width;
                var h0 = that.height();
                var w0 = that.width();
                var tarWidth = w0 * scale;
                var tarHeight = tarWidth * hwRatio;
                // 最大宽度限制
                if (null != options.max && tarWidth > options.max) {
                    tarWidth = options.max;
                    tarHeight = tarWidth * hwRatio;
                }
                // 最小宽度限制
                else if (null != options.min && tarWidth < options.min) {
                    tarWidth = options.min;
                    tarHeight = tarWidth * hwRatio;
                }
                that.height(parseInt(tarHeight));
                that.width(parseInt(tarWidth));
                if (typeof (center) === 'undefined' || center === null) {
                    center = {};
                    center.x = offset.left + w0 / 2;
                    center.y = offset.top + h0 / 2;
                }
                that.offset({
                    top: (center.y - that.height() * (center.y - offset.top) / h0),
                    left: (center.x - that.width() * (center.x - offset.left) / w0)
                })
                reloadMarkers(ID);
            },
            // 图片拖动
            // x>0向右移动，y>0向下移动
            "zoomMarker_Move": function (x, y) {
                const ID = $(this).attr('id');
                var params = getGlobalParam(ID);
                var options = params.options;
                // 是否允许拖动
                if (options.enable_drag) {
                    $(this).offset({top: y, left: x});
                    reloadMarkers(ID);
                }
            },
            "zoomMarker_Param": function () {
                const ID = $(this).attr('id');
                var params = getGlobalParam(ID);
                return params;
            },
            "zoomMarker_GetOffset": function (x, y) {
                const ID = $(this).attr('id');
                var params = getGlobalParam(ID);
                var options = params.options;

                var offsetX = x / options.imgNaturalSize.width * params.that.width()
                var offsetY = y / options.imgNaturalSize.height * params.that.height()
                return {'x': offsetX, 'y': offsetY};
            },
            // 添加标记点
            // marker {src:"marker.png", x:100, y:100, size:20}
            "zoomMarker_AddMarker": function (marker) {
                return addMarker($(this).attr('id'), marker);
            },
            // 删除标记点
            "zoomMarker_RemoveMarker": function (markerId) {
                removeMarker($(this).attr('id'), markerId);
            },
            // 清空标记点
            "zoomMarker_CleanMarker": function () {
                cleanMarkers($(this).attr('id'));
            },
            // 获取图像真实尺寸
            "zoomMarker_GetPicSize": function () {
                const ID = $(this).attr('id');
                var params = getGlobalParam(ID);
                var options = params.options;
                return options.imgNaturalSize;
            },
            // 图像居中显示
            "zoomMarker_ImageCenterAlign": function () {
                imageCenterAlign($(this).attr('id'));
            },
            // 设置是否允许拖动
            "zoomMarker_EnableDrag": function (enable) {
                enableDrag($(this).attr('id'), enable);
            },
            // 置顶图像迭代顺序
            "zoomMarker_TopIndexZ": function () {
                moveImageTop($(this).attr('id'));
            }
        });

        /**
         * 初始化全局变量
         * @param id        当前item的ID，作为标识的唯一KEY
         */
        var initGlobalData = function (id) {
            if (typeof (GLOBAL[id]) === 'undefined') {
                const param = {
                    index: gIndex += 2,
                    id: id,
                    options: {
                        imgNaturalSize: {
                            width: 0,
                            height: 0
                        }
                    },
                    that: null,
                    dialog: null,
                    isInit: false,
                    markerList: [],    // 数组，存放marker的DOM对象
                    markerId: 0        // marker的唯一ID，只能增加
                }
                GLOBAL.push(param);
                return param;
            } else {
                return GLOBAL[id];
            }
        }

        /**
         * 获取全局变量属性值
         * @param id            item对应ID
         * @returns {*}
         */
        var getGlobalParam = function (id) {
            for (var i = 0; i < GLOBAL.length; ++i) {
                if (GLOBAL[i].id === id) {
                    return GLOBAL[i];
                }
            }
            return null;
        }

        /**
         * 异步获取图片大小，需要等待图片加载完后才能判断图片实际大小
         * @param img
         * @param fn        {width:rw, height:rh}
         */
        var getImageSize = function (img, fn) {
            img.onload = null;
            if (img.complete) {
                fn(_getImageSize(img));
            } else {
                img.onload = function () {
                    fn(_getImageSize(img));
                }
            }
        };

        /**
         * 获取图片大小的子方法，参考getImageSize()
         * @param img
         * @returns {{width: (Number|number), height: (Number|number)}}
         * @private
         */
        var _getImageSize = function (img) {
            if (typeof img.naturalWidth === "undefined") {
                // IE 6/7/8
                var i = new Image();
                i.src = img.src;
                var rw = i.width;
                var rh = i.height;
            } else {
                // HTML5 browsers
                var rw = img.naturalWidth;
                var rh = img.naturalHeight;
            }
            return ({width: rw, height: rh});
        }

        /**
         * 加载图片
         * @param id        调用该方法的item对应的id
         * @param src       加载图片资源路径
         * @param noResize  是否调整图片的位置
         */
        var loadImage = function (id, src, noResize) {
            var params = getGlobalParam(id);
            var options = params.options;
            var that = params.that;
            that.trigger("zoom_marker_img_load", src);
            that.attr("src", src);
            that.css("z-index", params.index);
            getImageSize(document.getElementsByName(that.attr('name'))[0], function (size) {
                if (typeof (noResize) === 'undefined' || !noResize) {
                    // 调整图片宽高
                    var originWidth = that.width();
                    var originHeight = that.height();
                    if (options.width != null) {
                        that.width(options.width);
                        that.height(that.width() / originWidth * originHeight);
                    }
                }
                that.trigger("zoom_marker_img_loaded", size);
                if (typeof (noResize) === 'undefined' || !noResize) {
                    imageCenterAlign(id);
                }
                options.imgNaturalSize = size;
                params.options.imgNaturalSize = size;
                loadMarkers(id, options.markers);
            });
        }

        /**
         * 图像居中
         * @param id    item对应的id
         */
        var imageCenterAlign = function (id) {
            var params = getGlobalParam(id);
            var that = params.that;
            // 图像居中
            var offset = that.offset();
            var pDiv = that.parent();
            var top = offset.top + (pDiv.height() - that.height()) / 2;
            var left = offset.left + (pDiv.width() - that.width()) / 2;
            that.offset({top: top > 0 ? top : 0, left: left > 0 ? left : 0});
        }

        /**
         * 加载marker
         * @param id        item对应的id
         * @param markers   需要添加的marker数组
         */
        var loadMarkers = function (id, markers) {
            $(markers).each(function (index, marker) {
                addMarker(id, marker);
            });
        }

        /**
         * 添加marker
         * @param id    item对应的id
         * @param marker
         */
        var addMarker = function (id, marker) {
            var params = getGlobalParam(id);
            var options = params.options;
            var dialog = params.dialog;
            var that = params.that;
            var markerId = params.markerId;
            var markerList = params.markerList;
            var _marker = $("<div class='zoom-marker'><img draggable='false'><span></span></div>");
            _marker.css('z-index', params.index + 1);
            var __marker = _marker.find("img");
            var size = marker.size || options.marker_size;
            marker.size = size;
            __marker.attr('src', marker.src);
            // 标记点内容文本
            if (typeof (marker.hint) != 'undefined') {
                var span = _marker.find("span");
                span.empty().append(marker.hint.value || "");
                span.css(marker.hint.style || {});
            }
            __marker.height(size);
            __marker.width(size);
            var markerObj = {id: markerId++, marker: _marker, param: marker};
            params.markerId = markerId;
            // 按键监听器
            _marker.click(function () {
                if (typeof (marker.click) != "undefined") {
                    marker.click(markerObj);
                }
                that.trigger("zoom_marker_click", markerObj);
            });
            // 悬浮监听器
            if (typeof (marker.dialog) != 'undefined') {
                _marker.mousemove(function (e) {
                    options.hover_marker_id = markerObj.id;
                    dialog.empty().append(marker.dialog.value || '').css(marker.dialog.style || {}).show().offset({
                        left: (marker.dialog.offsetX || 0) + e.pageX,
                        top: (marker.dialog.offsetY || 0) + e.pageY
                    });
                });
                _marker.mouseout(function (e) {
                    options.hover_marker_id = null;
                    dialog.hide();
                });
            }
            ;
            that.parent().append(_marker);
            markerList.push(markerObj);
            params.markerList = markerList;
            setMarkerOffset(id, _marker, marker, that.offset());
            return markerObj;
        }

        /**
         * 在拖动或缩放后重新加载图标
         * @param id    item对应的id
         */
        var reloadMarkers = function (id) {
            var params = getGlobalParam(id);
            var that = params.that;
            var offset = that.offset();
            $(params.markerList).each(function (index, element) {
                setMarkerOffset(id, element.marker, element.param, offset);
            });
        };

        /**
         * 清空标记
         * @param id    item对应的id
         */
        var cleanMarkers = function (id) {
            var params = getGlobalParam(id);
            var markerList = params.markerList;
            $(markerList).each(function (index, element) {
                element.marker.unbind();
                element.marker.remove();
            });
            params.markerList = [];
            params.options.markers = [];
            params.dialog.hide();
        };

        /**
         * 删除标记
         * @param id        item对应的id
         * @param markerId  标记点的唯一ID
         */
        var removeMarker = function (id, markerId) {
            var params = getGlobalParam(id);
            var markerId = params.markerId;
            var options = params.options;
            var dialog = params.dialog;
            $(params.markerList).each(function (index, element) {
                if (element.id === markerId) {
                    element.marker.unbind();
                    element.marker.remove();
                    // 如果当前悬浮窗在该marker上显示，需要隐藏该悬浮窗
                    if (((options.hover_marker_id || null) != null) && options.hover_marker_id === markerId) {
                        dialog.hide();
                    }
                    return false;
                }
            });
        }

        /**
         * 配置marker的offset
         * @param id            item对应的id
         * @param marker        需要配置的marker对象
         * @param position      marker的位置信息，包含{x: , y: }
         * @param offset        图片的offset信息
         */
        var setMarkerOffset = function (id, marker, position, offset) {
            var params = getGlobalParam(id);
            const that = params.that;
            const options = params.options;
            marker.offset({
                left: that.width() * position.x / options.imgNaturalSize.width + offset.left - position.size / 2,
                top: that.height() * position.y / options.imgNaturalSize.height + offset.top - position.size
            });
        }

        /**
         * 是否允许图像拖动
         * @param id            item对应的id
         * @param enable        是否允许拖动，布尔型
         */
        var enableDrag = function (id, enable) {
            var params = getGlobalParam(id);
            params.options.enable_drag = enable;
        }

        /**
         * 移动图像和对应标记到顶层
         * @param id            图像id
         */
        var moveImageTop = function (id) {
            const params = getGlobalParam(id);
            // 除了当前图层之外，其他图层都要恢复原来的z-index属性
            GLOBAL.forEach(function (param, index) {
                if (param.id !== id) {
                    param.that.css('z-index', param.index);
                    param.markerList.forEach(function (element, index) {
                        element.marker.css('z-index', param.index + 1);
                    });
                }
            });
            // 配置当前图层z-index
            if (typeof (params) !== 'undefined') {
                const markerList = params.markerList;
                const img = params.that;
                img.css('z-index', 980);
                markerList.forEach(function (element, index) {
                    element.marker.css('z-index', 981);
                });
            }
        }

        var defaults = {
            rate: 0.2,              // 鼠标滚动的缩放速率
            src: null,              // 图片资源
            width: 500,             // 指定图片宽度
            min: 300,               // 图片最小宽度
            max: null,              // 图片最大宽度
            markers: [],            // marker数组，[{src:"marker.png", x:100, y:100, size:20, click:fn()}]
            marker_size: 20,        // 默认marker尺寸
            enable_drag: true,      // 是否允许拖动，默认允许
            auto_index_z: true      // 自动配置图像迭代顺序
        }
    })(jQuery);
    exports('zoom', zoom);
}).addcss('../../lib/extend/zoom/zoom-marker.css');
