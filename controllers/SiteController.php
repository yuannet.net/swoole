<?php

namespace app\controllers;

use app\services\Bug;
use app\services\Demand;
use app\services\LoginForm;
use app\services\OperationLog;
use app\services\Product;
use app\services\User;
use Yii;
use yii\helpers\Url;

class SiteController extends BaseController
{
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionMain()
    {
        $product_overview = [
            'product_total' => count(Product::authProductId([10, 20])),
            'demand_total' => Demand::find()->joinWith(['product'])->where(['demand.develop_user_id' => \Yii::$app->user->identity->id, 'demand.status' => 10, 'demand.step' => [1, 2]])->count(),
            'bug_total' => Bug::find()->joinWith(['product'])->where(['bug.develop_user_id' => \Yii::$app->user->identity->id, 'bug.status' => 10, 'bug.step' => [1, 2]])->count(),
        ];
        $product_info = Product::productStatistics();
        return $this->render('main', ['product_info' => $product_info, 'product_overview' => $product_overview]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post(), '');
            if ($model->login()) {
                $operation_log = new OperationLog();
                $operation_log->create($this, "登陆了系统");
                return $this->success(['url' => Yii::$app->getUser()->getReturnUrl()], '');
            } else {
                return $this->error($model->errors);
            }
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        $operation_log = new OperationLog();
        $operation_log->create($this, "退出了系统");
        Yii::$app->user->logout();
        return $this->goHome();
    }

    public function actionError()
    {
        return $this->alert();
    }

    public function actionUpdatePassword()
    {
        $request = Yii::$app->request;
        $post = Yii::$app->request->post();

        if ($request->isPost) {
            $model = User::findIdentity(Yii::$app->user->identity->id);
            $model->load($request->post(), '');

            //如果填写了密码则修改密码
            if ($post['password_hash']) {
                if ($post['password_hash'] != $post['password_confirm']) {
                    return $this->error('两次输入的密码不同');
                }
                $model->setPassword($post['password_hash']);
            }

            $model->save();
            $model->operation_log->create($this, "修改了自己的用户信息", Url::to(['/organization/user/view', 'id' => $model->id]));
            return $this->success([], '信息修改成功');
        }

        $model = User::findIdentity(Yii::$app->user->identity->id);
        return $this->render('update-password', ['model' => $model]);
    }
}
