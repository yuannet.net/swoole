<?php

namespace app\models;

/**
 * This is the model class for table "{{%memo}}".
 *
 * @property int $id
 * @property string $title 备忘名称
 * @property string|null $remark 备注
 * @property int $level 优先级
 * @property int $plan_date 计划时间
 * @property int $status 状态 0删除 10启用
 * @property int $created_at 创建时间
 * @property int $created_by 创建人
 * @property int $updated_at 修改时间
 * @property int $updated_by 修改人
 */
class Memo extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%memo}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['remark'], 'string'],
            [['level', 'plan_date', 'status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['title'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => '名称',
            'remark' => '描述',
            'level' => '优先级',
            'plan_date' => '时间',
            'status' => '状态',
            'created_at' => '创建时间',
            'created_by' => '创建人',
            'updated_at' => '修改时间',
            'updated_by' => '修改人',
        ];
    }
}
