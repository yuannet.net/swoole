<?php

namespace app\models;

/**
 * This is the model class for table "{{%bug}}".
 *
 * @property int $id
 * @property int $product_id 产品id
 * @property string $title Bug名称
 * @property int $step 进度 0待委派 1待确认 2开发中 3已完成
 * @property int $level 优先级
 * @property string|null $remark Bug描述
 * @property string|null $acceptance_remark 验收标准
 * @property int $develop_user_id 开发人员id
 * @property int $plan_start_date 计划开始时间
 * @property int $plan_end_date 计划完成时间
 * @property int $status 状态 0删除 10启用
 * @property int $created_at 创建时间
 * @property int $created_by 创建人
 * @property int $updated_at 修改时间
 * @property int $updated_by 修改人
 */
class Bug extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%bug}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'step', 'level', 'develop_user_id', 'plan_start_date', 'plan_end_date', 'status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['title'], 'required'],
            [['remark', 'acceptance_remark'], 'string'],
            [['title'], 'string', 'max' => 100],
            [['develop_user_id'], 'validateProductUser']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => '产品ID',
            'title' => 'Bug名称',
            'step' => '进度',
            'level' => '优先级',
            'remark' => 'Bug描述',
            'acceptance_remark' => '验收标准',
            'develop_user_id' => '开发者',
            'plan_start_date' => '计划开始时间',
            'plan_end_date' => '计划完成时间',
            'status' => '状态',
            'created_at' => '创建时间',
            'created_by' => '创建人',
            'updated_at' => '修改时间',
            'updated_by' => '修改人',
        ];
    }
}
