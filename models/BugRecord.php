<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%bug_record}}".
 *
 * @property int $id
 * @property int $bug_id 需求id
 * @property string|null $title 记录标题
 * @property string|null $remark 记录内容
 * @property int $status 状态 0删除 10启用
 * @property int $created_at 创建时间
 * @property int $created_by 创建人
 * @property int $updated_at 修改时间
 * @property int $updated_by 修改人
 */
class BugRecord extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%bug_record}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bug_id', 'status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['remark'], 'string'],
            [['title'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bug_id' => 'Bug ID',
            'title' => '记录名称',
            'remark' => '记录内容',
            'status' => '状态',
            'created_at' => '创建时间',
            'created_by' => '创建人',
            'updated_at' => '修改时间',
            'updated_by' => '修改人',
        ];
    }
}
