<?php

namespace app\models;

/**
 * This is the model class for table "{{%role}}".
 *
 * @property int $id
 * @property string $title 角色名称
 * @property string|null $perm 权限
 * @property int $status 状态 0删除 10启用 20停用
 * @property int $created_at 创建时间
 * @property int $created_by 创建人
 * @property int $updated_at 修改时间
 * @property int $updated_by 修改人
 */
class Role extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%role}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['perm'], 'string'],
            [['title'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => '角色名称',
            'perm' => '权限',
            'status' => '状态',
            'created_at' => '创建时间',
            'created_by' => '创建人',
            'updated_at' => '修改时间',
            'updated_by' => '修改人',
        ];
    }
}
