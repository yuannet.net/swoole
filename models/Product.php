<?php

namespace app\models;

/**
 * This is the model class for table "{{%product}}".
 *
 * @property int $id
 * @property string $title 产品名称
 * @property string|null $remark 产品描述
 * @property int $duration_total 总计工期
 * @property int $duration_used 已用工期
 * @property int $start_date 开始时间
 * @property int $acceptance_date 验收时间
 * @property int $expire_date 合同到期时间
 * @property int $status 产品状态 0删除 10正常 20停用
 * @property int $created_at 创建时间
 * @property int $created_by 创建人
 * @property int $updated_at 修改时间
 * @property int $updated_by 修改人
 */
class Product extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%product}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['remark'], 'string'],
            [['start_date', 'acceptance_date', 'expire_date', 'status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['duration_total', 'duration_used'], 'double'],
            [['title'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => '产品标题',
            'remark' => '产品描述',
            'duration_total' => '总计工期',
            'duration_used' => '已用工期',
            'start_date' => '开始时间',
            'acceptance_date' => '验收时间',
            'expire_date' => '合同到期时间',
            'status' => '状态',
            'created_at' => '创建时间',
            'created_by' => '创建人',
            'updated_at' => '修改时间',
            'updated_by' => '修改人',
        ];
    }
}
