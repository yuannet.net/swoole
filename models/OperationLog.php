<?php

namespace app\models;

/**
 * This is the model class for table "{{%operation_log}}".
 *
 * @property int $id
 * @property string|null $module 模块名
 * @property string|null $controller 控制器名
 * @property string|null $action 方法名
 * @property string|null $orgin 源数据
 * @property string|null $data 现数据
 * @property string|null $change 变动数据
 * @property string|null $remark 备注
 * @property int $is_show 是否显示
 * @property string|null $url 跳转链接
 * @property int $status 状态 0删除 10启用
 * @property int $created_at 创建时间
 * @property int $created_by 创建人
 * @property int $updated_at 修改时间
 * @property int $updated_by 修改人
 */
class OperationLog extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%operation_log}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['orgin', 'data', 'change', 'remark', 'url'], 'string'],
            [['is_show', 'status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['module', 'controller', 'action'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'module' => '模块名',
            'controller' => '控制器名',
            'action' => '方法名',
            'orgin' => '源数据',
            'data' => '现数据',
            'change' => '变动数据',
            'remark' => '备注',
            'is_show' => '是否显示在最新动态',
            'url' => '跳转链接',
            'status' => '状态',
            'created_at' => '创建时间',
            'created_by' => '创建人',
            'updated_at' => '修改时间',
            'updated_by' => '修改人',
        ];
    }
}
