<?php

namespace app\models;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property int $id
 * @property string $username 用户名
 * @property string $password 用户密码
 * @property string $authKey
 * @property string $accessToken
 * @property int $is_super 是否为超级管理员 超管具有一切权限，并且可以访问所有产品，不是超管只能访问属于产品成员的产品
 * @property int $status 账户状态 0 停用 10启用
 * @property int $created_at 创建时间
 * @property int $created_by 创建人
 * @property int $updated_at 修改时间
 * @property int $updated_by 修改人
 */
class User extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username'], 'unique', 'message' => '用户名已经被占用'],
            [['username', 'password'], 'required'],
            [['is_super', 'status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['username', 'realname', 'mobile', 'qq', 'wechat'], 'string', 'max' => 50],
            [['address','password','email'], 'string', 'max' => 255],
            [['role_id'], 'string'],
            [['authKey', 'accessToken'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => '用户名',
            'password' => '密码',
            'authKey' => '登录授权',
            'accessToken' => '移动登录',
            'is_super' => '超级管理员',
            'status' => '状态',
            'realname' => '姓名',
            'mobile' => '手机',
            'qq' => 'qq号',
            'wechat' => '微信号',
            'address' => '住址',
            'created_at' => '创建时间',
            'created_by' => '创建人',
            'updated_at' => '修改时间',
            'updated_by' => '修改人',
        ];
    }
}
