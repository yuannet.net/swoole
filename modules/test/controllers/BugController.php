<?php

namespace app\modules\test\controllers;

use app\controllers\BaseController;
use app\services\Bug;
use app\services\File;
use app\services\Product;
use app\services\User;
use app\utils\Utils;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;


class BugController extends BaseController
{
    public function actionList($data = 0)
    {
        if (!$data) {
            return $this->render('list');
        }

        $request = Yii::$app->request;
        $post = Yii::$app->request->post();
        $get = Yii::$app->request->get();

        $where[] = ['bug.status' => 10];
        $where[] = ['bug.product_id' => Product::authProductId()]; //获取拥有Bug组成员的Bug,超管获取全部
        if ($get['product_id']) {
            $where[] = ['bug.product_id' => $get['product_id']]; //筛选单个产品
        }
        switch ($get['search_type']) {
            case 1: //所有
                break;
            case 2: //未完成
                $where[] = ['<>', 'bug.step', 3];
                break;
            case 3: //指给我
                $where[] = ['bug.develop_user_id' => Yii::$app->user->identity->id];
                break;
            case 4: //指给我未完成
                $where[] = ['<>', 'bug.step', 3];
                $where[] = ['bug.develop_user_id' => Yii::$app->user->identity->id];
                break;
            case 5: //我创建
                $where[] = ['bug.created_by' => Yii::$app->user->identity->id];
                break;
            case 6: //我创建未完成
                $where[] = ['<>', 'bug.step', 3];
                $where[] = ['bug.created_by' => Yii::$app->user->identity->id];
                break;
            case 7: //已完成
                $where[] = ['bug.step' => 3];
                break;
        }

        if ($search = Utils::formUnserialize(Yii::$app->request->post('searchData'))) {
            foreach ($search as $k => $v) {
                if ($v === '') {
                    continue;
                }
                switch ($k) {
                    case 'order_no':
                        $where[] = ['bug.id' => Utils::getNumeric($v, 'BUG')];
                        break;
                    case 'title':
                        $where[] = ['like', 'bug.title', $v];
                        break;
                    case 'create_user':
                        $user = User::find()->where(['or', ['like', 'username', $v], ['like', 'realname', $v]])->one();
                        $where[] = $where[] = ['bug.created_by' => $user ? $user->id : 0];
                        break;
                    case 'develop_user':
                        $user = User::find()->where(['or', ['like', 'username', $v], ['like', 'realname', $v]])->one();
                        $where[] = $where[] = ['bug.develop_user_id' => $user ? $user->id : 0];
                        break;
                    case 'step':
                        $where[] = ['bug.step' => $v];
                        break;
                }
            }
        }

        $result = Bug::listPage($where, 'bug.*', 'bug.id DESC', $request->post('page', 1), $request->post('limit', 10), ['product']);
        $return = ArrayHelper::toArray($result);

        foreach ($result['data'] as $key => $val) {
            $return['data'][$key]['order_no'] = Utils::generateOrderNo('BUG', $val->id);
            $return['data'][$key]['product_title'] = $val->product ? $val->product->title : '';
            $return['data'][$key]['create_user'] = $val->createUser ? $val->createUser->name : '';
            $return['data'][$key]['develop_user'] = $val->developUser ? $val->developUser->name : '未指派';
            $return['data'][$key]['created_at'] = date('Y-m-d H:i:s', $val->created_at);
            $return['data'][$key]['plan_start_date'] = $val->plan_start_date ? date('Y-m-d H:i:s', $val->plan_start_date) : '';
            $return['data'][$key]['plan_end_date'] = $val->plan_end_date ? date('Y-m-d H:i:s', $val->plan_end_date) : '';
            $return['data'][$key]['step'] = $val::getStep()[$val->step];
            $return['data'][$key]['step_code'] = $val->step;
            $return['data'][$key]['canEdit'] = $val->canEdit();
            $return['data'][$key]['canDel'] = $val->canDel();
        }
        return $this->asJson($return);
    }

    public function actionCreate()
    {
        $request = Yii::$app->request;
        $post = $request->post();

        if ($request->isPost) {
            $model = new Bug();
            $model->load($post, '');
            $model->plan_start_date = $post['plan_start_date'] ? strtotime($post['plan_start_date']) : 0;
            $model->plan_end_date = $post['plan_end_date'] ? strtotime($post['plan_end_date']) : 0;
            if ($model->develop_user_id) $model->step = 1;

            if ($model->save()) {
                if (isset($post['attachment'])) File::batchSave($post['attachment'], 'bug', $model->id);
                $model->sendEmail();
                $model->operation_log->create($this, "创建了Bug {$model->title}", Url::to(['view', 'id' => $model->id]), true);
                if ($model->develop_user_id) $model->operation_log->create($this, "指派了Bug {$model->title} 给 {$model->developUser->name}", Url::to(['view', 'id' => $model->id]), true);
                return $this->success([], '创建成功');
            }
            return $this->error($model->errors);
        }

        return $this->render('create');
    }

    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $post = $request->post();

        if ($request->isPost) {
            $model = Bug::findOne(['id' => $post['id'], 'status' => 10]);
            $model->load($post, '');
            $model->plan_start_date = $post['plan_start_date'] ? strtotime($post['plan_start_date']) : 0;
            $model->plan_end_date = $post['plan_end_date'] ? strtotime($post['plan_end_date']) : 0;
            if ($model->develop_user_id != $model->getOldAttribute('develop_user_id')) {
                $model->step = 1;
                $set_develop_user = true;
            }

            if ($model->save()) {
                if (isset($post['attachment'])) File::batchSave($post['attachment'], 'bug', $model->id);
                if (isset($post['attachment_remove'])) File::batchRemove($post['attachment_remove']);
                $model->operation_log->create($this, "编辑了Bug {$model->title}", Url::to(['view', 'id' => $model->id]), true);
                if (isset($set_develop_user) && $set_develop_user) {
                    $model->sendEmail();
                    $model->operation_log->create($this, "指派了Bug {$model->title} 给 {$model->developUser->name}", Url::to(['view', 'id' => $model->id]), true);
                }
                return $this->success([], '编辑成功');
            }
            return $this->error($model->errors);
        }

        if (!$model = Bug::findOne(['id' => $id, 'status' => [10, 20]])) {
            return $this->alert('未找到该Bug');
        }
        if (!$model->canEdit()) {
            return $this->alert('反正不是我的 我也不该要~~');
        }
        return $this->render('update', ['model' => $model]);
    }

    public function actionView($id)
    {
        if (!$model = Bug::findOne(['id' => $id, 'status' => 10])) {
            return $this->alert('未找到该Bug');
        }

        return $this->render('view', ['model' => $model]);
    }

    public function actionDelete($id)
    {
        if (!$model = Bug::findOne(['id' => $id, 'status' => [10]])) {
            return $this->error('未找到该Bug');
        }
        if (!$model->canDel()) {
            return $this->error('反正不是我的 我也不该要~~');
        }
        $model->status = 0;
        $model->save();
        $model->operation_log->create($this, "删除了Bug {$model->title}", '', true);
        return $this->success([], '删除成功');
    }

    /**
     * Bug指派
     * @method actionSetDevelopUser
     * @return \yii\web\Response
     * @author ZhangYuan 574368565@qq.com
     * @time 2021/1/28 8:45
     */
    public function actionSetDevelopUser()
    {
        $request = Yii::$app->request;
        $post = $request->post();

        $bug_id = $post['bug_id'];
        $checked = $post['checked'];

        if (!$bug = Bug::findOne(['id' => $bug_id, 'status' => 10])) {
            return $this->error('未找到该Bug');
        }

        if ($bug->step == 3) {
            return $this->error('Bug完成不能改派');
        }
        if (!$bug->canEdit()) {
            return $this->error('反正不是我的 我也不该要~~');
        }

        $bug->develop_user_id = $checked[0]['id'];
        $bug->step = 1;

        if (!$bug->save()) return $this->error($bug->errors);
        $bug->sendEmail();
        $bug->operation_log->create($this, "指派了Bug {$bug->title} 给 {$bug->developUser->name}", Url::to(['view', 'id' => $bug->id]), true);
        return $this->success([], 'Bug指派成功');
    }

    /**
     * 切换Bug状态
     * @method actionSwitchStep
     * @param $bug_id
     * @return \yii\web\Response
     * @author ZhangYuan 574368565@qq.com
     * @time 2021/1/28 8:45
     */
    public function actionSwitchStep($bug_id)
    {
        if (!$bug = Bug::findOne(['id' => $bug_id, 'status' => 10])) {
            return $this->error('未找到该Bug');
        }

        $bug->step = $bug->step + 1;
        $bug->save();
        $bug->operation_log->create($this, "更新了Bug {$bug->title} 进度为 {$bug::getStep()[$bug->step]}", Url::to(['view', 'id' => $bug->id]), true);
        return $this->success([], '操作成功');
    }
}
