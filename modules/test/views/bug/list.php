<?php

use app\services\Product;
use yii\helpers\Url;

$product_id = Yii::$app->request->get('product_id', 0);
$search_type = Yii::$app->request->get('search_type', 2);
?>

<style>
    .layui-btn-primary {
        font-size: 14px;
        margin-top: -3px;
    }
</style>

<div class="layui-fluid layui-anim layui-anim-fadein layui-col-md-offset1 layui-col-md10">
    <div class="layui-card">
        <div class="layui-card-header" style="padding-left: 0px;height:auto">
            <form action="" class="layui-form" style="margin-left: 10px;">
                <div class="layui-inline" style="width:175px">
                    <select id="product_id" lay-filter="switch_product" lay-search>
                        <?php foreach (Product::productStatistics() as $key => $val): ?>
                            <option value="<?= $val['id'] ?>" <?= Yii::$app->request->get('product_id', 0) == $val['id'] ? 'selected' : '' ?>><?= $val['title'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="layui-inline">
                    <a href="<?= Url::to(['list', 'search_type' => 1, 'product_id' => $product_id]) ?>"
                       class="layui-btn layui-btn-sm <?= $search_type == 1 ? 'layui-btn-normal' : 'layui-btn-primary' ?>">所有</a>

                    <a href="<?= Url::to(['list', 'search_type' => 2, 'product_id' => $product_id]) ?>"
                       class="layui-btn layui-btn-sm <?= $search_type == 2 ? 'layui-btn-normal' : 'layui-btn-primary' ?>"">未关闭</a>

                    <a href="<?= Url::to(['list', 'search_type' => 3, 'product_id' => $product_id]) ?>"
                       class="layui-btn layui-btn-sm <?= $search_type == 3 ? 'layui-btn-normal' : 'layui-btn-primary' ?>"">指给我</a>

                    <a href="<?= Url::to(['list', 'search_type' => 4, 'product_id' => $product_id]) ?>"
                       class="layui-btn layui-btn-sm <?= $search_type == 4 ? 'layui-btn-normal' : 'layui-btn-primary' ?>"">指给我未关闭</a>

                    <a href="<?= Url::to(['list', 'search_type' => 5, 'product_id' => $product_id]) ?>"
                       class="layui-btn layui-btn-sm <?= $search_type == 5 ? 'layui-btn-normal' : 'layui-btn-primary' ?>"">我创建</a>

                    <a href="<?= Url::to(['list', 'search_type' => 6, 'product_id' => $product_id]) ?>"
                       class="layui-btn layui-btn-sm <?= $search_type == 6 ? 'layui-btn-normal' : 'layui-btn-primary' ?>"">我创建未关闭</a>

                    <a href="<?= Url::to(['list', 'search_type' => 7, 'product_id' => $product_id]) ?>"
                       class="layui-btn layui-btn-sm <?= $search_type == 7 ? 'layui-btn-normal' : 'layui-btn-primary' ?>"">已关闭</a>
                </div>
            </form>
        </div>
        <div class="layui-card-body full-height" data-free="91">
            <table id="dataTable" lay-filter="dataTable"></table>
        </div>
    </div>

    <!--搜索栏-->
    <script type="text/html" id="search-toolbar">
        <form class="search-form layui-form" for-table-filter="dataTable">
            <div class="layui-show-xs-block hidden">
                <a href="javascript:void(0)" class="layui-btn layui-btn-sm layui-btn-primary toolbar-search-show">搜索</a>
            </div>

            <div class="layui-hide-xs toolbar-search-area">
                <div class="layui-inline">
                    <input type="text" name="order_no" placeholder="Bug编号"
                           class="layui-input">
                </div>

                <div class="layui-inline">
                    <input type="text" name="title" placeholder="Bug名称"
                           class="layui-input">
                </div>

                <div class="layui-inline">
                    <input type="text" name="create_user" placeholder="创建者"
                           class="layui-input">
                </div>

                <div class="layui-inline">
                    <input type="text" name="develop_user" placeholder="指派给"
                           class="layui-input">
                </div>

                <div class="layui-inline">
                    <select name="step" id="">
                        <option value="">进度</option>
                        <?php foreach (\app\services\Demand::getStep() as $key => $val): ?>
                            <option value="<?= $key ?>"><?= $val ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="layui-inline">
                    <a href="javascript:;" class="layui-btn layui-btn-sm layui-btn-normal toolbar-search-btn">
                        <i class="layui-icon layui-icon-search"></i>搜索
                    </a>

                    <a href="javascript:;" class="layui-btn layui-btn-sm layui-btn-normal toolbar-search-reset">
                        <i class="layui-icon layui-icon-refresh-1"></i>重置搜索条件
                    </a>

                    <a href="javascript:;" class="layui-btn layui-btn-sm layui-btn-normal iframe-layer"
                       data-url="<?= Url::to(['/test/bug/create', 'product_id' => $product_id]) ?>"
                       data-title="提Bug" data-reload="dataTable">
                        <i class="layui-icon layui-icon-addition"></i>提Bug
                    </a>
                </div>
            </div>
        </form>
    </script>

    <!--操作栏-->
    <script type="text/html" id="dataTableHandle">
        <a class="layui-btn layui-btn-xs layui-btn-normal iframe-layer"
           data-url="<?= Url::to(['/test/bug/view']) ?>&id={{= d.id }}"
           data-reload="dataTable" data-title="详情">详情</a>

        {{# if(d.canEdit){ }}
        <a class="layui-btn layui-btn-xs layui-btn-warm iframe-layer"
           data-url="<?= Url::to(['/test/bug/update']) ?>&id={{= d.id }}" data-title="编辑"
           data-reload="dataTable">编辑</a>
        {{# } }}

        {{# if(d.canDel){ }}
        <a class="layui-btn layui-btn-xs layui-btn-danger confirm-layer"
           data-url="<?= Url::to(['/test/bug/delete']) ?>&id={{= d.id }}" data-reload="dataTable">删除</a>
        {{# } }}
    </script>
</div>

<?php $this->beginBlock('js') ?>
<script>
    layui.table.render({
        where: {},
        elem: '#dataTable',
        url: "<?= Url::to(['list', 'data' => 1, 'search_type' => $search_type, 'product_id' => $product_id]) ?>", //数据接口
        method: "post",
        page: true,
        customHeight: "100%",
        limit: 20,
        toolbar: '#search-toolbar',
        defaultToolbar: [],
        smartReloadModel: true,
        cols: [[
            {
                fixed: 'handle',
                width: 130,
                title: "操作",
                align: 'center',
                toolbar: '#dataTableHandle'
            },
            {
                field: 'level', width: 65, title: "优先级", align: "center", templet: function (d) {
                    var className = 'layui-btn layui-btn-xs ';
                    switch (d.level) {
                        case 1:
                            className += 'layui-btn-danger';
                            break;
                        case 2:
                            className += 'layui-btn-warm';
                            break;
                        case 3:
                            className += 'layui-btn-normal';
                            break;
                        case 4:
                            className += '';
                            break;
                    }
                    var html = "<a class='" + className + "' style='border-radius: 20px;padding: 0px 8px;font-weight: bold;'>" + d.level + "</a>";
                    return html;
                }
            },
            {field: 'order_no', width: 120, title: "Bug编号"},
            {field: 'product_title', minWidth: 180, title: "所属产品"},
            {field: 'title', width: 180, title: "Bug名称"},
            {field: 'create_user', width: 120, title: "创建者"},
            {
                field: 'develop_user', width: 120, title: "指派", templet: function (d) {
                    //谁发起谁指派
                    if (d.canEdit) {
                        var html = "<a href='javascript:void(0)' class='layui-btn layui-btn-xs layui-btn-primary iframe-layer' " +
                            "data-url='<?= Url::to(['/common/select/user-list', 'bug_id' => ''])?>" + d.id + "' data-reload='dataTable'>" +
                            "<i class='layui-icon layui-icon-user'></i>" + d.develop_user + "</a>";
                        return html;
                    } else {
                        return d.develop_user;
                    }
                }
            },
            {
                field: 'step', width: 80, title: "进度", align: "center", templet: function (d) {
                    var className = 'layui-btn layui-btn-xs ';
                    switch (d.step_code) {
                        case 0:
                            className += 'layui-btn-danger';
                            break;
                        case 1:
                            className += 'layui-btn-warm';
                            break;
                        case 2:
                            className += 'layui-btn-normal';
                            break;
                        case 3:
                            className += '';
                            break;
                    }
                    var html = "<a class='" + className + "'>" + d.step + "</a>";
                    return html;
                }
            },
            {field: 'plan_start_date', width: 180, title: "计划开始时间"},
            {field: 'plan_end_date', width: 180, title: "计划完成时间"},
            {field: 'created_at', width: 180, title: "创建时间"},
        ]]
    });

    /**
     * 选择产品后跳转页面
     */
    layui.form.on('select(switch_product)', function (data) {
        var product_id = data.value;
        window.location.replace("<?= Url::to(['list', 'search_type' => $search_type, 'product_id' => ''])?>" + product_id);
    });

    /**
     * 选择完用户回调
     * @param checked
     * @param get_params
     */
    function chooseUserCall(checked, get_params) {
        layui.layer.close(layui.layer.index);
        var data = {"bug_id": get_params.bug_id, "checked": checked};
        $.ajax({
            type: "POST",
            data: data,
            url: "<?= Url::to(['/test/bug/set-develop-user'])?>",
            success: function (res) {
                // if (res.error == 0) {
                //     layui.layer.close(layui.layer.index)
                // }

                layui.layer.msg(res.msg);
            }
        });
    }
</script>
<?php $this->endBlock() ?>
