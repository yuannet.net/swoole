<?php

use yii\helpers\Url;

?>

<div class="layui-fluid layui-anim layui-anim-fadein">
    <div class="layui-card">
        <div class="layui-card-header">
            <b>修改Bug</b>
        </div>
        <div class="layui-card-body">
            <form class="layui-form" action="" method="post">
                <div class="layui-form-item">
                    <label class="layui-form-label">当前进度</label>
                    <div class="layui-input-block">
                        <a href="javascript:void(0)" class="layui-btn layui-btn-xs <?= $model->stepColor ?>"
                           style="margin-top: 5px">
                            <?= $model::getStep()[$model->step] ?>
                        </a>
                    </div>
                </div>
                <div class="layui-row">
                    <div class="layui-col-sm6">
                        <div class="layui-form-item">
                            <label class="layui-form-label must">所属产品</label>
                            <div class="layui-input-block">
                                <select name="product_id" lay-verify="required" autocomplete="off">
                                    <option value="">请选择所属产品</option>
                                    <?php foreach (\app\services\Product::productStatistics(false) as $key => $val): ?>
                                        <option value="<?= $val['id'] ?>" <?= $model->product_id == $val['id'] ? 'selected' : '' ?>><?= $val['title'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm6">
                        <div class="layui-form-item">
                            <label class="layui-form-label must">Bug名称</label>
                            <div class="layui-input-block">
                                <input type="text" name="title" lay-verify="required" placeholder="请输入Bug名称"
                                       autocomplete="off" class="layui-input" value="<?= $model->title ?>">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="layui-row">
                    <div class="layui-col-sm6">
                        <div class="layui-form-item">
                            <label class="layui-form-label">优先级</label>
                            <div class="layui-input-block">
                                <select name="level" id="">
                                    <option value="1" <?= $model->level == 1 ? 'selected' : '' ?>>1</option>
                                    <option value="2" <?= $model->level == 2 ? 'selected' : '' ?>>2</option>
                                    <option value="3" <?= $model->level == 3 ? 'selected' : '' ?>>3</option>
                                    <option value="4" <?= $model->level == 4 ? 'selected' : '' ?>>4</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="layui-row">
                    <div class="layui-col-sm6">
                        <div class="layui-form-item">
                            <label class="layui-form-label">计划开始时间</label>
                            <div class="layui-input-block">
                                <input type="text" name="plan_start_date" placeholder="请选择计划开始时间"
                                       class="layui-input auto-date" date-type="datetime" readonly
                                       value="<?= $model->plan_start_date ? date('Y-m-d H:i:s', $model->plan_start_date) : '' ?>">
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm6">
                        <div class="layui-form-item">
                            <label class="layui-form-label">计划完成时间</label>
                            <div class="layui-input-block">
                                <input type="text" name="plan_end_date" placeholder="请选择计划完成时间"
                                       class="layui-input auto-date" date-type="datetime" readonly
                                       value="<?= $model->plan_end_date ? date('Y-m-d H:i:s', $model->plan_end_date) : '' ?>">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">Bug描述</label>
                    <div class="layui-input-block">
                        <textarea name="remark" class="layui-textarea"
                                  placeholder="请输入Bug描述"><?= $model->remark ?></textarea>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">验收标准</label>
                    <div class="layui-input-block">
                        <textarea name="acceptance_remark" class="layui-textarea"
                                  placeholder="请输入验收标准"><?= $model->acceptance_remark ?></textarea>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">指派给</label>
                    <div class="layui-input-block">
                        <div class="layui-input-inline" style="margin-right: 0px;margin-left: 0px">
                            <input type="text" class="layui-input" id="develop_user" readonly
                                   value="<?= $model->developUser ? $model->developUser->name : '' ?>">
                            <input type="hidden" class="layui-input" id="develop_user_id" name="develop_user_id"
                                   value="<?= $model->develop_user_id ?>">
                        </div>
                        <div class="layui-input-inline" style="margin-right: 0px;margin-left: 0px">
                            <a href='javascript:void(0)'
                               class='layui-btn layui-btn-sm layui-btn-primary iframe-layer'
                               data-url='<?= Url::to(['/common/select/user-list']) ?>'>
                                <i class='layui-icon layui-icon-user'></i>选择用户</a>
                        </div>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">附件</label>
                    <div class="layui-input-block">
                        <a href="javascript:void(0)" class="layui-btn layui-btn-sm layui-btn-primary upload-file-btn"
                           data-name="attachment" data-id="<?= $model->id ?>" data-module="bug">
                            <i class="layui-icon layui-icon-addition"></i>添加文件
                        </a>
                    </div>
                </div>

                <div class="layui-form-item">
                    <input type="hidden" name="id" value="<?= $model->id ?>">
                    <div class="layui-input-block">
                        <div class="layui-block">
                            <button class="layui-btn layui-btn-normal layui-btn-sm ajax-submit" lay-submit
                                    lay-filter="submit" after-success="close">立即提交
                            </button>
                        </div>
                        <div class="layui-form-mid layui-word-aux">Bug谁发起，谁管理（编辑/删除），谁安排（指派）</div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php $this->beginBlock('js') ?>
<script>
    function chooseUserCall(checked, get_params) {
        $('#develop_user').val(checked[0].realname ? checked[0].realname : checked[0].username);
        $('#develop_user_id').val(checked[0].id);
        layui.layer.close(layui.layer.index)
    }
</script>
<?php $this->endBlock() ?>
