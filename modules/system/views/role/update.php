<?php

?>

<div class="layui-fluid layui-anim layui-anim-fadein">
    <div class="layui-card">
        <div class="layui-card-header">
            <b>编辑角色</b>
        </div>
        <div class="layui-card-body">
            <form class="layui-form" action="" method="post">

                <div class="layui-form-item">
                    <label class="layui-form-label must">角色名称</label>
                    <div class="layui-input-block">
                        <input type="text" name="title" lay-verify="required" placeholder="请输入角色名称"
                               autocomplete="off" class="layui-input" value="<?= $model->title?>">
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">角色权限</label>
                    <div class="layui-input-block">
                        <div class="layui-collapse">
                            <?php foreach ($perm as $module_name => $module): ?>
                                <div class="layui-colla-item">
                                    <h2 class="layui-colla-title">
                                        <div class="layui-inline" style="margin-top: -5px">
                                            <input type="checkbox" lay-skin="primary" title="<?= $module['text'] ?>"
                                                   lay-filter="check_all">
                                        </div>
                                    </h2>
                                    <div class="layui-colla-content layui-show">
                                        <?php unset($module['text']);foreach ($module as $ctrl_name => $ctrl): ?>
                                            <div class="layui-block">
                                                <label class="layui-form-label"><?= $ctrl['text'] ?></label>
                                                <div class="layui-input-block">
                                                    <?php unset($ctrl['text']);foreach ($ctrl as $action_name => $action): ?>
                                                        <?php $action_arr = []; if (strpos($action_name, ',')) {
                                                            foreach (explode(',', $action_name) as $action_str) {
                                                                $action_arr[] = "$module_name/$ctrl_name/$action_str";
                                                            }
                                                        } else {
                                                            $action_arr[] = "$module_name/$ctrl_name/$action_name";
                                                        } ?>
                                                        <input type="checkbox" lay-skin="primary" <?= array_intersect($role_perm,$action_arr)?'checked':''?>
                                                               value="<?= implode(',',$action_arr) ?>"
                                                               title="<?= $action ?>" name="perm[]">
                                                    <?php endforeach; ?>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">状态</label>
                    <div class="layui-input-block">
                        <select name="status" id="">
                            <?php foreach (\app\services\User::getStatus() as $key => $val): ?>
                                <option value="<?= $key ?>" <?= $model->status==$key?'selected':''?>><?= $val ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>

                <div class="layui-form-item">
                    <input type="hidden" name="id" value="<?= $model->id?>">
                    <div class="layui-input-block">
                        <button class="layui-btn layui-btn-normal layui-btn-sm ajax-submit" lay-submit
                                lay-filter="submit" after-success="close">立即提交
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<?php $this->beginBlock('js') ?>
<script>
    layui.form.on('checkbox(check_all)', function (data) {
        data.othis.parents('.layui-colla-item').find(".layui-colla-content input[type='checkbox']").each(function (index, item) {
            item.checked = data.elem.checked;
        })

        layui.form.render('checkbox');
        event.stopPropagation();
    });
</script>
<?php $this->endBlock() ?>
