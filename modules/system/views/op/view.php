<?php

?>
<style>
    .remark-area {
        position: relative;
        margin: 5px auto auto 15px;
        padding: 5px 15px;
        border: 1px solid #e2e2e2;
        border-radius: 1px;
        background: #F0F0F0;
    }

    .remark-area-toobar {
        position: absolute;
        top: 5px;
        right: 15px;
        display: none;
    }

    .upload-file-remove-btn {
        display: none
    }
</style>

<div class="layui-fluid layui-anim layui-anim-fadein">
    <div class="layui-card">
        <div class="layui-card-header">
            <b>操作日志详情</b>
        </div>
        <div class="layui-card-body">
            <form class="layui-form" action="" method="post">
                <div class="layui-form-item">
                    <label class="layui-form-label">操作描述</label>
                    <div class="layui-input-block">
                        <input type="text" class="layui-input" value="<?= $model->remark ?>" disabled>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">操作人</label>
                    <div class="layui-input-block">
                        <input type="text" class="layui-input" value="<?= $model->createUser->name ?>" disabled>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">操作时间</label>
                    <div class="layui-input-block">
                        <input type="text" class="layui-input" value="<?= date('Y-m-d H:i:s', $model->created_at) ?>"
                               disabled>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">关联操作</label>
                    <div class="layui-input-block">
                        <?php if($model->url):?>
                            <a href="javascript:void(0)" class="layui-btn layui-btn-xs layui-btn-normal iframe-layer" data-url="<?= $model->url?>">查看信息</a>
                        <?php else:?>
                            <a href="javascript:void(0)" class="layui-btn layui-btn-xs layui-btn-primary">无关联</a>
                        <?php endif;?>
                    </div>
                </div>
            </form>
        </div>
        <div class="layui-card-header">
            <b>变动数据</b>
        </div>
        <div class="layui-card-body">
            <div class="layui-row">
                <div class="layui-col-sm5">
                    <table class="layui-table">
                        <thead>
                        <tr>
                            <td colspan="2" class="text-center"><b>原数据</b></td>
                        </tr>
                        <tr>
                            <td style="width: 50%">字段</td>
                            <td style="width: 50%">内容</td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if ($model->orginInfo): ?>
                            <?php foreach ($model->orginInfo as $col => $val): ?>
                                <tr>
                                    <td><?= $col ?></td>
                                    <td><?= $val ?></td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="2" class="text-center">null</td>
                            </tr>
                        <?php endif; ?>
                        </tbody>
                    </table>
                </div>

                <div class="layui-col-sm2 text-center" style="padding-top: 55px">
                    <i class="layui-icon layui-icon-right" style="font-size: 30px"></i>
                </div>

                <div class="layui-col-sm5">
                    <table class="layui-table">
                        <thead>
                        <tr>
                            <td colspan="2" class="text-center"><b>现数据</b></td>
                        </tr>
                        <tr>
                            <td style="width: 50%">字段</td>
                            <td style="width: 50%">内容</td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if ($model->dataInfo): ?>
                            <?php foreach ($model->dataInfo as $col => $val): ?>
                                <tr>
                                    <td><?= $col ?></td>
                                    <td><?= $val ?></td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="2" class="text-center">null</td>
                            </tr>
                        <?php endif; ?>
                        </tbody>
                    </table>
                </div>

                <div class="layui-col-sm12">
                    <table class="layui-table">
                        <thead>
                        <tr>
                            <td colspan="2" class="text-center"><b>变更数据</b></td>
                        </tr>
                        <tr>
                            <td style="width: 50%">字段</td>
                            <td style="width: 50%">内容</td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if ($model->changeInfo): ?>
                            <?php foreach ($model->changeInfo as $col => $val): ?>
                                <tr>
                                    <td><?= $col ?></td>
                                    <td><?= $val ?></td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="2" class="text-center">null</td>
                            </tr>
                        <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<?php $this->beginBlock('js') ?>
<script>
    function reload() {
        location.reload();
    }

    $('.remark-area').mouseover(function () {
        $(this).find('.remark-area-toobar').show();
    }).mouseleave(function () {
        $(this).find('.remark-area-toobar').hide();
    })
</script>
<?php $this->endBlock() ?>
