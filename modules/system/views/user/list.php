<?php

use yii\helpers\Url;

?>

<div class="layui-fluid layui-anim layui-anim-fadein layui-col-md-offset1 layui-col-md10">
    <div class="layui-card">
        <div class="layui-card-header">
            <b>人员列表</b>
        </div>
        <div class="layui-card-body full-height" data-free="90">
            <table id="dataTable" lay-filter="dataTable"></table>
        </div>
    </div>

    <!--搜索栏-->
    <script type="text/html" id="search-toolbar">
        <form class="search-form layui-form" for-table-filter="dataTable">
            <div class="layui-show-xs-block hidden">
                <a href="javascript:void(0)" class="layui-btn layui-btn-sm layui-btn-primary toolbar-search-show">搜索</a>
            </div>

            <div class="layui-hide-xs toolbar-search-area">
                <div class="layui-inline">
                    <input type="text" name="order_no" placeholder="人员编号"
                           class="layui-input">
                </div>

                <div class="layui-inline">
                    <input type="text" name="username" placeholder="用户名"
                           class="layui-input">
                </div>

                <div class="layui-inline">
                    <input type="text" name="realname" placeholder="姓名"
                           class="layui-input">
                </div>

                <div class="layui-inline">
                    <input type="text" name="mobile" placeholder="手机号"
                           class="layui-input">
                </div>

                <div class="layui-inline">
                    <input type="text" name="qq" placeholder="QQ号"
                           class="layui-input">
                </div>

                <div class="layui-inline">
                    <input type="text" name="wechat" placeholder="微信号"
                           class="layui-input">
                </div>

                <div class="layui-inline">
                    <input type="text" name="email" placeholder="邮箱"
                           class="layui-input">
                </div>

                <div class="layui-inline">
                    <input type="text" name="address" placeholder="住址"
                           class="layui-input">
                </div>

                <div class="layui-inline">
                    <select name="status">
                        <option value="">状态</option>
                        <?php foreach (\app\services\User::getStatus() as $key => $val): ?>
                            <option value="<?= $key ?>"><?= $val ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="layui-inline">
                    <a href="javascript:;" class="layui-btn layui-btn-sm layui-btn-normal toolbar-search-btn">
                        <i class="layui-icon layui-icon-search"></i>搜索
                    </a>

                    <a href="javascript:;" class="layui-btn layui-btn-sm layui-btn-normal toolbar-search-reset">
                        <i class="layui-icon layui-icon-refresh-1"></i>重置搜索条件
                    </a>

                    <a href="javascript:;" class="layui-btn layui-btn-sm layui-btn-normal iframe-layer"
                       data-url="<?= Url::to(['/system/user/create']) ?>"
                       data-title="添加人员" data-reload="dataTable">
                        <i class="layui-icon layui-icon-addition"></i>添加人员
                    </a>
                </div>
            </div>
        </form>
    </script>

    <!--操作栏-->
    <script type="text/html" id="dataTableHandle">
        <a class="layui-btn layui-btn-xs layui-btn-normal iframe-layer"
           data-url="<?= Url::to(['/system/user/view']) ?>&id={{= d.id }}"
           data-reload="dataTable" data-title="详情">详情</a>

        <a class="layui-btn layui-btn-xs layui-btn-warm iframe-layer"
           data-url="<?= Url::to(['/system/user/update']) ?>&id={{= d.id }}" data-title="编辑"
           data-reload="dataTable">编辑</a>

        <a class="layui-btn layui-btn-xs layui-btn-danger confirm-layer"
           data-url="<?= Url::to(['/system/user/delete']) ?>&id={{= d.id }}" data-reload="dataTable">删除</a>
    </script>
</div>

<?php $this->beginBlock('js') ?>
<script>
    layui.table.render({
        where: {},
        elem: '#dataTable',
        url: "<?= Url::to(['list', 'data' => 1]) ?>", //数据接口
        method: "post",
        page: true,
        customHeight: "100%",
        limit: 20,
        toolbar: '#search-toolbar',
        defaultToolbar: [],
        smartReloadModel: true,
        cols: [[
            {
                fixed: 'handle',
                width: 130,
                title: "操作",
                align: 'center',
                toolbar: '#dataTableHandle'
            },
            {field: 'order_no', width: 120, title: "人员编号"},
            {field: 'username', width: 120, title: "用户名"},
            {field: 'realname', width: 120, title: "姓名"},
            {field: 'mobile', width: 120, title: "手机号"},
            {field: 'qq', width: 100, title: "QQ号"},
            {field: 'wechat', width: 160, title: "微信号"},
            {field: 'email', width: 160, title: "邮箱"},
            {field: 'address', width: 260, title: "住址"},
            {field: 'status', width: 50, title: "状态"},
            {field: 'created_at', width: 180, title: "创建时间"},
        ]]
    });
</script>
<?php $this->endBlock() ?>
