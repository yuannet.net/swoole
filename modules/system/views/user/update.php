<?php

?>

<div class="layui-fluid layui-anim layui-anim-fadein">
    <div class="layui-card">
        <div class="layui-card-header">
            <b>编辑人员</b>
        </div>
        <div class="layui-card-body">
            <form class="layui-form" action="" method="post">
                <div class="layui-row">
                    <div class="layui-col-sm6">
                        <div class="layui-form-item">
                            <label class="layui-form-label must">用户名</label>
                            <div class="layui-input-block">
                                <input type="text" name="username" lay-verify="required" placeholder="请输入用户名"
                                       autocomplete="off" class="layui-input" value="<?= $model->username ?>">
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm6">
                        <div class="layui-form-item">
                            <label class="layui-form-label">密码</label>
                            <div class="layui-input-block">
                                <input type="password" name="password" placeholder="密码为空则不进行修改"
                                       autocomplete="off" class="layui-input">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="layui-row">
                    <div class="layui-col-sm6">
                        <div class="layui-form-item">
                            <label class="layui-form-label must">姓名</label>
                            <div class="layui-input-block">
                                <input type="text" name="realname" lay-verify="required" placeholder="请输入姓名"
                                       autocomplete="off" class="layui-input" value="<?= $model->realname ?>">
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm6">
                        <div class="layui-form-item">
                            <label class="layui-form-label">手机号</label>
                            <div class="layui-input-block">
                                <input type="text" name="mobile" placeholder="请输入手机号"
                                       autocomplete="off" class="layui-input" value="<?= $model->mobile ?>">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="layui-row">
                    <div class="layui-col-sm6">
                        <div class="layui-form-item">
                            <label class="layui-form-label">QQ号</label>
                            <div class="layui-input-block">
                                <input type="text" name="qq" placeholder="请输入QQ号"
                                       autocomplete="off" class="layui-input" value="<?= $model->qq ?>">
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm6">
                        <div class="layui-form-item">
                            <label class="layui-form-label">微信号</label>
                            <div class="layui-input-block">
                                <input type="text" name="wechat" placeholder="请输入微信号"
                                       autocomplete="off" class="layui-input" value="<?= $model->wechat ?>">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">邮箱</label>
                    <div class="layui-input-block">
                        <input type="text" name="email" placeholder="请输入邮箱（邮箱用于任务指派通知）"
                               autocomplete="off" class="layui-input" value="<?= $model->email ?>">
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">住址</label>
                    <div class="layui-input-block">
                        <textarea name="address" class="layui-textarea"
                                  placeholder="请输入住址"><?= $model->address ?></textarea>
                    </div>
                </div>

                <div class="layui-row">
                    <div class="layui-col-sm6">
                        <div class="layui-form-item">
                            <label class="layui-form-label">超管（可查看所有数据）</label>
                            <div class="layui-input-block">
                                <select name="is_super">
                                    <option value="0" <?= $model->is_super === 0 ? 'selected' : '' ?>>否</option>
                                    <option value="1" <?= $model->is_super === 1 ? 'selected' : '' ?>>是</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm6">
                        <div class="layui-form-item">
                            <label class="layui-form-label">状态</label>
                            <div class="layui-input-block">
                                <select name="status" id="">
                                    <?php foreach (\app\services\User::getStatus() as $key => $val): ?>
                                        <option value="<?= $key ?>" <?= $model->status === $key ? 'selected' : '' ?>><?= $val ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">所属产品</label>
                    <div class="layui-input-block" style="">
                        <?php foreach (\app\services\Product::getAllProduct() as $val): ?>
                            <input type="checkbox" name="product[]" title="<?= $val->title ?>" value="<?= $val->id ?>"
                                   lay-skin="primary" <?= in_array($val->id, $product_array) ? 'checked' : '' ?>>
                        <?php endforeach; ?>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">权限角色</label>
                    <div class="layui-input-block" style="">
                        <?php foreach (\app\services\Role::findAll(['status' => [10, 20]]) as $val): ?>
                            <input type="checkbox" name="role_id[]" title="<?= $val->title ?>" value="<?= $val->id ?>"
                                   lay-skin="primary" <?= $model->role_id && in_array($val->id, explode(',', $model->role_id)) ? 'checked' : '' ?>>
                        <?php endforeach; ?>
                    </div>
                </div>

                <div class="layui-form-item">
                    <input type="hidden" name="id" value="<?= $model->id ?>">
                    <div class="layui-input-block">
                        <button class="layui-btn layui-btn-normal layui-btn-sm ajax-submit" lay-submit
                                lay-filter="submit" after-success="close">立即提交
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<?php $this->beginBlock('js') ?>
<?php $this->endBlock() ?>
