<?php

namespace app\modules\system\controllers;

use app\controllers\BaseController;
use app\services\Perm;
use app\services\Role;
use app\utils\Utils;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;


class RoleController extends BaseController
{
    public function actionList($data = 0)
    {
        if (!$data) {
            return $this->render('list');
        }

        $request = Yii::$app->request;

        $where[] = ['status' => [10, 20]];

        if ($search = Utils::formUnserialize($request->post('searchData'))) {
            foreach ($search as $k => $v) {
                if ($v === '') {
                    continue;
                }
                switch ($k) {
                    case 'order_no':
                        $where[] = ['id' => Utils::getNumeric($v, 'ROLE')];
                        break;
                    default:
                        $where[] = ['like', $k, $v];
                        break;
                }
            }
        }

        $result = Role::listPage($where, '*', 'id DESC', $request->post('page', 1), $request->post('limit', 10));
        $return = ArrayHelper::toArray($result);

        foreach ($result['data'] as $key => $val) {
            $return['data'][$key]['order_no'] = Utils::generateOrderNo('ROLE', $val->id);
            $return['data'][$key]['created_at'] = date('Y-m-d H:i:s', $val->created_at);
            $return['data'][$key]['status'] = $val::getStatus()[$val->status];
        }
        return $this->asJson($return);
    }

    public function actionCreate()
    {
        $request = Yii::$app->request;
        $post = $request->post();

        if ($request->isPost) {
            $model = new Role();
            $post['perm'] = isset($post['perm']) ? implode(',', $post['perm']) : '';
            $model->load($post, '');
            if (!$model->save()) return $this->error($model->errors);

            $model->operation_log->create($this, "创建了角色 {$model->title}", Url::to(['view', 'id' => $model->id]));
            return $this->success([], '创建成功');
        }

        $perm = Perm::getPerm();
        return $this->render('create', ['perm' => $perm]);
    }

    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $post = $request->post();

        if ($request->isPost) {
            $model = Role::findOne(['id' => $post['id'], 'status' => [10, 20]]);
            $post['perm'] = isset($post['perm']) ? implode(',', $post['perm']) : '';
            $model->load($post, '');
            if (!$model->save()) return $this->error($model->errors);

            $model->operation_log->create($this, "编辑了角色 {$model->title}", Url::to(['view', 'id' => $model->id]));
            return $this->success([], '编辑成功');
        }

        if (!$model = Role::findOne(['id' => $id, 'status' => [10, 20]])) {
            return $this->alert('未找到该角色');
        }

        $perm = Perm::getPerm();
        $role_perm = $model->perm ? explode(',', $model->perm) : [];
        return $this->render('update', ['model' => $model, 'perm' => $perm, 'role_perm' => $role_perm]);
    }

    public function actionView($id)
    {
        if (!$model = Role::findOne(['id' => $id, 'status' => [10, 20]])) {
            return $this->alert('未找到该角色');
        }

        $perm = Perm::getPerm();
        $role_perm = $model->perm ? explode(',', $model->perm) : [];
        return $this->render('view', ['model' => $model, 'perm' => $perm, 'role_perm' => $role_perm]);
    }

    public function actionDelete($id)
    {
        if (!$model = Role::findOne(['id' => $id, 'status' => [10, 20]])) {
            return $this->error('未找到该角色');
        }
        $model->status = 0;
        $model->save();
        $model->operation_log->create($this, "删除了角色 {$model->title}");
        return $this->success([], '删除成功');
    }
}
