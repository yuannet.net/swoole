<?php

?>

<div class="layui-fluid layui-anim layui-anim-fadein">
    <div class="layui-card">
        <div class="layui-card-header">
            <b>添加备注</b>
        </div>
        <div class="layui-card-body">
            <form class="layui-form" action="" method="post">
                <div class="layui-form-item">
                    <label class="layui-form-label">备注</label>
                    <div class="layui-input-block">
                        <textarea name="remark" class="layui-textarea"><?= $model->remark ?></textarea>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">附件</label>
                    <div class="layui-input-block">
                        <a href="javascript:void(0)" class="layui-btn layui-btn-sm layui-btn-primary upload-file-btn"
                           data-name="attachment" data-id="<?= $model->id ?>" data-module="demand-record">
                            <i class="layui-icon layui-icon-addition"></i>添加文件
                        </a>
                    </div>
                </div>

                <div class="layui-form-item">
                    <input type="hidden" name="id" value="<?= Yii::$app->request->get('id') ?>">
                    <div class="layui-input-block">
                        <button class="layui-btn layui-btn-normal layui-btn-sm ajax-submit" lay-submit
                                lay-filter="submit" after-success="close">立即提交
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<?php $this->beginBlock('js') ?>

<?php $this->endBlock() ?>
