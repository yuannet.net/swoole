<?php

use yii\helpers\Url;

?>

<div class="layui-card">
    <div class="layui-card-header">
        <b>产品列表</b>
    </div>
    <div class="layui-card-body">
        <table id="dataTable" lay-filter="dataTable"></table>
    </div>
</div>

<!--搜索栏-->
<script type="text/html" id="search-toolbar">
    <form class="search-form layui-form" for-table-filter="dataTable">
        <div class="layui-show-xs-block hidden">
            <a href="javascript:void(0)" class="layui-btn layui-btn-sm layui-btn-primary toolbar-search-show">搜索</a>
        </div>

        <div class="layui-hide-xs toolbar-search-area">
            <div class="layui-inline">
                <input type="text" name="order_no" placeholder="产品编号"
                       class="layui-input">
            </div>

            <div class="layui-inline">
                <input type="text" name="title" placeholder="产品名称"
                       class="layui-input">
            </div>

            <div class="layui-inline">
                <select name="status" id="">
                    <option value="">状态</option>
                    <?php foreach (\app\services\Product::getStatus() as $key => $val): ?>
                        <option value="<?= $key ?>"><?= $val ?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <div class="layui-inline">
                <a href="javascript:;" class="layui-btn layui-btn-sm layui-btn-normal toolbar-search-btn">
                    <i class="layui-icon layui-icon-search"></i>搜索
                </a>

                <a href="javascript:;" class="layui-btn layui-btn-sm layui-btn-normal toolbar-search-reset">
                    <i class="layui-icon layui-icon-refresh-1"></i>重置搜索条件
                </a>

                <a href="javascript:;" class="layui-btn layui-btn-sm layui-btn-normal iframe-layer"
                   data-url="<?= Url::to(['/product/product/create']) ?>"
                   data-title="创建产品" data-reload="dataTable">
                    <i class="layui-icon layui-icon-addition"></i>创建产品
                </a>
            </div>
        </div>

    </form>
</script>

<!--操作栏-->
<script type="text/html" id="dataTableHandle">
    <a class="layui-btn layui-btn-xs layui-btn-normal iframe-layer"
       data-url="<?= Url::to(['/product/product/view']) ?>&id={{= d.id }}"
       data-reload="dataTable" data-title="详情">详情</a>

    <a class="layui-btn layui-btn-xs layui-btn-warm iframe-layer"
       data-url="<?= Url::to(['/product/product/update']) ?>&id={{= d.id }}" data-title="编辑"
       data-reload="dataTable">编辑</a>

    <a class="layui-btn layui-btn-xs layui-btn-danger confirm-layer"
       data-url="<?= Url::to(['/product/product/delete']) ?>&id={{= d.id }}" data-reload="dataTable">删除</a>
</script>