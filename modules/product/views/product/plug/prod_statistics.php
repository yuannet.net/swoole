<?php

?>

<style>
    .layui-card-body {
        padding: 15px;
    }

    .produce-list li {
        height: 30px;
        line-height: 30px;
        padding-left: 10px;
        border-left: 3px solid rgba(30, 159, 255, 0);
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
        cursor: pointer;
    }

    .li_this {
        border-left: 3px solid #1E9FFF !important;
        background: #F0F0F0;
    }

    .product-detail-view {
        padding-top: 20px;
        text-align: center;
    }
</style>


<!--产品统计-->
<div class="layui-card">
    <div class="layui-card-header layui-row">
        <b>产品统计</b>
    </div>
    <div class="layui-card-body">
        <div class="layui-row layui-col-space20">
<!--            产品菜单-->
            <div class="layui-col-sm4" style="padding-right: 0px;max-height:250px;overflow-y: auto">
                <ul class="produce-list">
                    <?php foreach ($product_info as $key => $val): ?>
                        <li class="<?= $key == 0 ? 'li_this' : '' ?>" for="product_<?= $key ?>"><?= $val['title'] ?></li>
                    <?php endforeach; ?>
                </ul>
            </div>

<!--            产品详情-->
            <div class="layui-col-sm8" id="product_detail" style="border-left: 1px solid #F0F0F0;min-height:250px">
                <?php foreach ($product_info as $key => $val): ?>
                <div class="layui-row product-detail-view <?= $key!=0?'hidden':''?>" id="product_<?= $key?>">
                    <div class="layui-col-sm6">
                        <p style="font-size: 18px">进行需求</p>
                        <p class="layuiadmin-big-font" style="color: #000000"><?= $val['demand']['total']?></p>
                        <p>
                            <a class="layui-btn layui-btn-sm layui-btn-normal iframe-layer" style="border-radius: 50px" data-title="进行需求"
                               data-url="<?= \yii\helpers\Url::to(['/product/demand/list','product_id'=>$val['id'],'search_type'=>2])?>">查看全部<i class="layui-icon layui-icon-right"></i></a>
                        </p>
                        <div class="layui-row layui-col-space5" style="margin-top: 15px">
                            <div class="layui-col-xs3">
                                <p>待指派</p>
                                <p><?= $val['demand']['step0']?></p>
                            </div>
                            <div class="layui-col-xs3">
                                <p>待确认</p>
                                <p><?= $val['demand']['step1']?></p>
                            </div>
                            <div class="layui-col-xs3">
                                <p>开发中</p>
                                <p><?= $val['demand']['step2']?></p>
                            </div>
                            <div class="layui-col-xs3">
                                <p>已完成</p>
                                <p><?= $val['demand']['step3']?></p>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm6 layui-col-space10">
                        <div class="layui-col-xs6 layui-col-sm12">
                            <a class="layui-btn layui-btn-normal iframe-layer" data-title="[<?= $val['title']?>]添加产品成员"
                               data-url="<?= \yii\helpers\Url::to(['/common/select/user-list','product_id'=>$val['id'],'select_type'=>'checkbox']) ?>">
                                <i class="layui-icon layui-icon-addition"></i> 产品成员</a>
                        </div>

                        <div class="layui-col-xs6 layui-col-sm12">
                            <a href="javascript:;" class="layui-btn layui-btn-normal iframe-layer"
                               data-url="<?= \yii\helpers\Url::to(['/product/demand/create', 'product_id' => $val['id']]) ?>"
                               data-title="提需求">
                                <i class="layui-icon layui-icon-addition"></i> 创建需求
                            </a>
                        </div>

                        <div class="layui-col-xs6 layui-col-sm12">
                            <a href="javascript:;" class="layui-btn layui-btn-normal iframe-layer"
                               data-url="<?= \yii\helpers\Url::to(['/test/bug/create', 'product_id' => $val['id']]) ?>"
                               data-title="提Bug">
                                <i class="layui-icon layui-icon-addition"></i> 创建Bug
                            </a>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>

        </div>
    </div>
</div>

