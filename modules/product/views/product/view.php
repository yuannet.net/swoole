<?php

use yii\helpers\Url;

?>

<style>
    .upload-file-remove-btn {
        display: none
    }
</style>

<div class="layui-fluid layui-anim layui-anim-fadein">
    <div class="layui-card">
        <div class="layui-card-header">
            <b>产品详情</b>
        </div>
        <div class="layui-card-body">
            <form class="layui-form" action="" method="post">
                <div class="layui-form-item">
                    <label class="layui-form-label">产品名称</label>
                    <div class="layui-input-block">
                        <input type="text" name="title" lay-verify="required" placeholder="请输入产品名称"
                               autocomplete="off" class="layui-input" value="<?= $model->title ?>" disabled>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">产品描述</label>
                    <div class="layui-input-block">
                        <textarea id="remark" name="remark" class="layui-textarea"
                                  disabled><?= $model->remark ?></textarea>
                    </div>
                </div>

                <div class="layui-row">
                    <div class="layui-col-sm6">
                        <div class="layui-form-item">
                            <label class="layui-form-label">总计工期</label>
                            <div class="layui-input-block">
                                <input type="number" name="duration_total" lay-verify="required" placeholder="请输入产品总计工期"
                                       class="layui-input" value="<?= $model->duration_total ?>" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm6">
                        <div class="layui-form-item">
                            <label class="layui-form-label">已用工期</label>
                            <div class="layui-input-block">
                                <input type="number" name="duration_used" lay-verify="required" placeholder="请输入产品已用工期"
                                       class="layui-input" value="<?= $model->duration_used ?>" disabled>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="layui-row">
                    <div class="layui-col-sm6">
                        <div class="layui-form-item">
                            <label class="layui-form-label">发起时间</label>
                            <div class="layui-input-block">
                                <input type="text" name="start_date" lay-verify="required" placeholder="请选择产品发起时间"
                                       class="layui-input auto-date" readonly disabled
                                       value="<?= $model->start_date ? date('Y-m-d', $model->start_date) : '' ?>">
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm6">
                        <div class="layui-form-item">
                            <label class="layui-form-label">验收时间</label>
                            <div class="layui-input-block">
                                <input type="text" name="acceptance_date" placeholder="请选择产品验收时间"
                                       class="layui-input auto-date" readonly disabled
                                       value="<?= $model->acceptance_date ? date('Y-m-d', $model->acceptance_date) : '' ?>">
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm6">
                        <div class="layui-form-item">
                            <label class="layui-form-label">合同到期时间</label>
                            <div class="layui-input-block">
                                <input type="text" name="expire_date" placeholder="请选择合同到期时间"
                                       class="layui-input auto-date" readonly disabled
                                       value="<?= $model->expire_date ? date('Y-m-d', $model->expire_date) : '' ?>">
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm6">
                        <div class="layui-form-item">
                            <label class="layui-form-label">状态</label>
                            <div class="layui-input-block">
                                <select name="status" id="" disabled>
                                    <?php foreach (\app\services\Product::getStatus() as $key => $val): ?>
                                        <option value="<?= $key ?>" <?= $model->status === $key ? 'selected' : '' ?>><?= $val ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">附件</label>
                        <div class="layui-input-block">
                            <a href="javascript:void(0)"
                               class="layui-btn layui-btn-sm layui-btn-primary upload-file-btn hidden"
                               data-name="attachment" data-id="<?= $model->id ?>" data-module="product">
                                <i class="layui-icon layui-icon-addition"></i>添加文件
                            </a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="layui-card">
        <div class="layui-card-header">
            <b>产品成员</b>
        </div>
        <div class="layui-card-body">
            <table id="dataTable" lay-filter="dataTable"></table>
            <!--搜索栏-->
            <script type="text/html" id="search-toolbar">
                <form class="search-form layui-form" for-table-filter="dataTable">
                    <div class="layui-show-xs-block hidden">
                        <a href="javascript:void(0)"
                           class="layui-btn layui-btn-sm layui-btn-primary toolbar-search-show">搜索</a>
                    </div>

                    <div class="layui-hide-xs toolbar-search-area">
                        <div class="layui-inline">
                            <input type="text" name="order_no" placeholder="人员编号"
                                   class="layui-input">
                        </div>

                        <div class="layui-inline">
                            <input type="text" name="username" placeholder="用户名"
                                   class="layui-input">
                        </div>

                        <div class="layui-inline">
                            <input type="text" name="realname" placeholder="姓名"
                                   class="layui-input">
                        </div>

                        <div class="layui-inline">
                            <a href="javascript:;" class="layui-btn layui-btn-sm layui-btn-normal toolbar-search-btn">
                                <i class="layui-icon layui-icon-search"></i>搜索
                            </a>

                            <a href="javascript:;" class="layui-btn layui-btn-sm layui-btn-normal toolbar-search-reset">
                                <i class="layui-icon layui-icon-refresh-1"></i>重置搜索条件
                            </a>

                            <a class="layui-btn layui-btn-sm layui-btn-normal iframe-layer"
                               data-title="[<?= $model['title'] ?>]添加产品成员" data-reload="dataTable"
                               data-url="<?= \yii\helpers\Url::to(['/common/select/user-list', 'product_id' => $model['id'], 'select_type' => 'checkbox']) ?>">
                                <i class="layui-icon layui-icon-addition"></i> 添加成员</a>
                        </div>
                    </div>
                </form>
            </script>

            <!--操作栏-->
            <script type="text/html" id="dataTableHandle">
                <a class="layui-btn layui-btn-xs layui-btn-normal iframe-layer"
                   data-url="<?= Url::to(['/common/select/user-view']) ?>&id={{= d.user_id }}"
                   data-reload="dataTable" data-title="详情">详情</a>

                <a class="layui-btn layui-btn-xs layui-btn-danger confirm-layer"
                   data-url="<?= Url::to(['/product/product-user/delete']) ?>&id={{= d.id }}"
                   data-reload="dataTable">移除</a>
            </script>
        </div>
    </div>
</div>

<?php $this->beginBlock('js') ?>
<script>
    layui.table.render({
        where: {},
        elem: '#dataTable',
        url: "<?= Url::to(['product-user/list', 'product_id' => Yii::$app->request->get('id')]) ?>", //数据接口
        method: "post",
        page: true,
        height: "350",
        limit: 20,
        toolbar: '#search-toolbar',
        defaultToolbar: [],
        smartReloadModel: true,
        cols: [[
            {
                fixed: 'handle',
                width: 100,
                title: "操作",
                align: 'center',
                toolbar: '#dataTableHandle'
            },
            {field: 'order_no', width: 120, title: "人员编号"},
            {field: 'username', width: 120, title: "用户名"},
            {field: 'realname', width: 120, title: "姓名"},
            {field: 'mobile', width: 120, title: "手机号"},
            {field: 'qq', width: 100, title: "QQ号"},
            {field: 'wechat', width: 160, title: "微信号"},
            {field: 'address', width: 260, title: "住址"},
            {field: 'status', width: 50, title: "状态"},
            {field: 'created_at', width: 180, title: "创建时间"},
        ]]
    });

    function chooseUserCall(checked, get_params) {
        var data = {"product_id": get_params.product_id, "checked": checked};
        $.ajax({
            type: "POST",
            data: data,
            url: "<?= Url::to(['product-user/create'])?>",
            success: function (res) {
                if (res.error == 0) {
                    layui.layer.close(layui.layer.index)
                }

                layui.layer.msg(res.msg);
            }
        });
    }
</script>
<?php $this->endBlock() ?>
