<?php

?>

<div class="layui-fluid layui-anim layui-anim-fadein">
    <div class="layui-card">
        <div class="layui-card-header">
            <b>创建产品</b>
        </div>
        <div class="layui-card-body">
            <form class="layui-form" action="" method="post">
                <div class="layui-form-item">
                    <label class="layui-form-label must">产品名称</label>
                    <div class="layui-input-block">
                        <input type="text" name="title" lay-verify="required" placeholder="请输入产品名称"
                               autocomplete="off" class="layui-input">
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">产品描述</label>
                    <div class="layui-input-block">
                        <textarea id="remark" name="remark" class="layui-textarea"></textarea>
                    </div>
                </div>

                <div class="layui-row">
                    <div class="layui-col-sm6">
                        <div class="layui-form-item">
                            <label class="layui-form-label must">总计工期</label>
                            <div class="layui-input-block">
                                <input type="number" name="duration_total" lay-verify="required" placeholder="请输入产品总计工期"
                                       class="layui-input">
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm6">
                        <div class="layui-form-item">
                            <label class="layui-form-label must">已用工期</label>
                            <div class="layui-input-block">
                                <input type="number" name="duration_used" lay-verify="required" placeholder="请输入产品已用工期"
                                       class="layui-input">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="layui-row">
                    <div class="layui-col-sm6">
                        <div class="layui-form-item">
                            <label class="layui-form-label">发起时间</label>
                            <div class="layui-input-block">
                                <input type="text" name="start_date" placeholder="请选择产品发起时间"
                                       class="layui-input auto-date" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm6">
                        <div class="layui-form-item">
                            <label class="layui-form-label">验收时间</label>
                            <div class="layui-input-block">
                                <input type="text" name="acceptance_date" placeholder="请选择产品验收时间"
                                       class="layui-input auto-date" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm6">
                        <div class="layui-form-item">
                            <label class="layui-form-label">合同到期时间</label>
                            <div class="layui-input-block">
                                <input type="text" name="expire_date" placeholder="请选择合同到期时间"
                                       class="layui-input auto-date" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm6">
                        <div class="layui-form-item">
                            <label class="layui-form-label">状态</label>
                            <div class="layui-input-block">
                                <select name="status" id="">
                                    <?php foreach (\app\services\Product::getStatus() as $key => $val): ?>
                                        <option value="<?= $key ?>"><?= $val ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">附件</label>
                    <div class="layui-input-block">
                        <a href="javascript:void(0)" class="layui-btn layui-btn-sm layui-btn-primary upload-file-btn"
                           data-name="attachment">
                            <i class="layui-icon layui-icon-addition"></i>添加文件
                        </a>
                    </div>
                </div>

                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <button class="layui-btn layui-btn-normal layui-btn-sm ajax-submit" lay-submit
                                lay-filter="submit" after-success="close">立即提交
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php $this->beginBlock('js') ?>

<?php $this->endBlock() ?>
