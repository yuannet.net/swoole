<?php

use yii\helpers\Url;

?>

<div class="layui-fluid layui-anim layui-anim-fadein layui-col-md-offset1 layui-col-md10">
    <div class="layui-row layui-col-space10">
        <!--左边栏-->
        <div class="layui-col-sm8">
            <!--产品统计-->
            <?= $this->render('plug/prod_statistics', ['product_info' => $product_info]) ?>

            <!--产品列表-->
            <?= $this->render('plug/prod_list') ?>
        </div>

        <!--右边栏-->
        <div class="layui-col-sm4">
            <!--我的需求-->
            <?= \app\components\MyDemandWidget::widget() ?>

            <!--我的Bug-->
            <?= \app\components\MyBugWidget::widget() ?>
        </div>
    </div>
</div>

<?php $this->beginBlock('js') ?>
<script>
    layui.table.render({
        where: {},
        elem: '#dataTable',
        url: "<?= Url::to(['list']) ?>", //数据接口
        method: "post",
        page: true,
        height: 350,
        limit: 20,
        toolbar: '#search-toolbar',
        defaultToolbar: [],
        smartReloadModel: true,
        cols: [[
            {
                fixed: 'handle',
                width: 130,
                title: "操作",
                align: 'center',
                toolbar: '#dataTableHandle'
            },
            {field: 'order_no', width: 120, title: "产品编号"},
            {field: 'title', width: 120, title: "产品名称"},
            {field: 'demand_total', width: 90, title: "进行需求数"},
            {field: 'bug_total', width: 90, title: "进行Bug数"},
            {field: 'duration_total', width: 80, title: "总计工期"},
            {field: 'duration_used', width: 80, title: "已用工期"},
            {field: 'duration_left', width: 80, title: "剩余工期"},
            {field: 'start_date', width: 100, title: "开始时间"},
            {field: 'acceptance_date', width: 100, title: "验收时间"},
            {field: 'expire_date', width: 110, title: "合同到期时间"},
            {field: 'status', width: 50, title: "状态"},
        ]]
    });

    $('.produce-list li').mouseover(function () {
        $(this).siblings().removeClass('li_this');
        $(this).addClass('li_this');
        $('#product_detail').find('.product-detail-view').hide();
        $('#product_detail').find('#' + $(this).attr('for')).show();
    })

    function chooseUserCall(checked, get_params) {
        var data = {"product_id": get_params.product_id, "checked": checked};
        $.ajax({
            type: "POST",
            data: data,
            url: "<?= Url::to(['product-user/create'])?>",
            success: function (res) {
                if (res.error == 0) {
                    layui.layer.close(layui.layer.index)
                }

                layui.layer.msg(res.msg);
            }
        });
    }
</script>
<?php $this->endBlock() ?>
