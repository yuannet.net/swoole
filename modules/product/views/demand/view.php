<?php

?>
<style>
    .remark-area {
        position: relative;
        margin: 5px auto auto 15px;
        padding: 5px 15px;
        border: 1px solid #e2e2e2;
        border-radius: 1px;
        background: #F0F0F0;
    }

    .remark-area-toobar {
        position: absolute;
        top: 5px;
        right: 15px;
        display: none;
    }

    .upload-file-remove-btn {
        display: none
    }
</style>

<div class="layui-fluid layui-anim layui-anim-fadein">
    <div class="layui-card">
        <div class="layui-card-header">
            <b>需求详情</b>
        </div>
        <div class="layui-card-body">
            <form class="layui-form" action="" method="post">
                <div class="layui-form-item">
                    <label class="layui-form-label">当前进度</label>
                    <div class="layui-input-block">
                        <a href="javascript:void(0)" class="layui-btn layui-btn-xs <?= $model->stepColor ?>"
                           style="margin-top: 5px">
                            <?= $model::getStep()[$model->step] ?>
                        </a>
                    </div>
                </div>
                <div class="layui-row">
                    <div class="layui-col-sm6">
                        <div class="layui-form-item">
                            <label class="layui-form-label must">所属产品</label>
                            <div class="layui-input-block">
                                <select name="product_id" layui-verify="required" autocomplete="off" disabled>
                                    <option value="">请选择所属产品</option>
                                    <?php foreach (\app\services\Product::productStatistics(false) as $key => $val): ?>
                                        <option value="<?= $val['id'] ?>" <?= $model->product_id == $val['id'] ? 'selected' : '' ?>><?= $val['title'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm6">
                        <div class="layui-form-item">
                            <label class="layui-form-label must">需求名称</label>
                            <div class="layui-input-block">
                                <input type="text" name="title" lay-verify="required" placeholder="请输入需求名称" disabled
                                       autocomplete="off" class="layui-input" value="<?= $model->title ?>">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="layui-row">
                    <div class="layui-col-sm6">
                        <div class="layui-form-item">
                            <label class="layui-form-label">优先级</label>
                            <div class="layui-input-block">
                                <select name="level" id="" disabled>
                                    <option value="1" <?= $model->level == 1 ? 'selected' : '' ?>>1</option>
                                    <option value="2" <?= $model->level == 2 ? 'selected' : '' ?>>2</option>
                                    <option value="3" <?= $model->level == 3 ? 'selected' : '' ?>>3</option>
                                    <option value="4" <?= $model->level == 4 ? 'selected' : '' ?>>4</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="layui-col-sm6">
                        <div class="layui-form-item">
                            <label class="layui-form-label must">所需工期</label>
                            <div class="layui-input-block">
                                <input type="number" name="period" lay-verify="required" placeholder="请输入所需工期"
                                       autocomplete="off" class="layui-input" value="<?= $model->period ?>" disabled>
                                <div class="layui-form-mid layui-word-aux">
                                    产品当前剩余工期 <?= $model->product->duration_total - $model->product->duration_used ?>
                                    （产品剩余工期会在需求完成时扣除）
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="layui-row">
                    <div class="layui-col-sm6">
                        <div class="layui-form-item">
                            <label class="layui-form-label">计划开始时间</label>
                            <div class="layui-input-block">
                                <input type="text" name="plan_start_date" placeholder="请选择计划开始时间"
                                       class="layui-input auto-date" date-type="datetime" readonly disabled
                                       value="<?= $model->plan_start_date ? date('Y-m-d H:i:s', $model->plan_start_date) : '' ?>">
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm6">
                        <div class="layui-form-item">
                            <label class="layui-form-label">计划完成时间</label>
                            <div class="layui-input-block">
                                <input type="text" name="plan_end_date" placeholder="请选择计划完成时间"
                                       class="layui-input auto-date" date-type="datetime" readonly disabled
                                       value="<?= $model->plan_end_date ? date('Y-m-d H:i:s', $model->plan_end_date) : '' ?>">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">需求描述</label>
                    <div class="layui-input-block">
                        <textarea name="remark" class="layui-textarea" disabled
                                  placeholder="请输入需求描述"><?= $model->remark ?></textarea>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">验收标准</label>
                    <div class="layui-input-block">
                        <textarea name="acceptance_remark" class="layui-textarea" disabled
                                  placeholder="请输入验收标准"><?= $model->acceptance_remark ?></textarea>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">指派给</label>
                    <div class="layui-input-block">
                        <div class="layui-input-inline" style="margin-right: 0px;margin-left: 0px">
                            <input type="text" class="layui-input" id="develop_user" disabled
                                   value="<?= $model->developUser ? $model->developUser->name : '' ?>">
                        </div>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">附件</label>
                    <div class="layui-input-block">
                        <a href="javascript:void(0)"
                           class="layui-btn layui-btn-sm layui-btn-primary upload-file-btn hidden"
                           data-name="attachment" data-id="<?= $model->id ?>" data-module="demand">
                            <i class="layui-icon layui-icon-addition"></i>添加文件
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <?php if ($model->develop_user_id == Yii::$app->user->identity->id): ?>
        <div class="layui-card">
            <div class="layui-card-header">
                <b>操作</b>
            </div>
            <div class="layui-card-body" style="text-align: center">
                <?php if ($model->step == 1): ?>
                    <a href="javascript:void(0)" class="layui-btn layui-btn-sm layui-btn-primary confirm-layer"
                       data-content="确认要开始此需求的开发吗？" data-icon="1" success-callback="reload"
                       data-url="<?= \yii\helpers\Url::to(['/product/demand/switch-step', 'demand_id' => $model->id]) ?>">确认需求/开始开发</a>
                <?php endif; ?>
                <?php if ($model->step == 2): ?>
                    <a href="javascript:void(0)" class="layui-btn layui-btn-sm layui-btn-normal confirm-layer"
                       data-content="确认要完成此需求的开发吗？" data-icon="1" success-callback="reload"
                       data-url="<?= \yii\helpers\Url::to(['/product/demand/switch-step', 'demand_id' => $model->id]) ?>">完成开发</a>
                <?php endif; ?>
                <?php if ($model->step == 3): ?>
                    <a href="javascript:void(0)" class="layui-btn layui-btn-sm layui-btn-disabled"
                       data-url="<?= \yii\helpers\Url::to(['/product/demand/switch-step', 'demand_id' => $model->id]) ?>">流程结束</a>
                <?php endif; ?>
            </div>
        </div>
    <?php endif; ?>

    <div class="layui-card">
        <div class="layui-card-header">
            <div class="layui-inline">
                <b>需求的心路历程</b><span style="color: #c2c2c2" class="layui-hide-xs">（备注只能由需求创建者和开发者添加，只能由备注发起者修改/删除）</span>
            </div>
            <!--只能需求创建者或者指派者添加修改备注-->
            <? if ($model->develop_user_id == Yii::$app->user->identity->id || $model->created_by == Yii::$app->user->identity->id): ?>
                <div class="layui-inline" style="float: right">
                    <a href="javascript:void(0)" class="layui-btn layui-btn-sm layui-btn-primary iframe-layer"
                       data-title="添加备注"
                       data-url="<?= \yii\helpers\Url::to(['/product/demand-record/create', 'demand_id' => Yii::$app->request->get('id')]) ?>"
                       data-jump=true>添加备注</a>
                </div>
            <?php endif; ?>
        </div>
        <div class="layui-card-body" style="padding-left: 15px">
            <?php foreach ($model->record as $key => $record): ?>
                <div class="layui-inline">
                    <?= $key + 1 ?>. <?= date('Y-m-d H:i:s', $record->created_at) ?>，由
                    <b><?= $record->createUser->name ?></b> 添加备注
                </div>

                <div class="remark-area">
                    <!--只能备注创建者修改删除备注-->
                    <? if ($record->created_by == Yii::$app->user->identity->id): ?>
                        <div class="remark-area-toobar">
                            <a href="javascript:void(0)" class="iframe-layer" data-title="修改备注" data-jump=true
                               data-url="<?= \yii\helpers\Url::to(['/product/demand-record/update', 'id' => $record->id]) ?>">
                                <i class="layui-icon layui-icon-edit"></i>
                            </a>
                            <a href="javascript:void(0)" class="confirm-layer" success-callback="reload"
                               data-url="<?= \yii\helpers\Url::to(['/product/demand-record/delete', 'id' => $record->id]) ?>">
                                <i class="layui-icon layui-icon-delete"></i>
                            </a>
                        </div>
                    <?php endif; ?>
                    <div>
                        <?= $record->remark ?>
                    </div>
                    <div>
                        <a href="javascript:void(0)"
                           class="layui-btn layui-btn-sm layui-btn-primary upload-file-btn hidden"
                           data-name="attachment" data-id="<?= $record->id ?>" data-module="demand-record">
                            <i class="layui-icon layui-icon-addition"></i>添加文件
                        </a>
                    </div>
                </div>
            <? endforeach; ?>
        </div>
    </div>
</div>


<?php $this->beginBlock('js') ?>
<script>
    function reload() {
        location.reload();
    }

    $('.remark-area').mouseover(function () {
        $(this).find('.remark-area-toobar').show();
    }).mouseleave(function () {
        $(this).find('.remark-area-toobar').hide();
    })
</script>
<?php $this->endBlock() ?>
