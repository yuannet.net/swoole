<?php

namespace app\modules\product\controllers;

use app\controllers\BaseController;
use app\services\Bug;
use app\services\Demand;
use app\services\File;
use app\services\Product;
use app\utils\Utils;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;


class ProductController extends BaseController
{
    public function actionIndex()
    {
        $product_info = Product::productStatistics();
        return $this->render('index', ['product_info' => $product_info]);
    }

    public function actionList()
    {
        $request = Yii::$app->request;

        $where[] = ['status' => [10, 20]];

        //只获取拥有产品组成员的产品
        $where[] = ['id' => Product::authProductId([10, 20])];

        if ($search = Utils::formUnserialize($request->post('searchData'))) {
            foreach ($search as $k => $v) {
                if ($v === '') {
                    continue;
                }
                switch ($k) {
                    case 'order_no':
                        $where[] = ['id' => Utils::getNumeric($v, 'PROD')];
                        break;
                    case 'title':
                        $where[] = ['like', $k, $v];
                        break;
                    case 'status':
                        $where[] = [$k => $v];
                        break;
                }
            }
        }

        $result = Product::listPage($where, '*', 'id DESC', $request->post('page', 1), $request->post('limit', 10));
        $return = ArrayHelper::toArray($result);

        foreach ($result['data'] as $key => $val) {
            $return['data'][$key]['order_no'] = Utils::generateOrderNo('PROD', $val->id);
            $return['data'][$key]['start_date'] = $val->start_date ? date('Y-m-d', $val->start_date) : '待完善';
            $return['data'][$key]['acceptance_date'] = $val->acceptance_date ? date('Y-m-d', $val->acceptance_date) : '待完善';
            $return['data'][$key]['expire_date'] = $val->expire_date ? date('Y-m-d', $val->expire_date) : '待完善';
            $return['data'][$key]['duration_left'] = $val->duration_total - $val->duration_used;
            $return['data'][$key]['status'] = $val::getStatus()[$val->status];
            $return['data'][$key]['status'] = $val::getStatus()[$val->status];
            $return['data'][$key]['demand_total'] = Demand::find()->where(['product_id' => $val->id, 'status' => 10, 'step' => [0, 1, 2]])->count();
            $return['data'][$key]['bug_total'] = Bug::find()->where(['product_id' => $val->id, 'status' => 10, 'step' => [0, 1, 2]])->count();;
        }
        return $this->asJson($return);
    }

    public function actionCreate()
    {
        $request = Yii::$app->request;
        $post = $request->post();

        if ($request->isPost) {
            $model = new Product();
            $model->load($post, '');
            $model->start_date = $post['start_date'] ? strtotime($post['start_date']) : 0;
            $model->acceptance_date = $post['acceptance_date'] ? strtotime($post['acceptance_date']) : 0;
            $model->expire_date = $post['expire_date'] ? strtotime($post['expire_date']) : 0;
            if ($model->save()) {
                if (isset($post['attachment'])) File::batchSave($post['attachment'], 'product', $model->id);
                $model->operation_log->create($this, "创建了产品 {$model->title}", Url::to(['view', 'id' => $model->id]), true);
                return $this->success([], '创建成功');
            }
            return $this->error($model->errors);
        }

        return $this->render('create');
    }

    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $post = $request->post();

        if ($request->isPost) {
            $model = Product::findOne(['id' => $post['id'], 'status' => [10, 20]]);
            $model->load($post, '');
            $model->start_date = $post['start_date'] ? strtotime($post['start_date']) : 0;
            $model->acceptance_date = $post['acceptance_date'] ? strtotime($post['acceptance_date']) : 0;
            $model->expire_date = $post['expire_date'] ? strtotime($post['expire_date']) : 0;
            if ($model->save()) {
                if (isset($post['attachment'])) File::batchSave($post['attachment'], 'product', $model->id);
                if (isset($post['attachment_remove'])) File::batchRemove($post['attachment_remove']);
                $model->operation_log->create($this, "编辑了产品 {$model->title}", Url::to(['view', 'id' => $model->id]), true);
                return $this->success([], '编辑成功');
            }
            return $this->error($model->errors);
        }

        if (!$model = Product::findOne(['id' => $id, 'status' => [10, 20]])) {
            return $this->alert('未找到该产品');
        }

        return $this->render('update', ['model' => $model]);
    }

    public function actionView($id)
    {
        if (!$model = Product::findOne(['id' => $id, 'status' => [10, 20]])) {
            return $this->alert('未找到该产品');
        }

        return $this->render('view', ['model' => $model]);
    }

    public function actionDelete($id)
    {
        if (!$model = Product::findOne(['id' => $id, 'status' => [10, 20]])) {
            return $this->error('未找到该产品');
        }
        $model->status = 0;
        $model->save();
        $model->operation_log->create($this, "删除了产品 {$model->title}",'',true);
        return $this->success([], '删除成功');
    }
}
