<?php

namespace app\modules\product\controllers;

use app\controllers\BaseController;
use app\services\Product;
use app\services\ProductUser;
use app\services\User;
use app\utils\Utils;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;


class ProductUserController extends BaseController
{
    public function actionList()
    {
        $request = Yii::$app->request;

        $where[] = ['product_user.product_id' => $request->get('product_id')];
        $where[] = ['product_user.status' => 10];

        if ($search = Utils::formUnserialize($request->post('searchData'))) {
            foreach ($search as $k => $v) {
                if ($v === '') {
                    continue;
                }
                switch ($k) {
                    case 'order_no':
                        $where[] = ['user.id' => Utils::getNumeric($v, 'USER')];
                        break;
                    case 'realname':
                        $where[] = ['like', 'user.realname', $v];
                        break;
                    case 'username':
                        $where[] = ['user.username' => $v];
                        break;
                }
            }
        }

        $result = ProductUser::listPage($where, 'product_user.*', 'product_user.id DESC', $request->post('page', 1), $request->post('limit', 10), ['user']);
        $return = ArrayHelper::toArray($result);

        foreach ($result['data'] as $key => $val) {
            $user = ArrayHelper::toArray($val->user);
            $return['data'][$key] = $user;
            $return['data'][$key]['id'] = $val->id;
            $return['data'][$key]['user_id'] = $user['id'];
            $return['data'][$key]['order_no'] = Utils::generateOrderNo('USER', $user['id']);
            $return['data'][$key]['status'] = User::getStatus()[$user['status']];
            $return['data'][$key]['created_at'] = date('Y-m-d H:i:s', $user['created_at']);

        }
        return $this->asJson($return);
    }


    public function actionCreate()
    {
        $request = Yii::$app->request;
        $post = $request->post();

        $product_id = $post['product_id'];
        $checked = $post['checked'];

        if (!$product = Product::findOne(['id' => $product_id, 'status' => 10])) {
            return $this->error('未找到该产品');
        }

        foreach ($checked as $key => $val) {
            $p_user = ProductUser::findOne(['product_id' => $product_id, 'user_id' => $val['id']]) ?: new ProductUser();
            $p_user->product_id = $product_id;
            $p_user->user_id = $val['id'];
            $p_user->status = 10;
            $p_user->save();
            $p_user->operation_log->create($this, "添加了产品 {$product->title} 项目成员 {$p_user->user->name}", Url::to(['/product/product/view', 'id' => $p_user->product_id]),true);
        }

        return $this->success([], '新增产品成员成功');
    }

    public function actionDelete($id)
    {
        if (!$model = ProductUser::findOne(['id' => $id, 'status' => 10])) {
            return $this->error('未找到该成员');
        }
        $model->status = 0;
        $model->save();
        $model->operation_log->create($this, "移除了产品 {$model->product->title} 项目成员 {$model->user->name}", Url::to(['/product/product/view', 'id' => $model->product_id]),true);
        return $this->success([], '移除成功');
    }
}
