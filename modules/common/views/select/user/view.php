<?php

?>

<div class="layui-fluid layui-anim layui-anim-fadein">
    <div class="layui-card">
        <div class="layui-card-header">
            <b>人员详情</b>
        </div>
        <div class="layui-card-body">
            <form class="layui-form" action="" method="post">
                <div class="layui-form-item">
                    <label class="layui-form-label">用户名</label>
                    <div class="layui-input-block">
                        <input type="text" name="username" lay-verify="required" placeholder="请输入用户名"
                               autocomplete="off" class="layui-input" value="<?= $model->username ?>" disabled>
                    </div>
                </div>

                <div class="layui-row">
                    <div class="layui-col-sm6">
                        <div class="layui-form-item">
                            <label class="layui-form-label">姓名</label>
                            <div class="layui-input-block">
                                <input type="text" name="realname" lay-verify="required" placeholder="请输入姓名"
                                       autocomplete="off" class="layui-input" value="<?= $model->realname ?>" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm6">
                        <div class="layui-form-item">
                            <label class="layui-form-label">手机号</label>
                            <div class="layui-input-block">
                                <input type="text" name="mobile" placeholder="请输入手机号"
                                       autocomplete="off" class="layui-input" value="<?= $model->mobile ?>" disabled>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="layui-row">
                    <div class="layui-col-sm6">
                        <div class="layui-form-item">
                            <label class="layui-form-label">QQ号</label>
                            <div class="layui-input-block">
                                <input type="text" name="qq" placeholder="请输入QQ号"
                                       autocomplete="off" class="layui-input" value="<?= $model->qq ?>" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm6">
                        <div class="layui-form-item">
                            <label class="layui-form-label">微信号</label>
                            <div class="layui-input-block">
                                <input type="text" name="wechat" placeholder="请输入微信号"
                                       autocomplete="off" class="layui-input" value="<?= $model->wechat ?>" disabled>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">住址</label>
                    <div class="layui-input-block">
                        <textarea name="address" class="layui-textarea"
                                  placeholder="请输入住址" disabled><?= $model->address ?></textarea>
                    </div>
                </div>

                <div class="layui-row">
                    <div class="layui-col-sm6">
                        <div class="layui-form-item">
                            <label class="layui-form-label">超管（可查看所有数据）</label>
                            <div class="layui-input-block">
                                <select name="is_super" disabled>
                                    <option value="0" <?= $model->is_super === 0 ? 'selected' : '' ?>>否</option>
                                    <option value="1" <?= $model->is_super === 1 ? 'selected' : '' ?>>是</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm6">
                        <div class="layui-form-item">
                            <label class="layui-form-label">状态</label>
                            <div class="layui-input-block">
                                <select name="status" disabled>
                                    <?php foreach (\app\services\User::getStatus() as $key => $val): ?>
                                        <option value="<?= $key ?>" <?= $model->status === $key ? 'selected' : '' ?>><?= $val ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">所属产品</label>
                    <div class="layui-input-block" style="">
                        <?php foreach (\app\services\Product::getAllProduct() as $val): ?>
                            <input type="checkbox" name="product[]" title="<?= $val->title ?>" value="<?= $val->id ?>"
                                   lay-skin="primary" <?= in_array($val->id, $product_array) ? 'checked' : '' ?> disabled>
                        <?php endforeach; ?>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<?php $this->beginBlock('js') ?>

<?php $this->endBlock() ?>
