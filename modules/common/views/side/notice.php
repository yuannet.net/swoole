<?php

?>

<div class="layui-fluid layui-anim layui-anim-fadein layui-bg-white layui-border-box" style="padding: 15px;">
    <div class="layui-tab">
        <ul class="layui-tab-title" style="text-align: center">
            <li class="layui-this">最新动态(近3天)</li>
            <!--            <li>私信</li>-->
        </ul>
        <div class="layui-tab-content">
            <div class="layui-tab-item layui-show">
                <div class="layui-col-xs12" style="text-align: right">
                    <a href="javascript:void(0)" class="confirm-layer" data-content="是否确认全部通知" data-icon="3"
                       data-url="<?= \yii\helpers\Url::to(['/common/side/set-notice-last-id']) ?>"
                       success-callback="clean_notice">确认全部通知</a>
                </div>
                <?php if ($model): ?>
                    <table class="layui-table" lay-even lay-skin="nob" lay-size="sm">
                        <tbody>
                        <?php foreach ($model as $key => $val): ?>
                            <tr>
                                <td title="未确认通知，确认全部通知后隐藏">
                                    <?php if ($val->id > \app\utils\Utils::getCookie('notice_last_id')): ?>
                                        <span class="layui-badge-dot layui-bg-red"></span>
                                    <?php endif; ?>
                                </td>
                                <td>
                                    <p style="font-size: 14px">
                                        <?php if ($val->url): ?>
                                            <a href="javascript:void(0)" class="iframe-layer" style="color: #01AAED"
                                               data-title="最新动态"
                                               data-url="<?= $val->url ?>">
                                                <?= $val->createUser->name ?> <?= $val->remark ?>
                                            </a>
                                        <?php else: ?>
                                            <?= $val->createUser->name ?> <?= $val->remark ?>
                                        <?php endif; ?>
                                    </p>
                                    <p style="margin-top: 5px"><?= date('m月d日 H:i', $val->created_at) ?></p>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php else: ?>
                    <div style="text-align: center">暂无动态...</div>
                <?php endif; ?>
            </div>
            <!--            <div class="layui-tab-item">-->
            <!--                开发中...-->
            <!--            </div>-->
        </div>
    </div>
</div>


<?php $this->beginBlock('js') ?>
<script>
    $(function () {
        $('.layui-fluid').css('minHeight', $(window).height());
    })

    function clean_notice() {
        $('.layui-badge-dot').remove();
    }
</script>
<?php $this->endBlock() ?>
