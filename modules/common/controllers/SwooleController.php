<?php

namespace app\modules\common\controllers;

use yii\web\Controller;

class SwooleController extends Controller
{
    public function actionTest()
    {
        $client = new \swoole_client(SWOOLE_SOCK_TCP);
        $client->connect('127.0.0.1', 39500);
        $client->send(random_int(1, 5));
        $client->close();
        echo '请求：' . date('H:i:s');
    }

    public function actionSocket()
    {
        return $this->render('socket');
    }
}