<?php

namespace app\modules\common\controllers;

use app\controllers\BaseController;
use app\services\Bug;
use app\services\Demand;
use app\services\OperationLog;
use app\services\Product;
use app\utils\Utils;
use Yii;


class BadgeController extends BaseController
{
    /**
     * 待确认需求数
     * @method actionBadgeCount
     * @author ZhangYuan 574368565@qq.com
     * @time 2021/2/19 15:48
     */
    public function actionDemandCount()
    {
        $where[] = 'and';
        $where[] = ['demand.status' => 10];
        $where[] = ['demand.product_id' => Product::authProductId()]; //获取拥有需求组成员的需求,超管获取全部
        $where[] = ['demand.step' => 1];
        $where[] = ['demand.develop_user_id' => Yii::$app->user->identity->id];
        $count = Demand::find()->joinWith(['product'])->where($where)->count();
        return $this->success(['count' => $count]);
    }

    /**
     * 待确认Bug数
     * @method actionBugCount
     * @return \yii\web\Response
     * @author ZhangYuan 574368565@qq.com
     * @time 2021/2/19 16:07
     */
    public function actionBugCount()
    {
        $where[] = 'and';
        $where[] = ['bug.status' => 10];
        $where[] = ['bug.product_id' => Product::authProductId()]; //获取拥有需求组成员的需求,超管获取全部
        $where[] = ['bug.step' => 1];
        $where[] = ['bug.develop_user_id' => Yii::$app->user->identity->id];
        $count = Bug::find()->joinWith(['product'])->where($where)->count();
        return $this->success(['count' => $count]);
    }

    /**
     * 最新动态通知数量
     * @method actionNoticeCount
     * @return \yii\web\Response
     * @author ZhangYuan 574368565@qq.com
     * @time 2021/3/1 12:38
     */
    public function actionNoticeCount()
    {
        $last_id = Utils::getCookie('notice_last_id') ? Utils::getCookie('notice_last_id') : 0;
        $begin_time = strtotime(date('Y-m-d', strtotime("-2 day")));
        $notice_count = OperationLog::find()->where(['is_show' => 1, 'status' => 10])->andWhere(['>', 'id', $last_id])->andWhere(['>=', 'created_at', $begin_time])->count();
        return $this->success(['count' => $notice_count]);
    }
}
