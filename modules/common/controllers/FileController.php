<?php

namespace app\modules\common\controllers;

use app\controllers\BaseController;
use app\services\File;
use app\utils\Utils;


class FileController extends BaseController
{
    public function actionFileList($module, $data_id)
    {
        $file = File::findAll(['module' => $module, 'data_id' => $data_id, 'status' => 10]);
        return $this->success($file);
    }

    public function actionUpload()
    {
        try {
            $file = Utils::uploadFile();
            return $file ? $this->success(['src' => $file]) : $this->error('上传失败，请稍后重试');
        } catch (Exception $e) {
            return $this->error('上传失败，请稍后重试');
        }
    }
}
