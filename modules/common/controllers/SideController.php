<?php

namespace app\modules\common\controllers;

use app\controllers\BaseController;
use app\services\OperationLog;
use app\utils\Utils;


class SideController extends BaseController
{
    public function actionNotice()
    {
        $begin_time = strtotime(date('Y-m-d', strtotime("-2 day")));
        $model = OperationLog::find()->where(['is_show' => 1, 'status' => 10])->andWhere(['>=', 'created_at', $begin_time])->orderBy('id desc')->all();
        return $this->render('notice', ['model' => $model]);
    }

    public function actionSetNoticeLastId()
    {
        //记录最新的id
        $last = OperationLog::find()->orderBy('id desc')->one();
        $last_id = $last ? $last->id : 0;
        Utils::setCookie('notice_last_id', $last_id, time() + 86400 * 10000);
        return $this->success([], '操作成功 ');
    }
}
