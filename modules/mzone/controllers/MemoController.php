<?php

namespace app\modules\mzone\controllers;

use app\controllers\BaseController;
use app\services\File;
use app\services\Memo;
use app\utils\Utils;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;


class MemoController extends BaseController
{
    public function actionList($data = 0)
    {
        if (!$data) {
            return $this->render('list');
        }

        $request = Yii::$app->request;
        $post = Yii::$app->request->post();
        $get = Yii::$app->request->get();

        $where[] = ['status' => 10];
        $where[] = ['created_by' => Yii::$app->user->identity->id];

        if ($search = Utils::formUnserialize(Yii::$app->request->post('searchData'))) {
            foreach ($search as $k => $v) {
                if ($v === '') {
                    continue;
                }
                switch ($k) {
                    case 'order_no':
                        $where[] = ['id' => Utils::getNumeric($v, 'MEMO')];
                        break;
                    case 'title':
                        $where[] = ['like', 'title', $v];
                        break;
                }
            }
        }

        $result = Memo::listPage($where, '*', 'id DESC', $request->post('page', 1), $request->post('limit', 10));
        $return = ArrayHelper::toArray($result);

        foreach ($result['data'] as $key => $val) {
            $return['data'][$key]['order_no'] = Utils::generateOrderNo('MEMO', $val->id);
            $return['data'][$key]['created_at'] = date('Y-m-d H:i:s', $val->created_at);
            $return['data'][$key]['create_user'] = $val->createUser ? $val->createUser->name : '';
            $return['data'][$key]['plan_date'] = $val->plan_date ? date('Y-m-d H:i:s', $val->plan_date) : '';
            $return['data'][$key]['canEdit'] = $val->canEdit();
            $return['data'][$key]['canDel'] = $val->canDel();
        }
        return $this->asJson($return);
    }

    public function actionCreate()
    {
        $request = Yii::$app->request;
        $post = $request->post();

        if ($request->isPost) {
            $model = new Memo();
            $model->load($post, '');
            $model->plan_date = $post['plan_date'] ? strtotime($post['plan_date']) : 0;

            if ($model->save()) {
                if (isset($post['attachment'])) File::batchSave($post['attachment'], 'memo', $model->id);
                $model->operation_log->create($this, "创建了备忘录 {$model->title}", Url::to(['view', 'id' => $model->id]));
                return $this->success([], '创建成功');
            }
            return $this->error($model->errors);
        }

        return $this->render('create');
    }

    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $post = $request->post();

        if ($request->isPost) {
            $model = Memo::findOne(['id' => $post['id'], 'status' => 10]);
            $model->load($post, '');
            $model->plan_date = $post['plan_date'] ? strtotime($post['plan_date']) : 0;

            if ($model->save()) {
                if (isset($post['attachment'])) File::batchSave($post['attachment'], 'memo', $model->id);
                if (isset($post['attachment_remove'])) File::batchRemove($post['attachment_remove']);
                $model->operation_log->create($this, "编辑了备忘录 {$model->title}", Url::to(['view', 'id' => $model->id]));
                return $this->success([], '编辑成功');
            }
            return $this->error($model->errors);
        }

        if (!$model = Memo::findOne(['id' => $id, 'status' => [10, 20]])) {
            return $this->alert('未找到该备忘录');
        }
        if (!$model->canEdit()) {
            return $this->alert('反正不是我的 我也不该要~~');
        }
        return $this->render('update', ['model' => $model]);
    }

    public function actionView($id)
    {
        if (!$model = Memo::findOne(['id' => $id, 'status' => 10])) {
            return $this->alert('未找到该备忘录');
        }

        if (!$model->canEdit()) {
            return $this->alert('反正不是我的 我也不该要~~');
        }

        return $this->render('view', ['model' => $model]);
    }

    public function actionDelete($id)
    {
        if (!$model = Memo::findOne(['id' => $id, 'status' => [10]])) {
            return $this->error('未找到该备忘录');
        }
        if (!$model->canDel()) {
            return $this->error('反正不是我的 我也不该要~~');
        }
        $model->status = 0;
        $model->save();
        $model->operation_log->create($this, "删除了备忘录 {$model->title}");
        return $this->success([], '删除成功');
    }
}
