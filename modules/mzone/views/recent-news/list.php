<?php

use yii\helpers\Url;

$product_id = Yii::$app->request->get('product_id', 0);
$search_type = Yii::$app->request->get('search_type', 2);
?>

<style>
    .layui-btn-primary {
        font-size: 14px;
        margin-top: -3px;
    }
</style>

<div class="layui-fluid layui-anim layui-anim-fadein layui-col-md-offset1 layui-col-md10">
    <div class="layui-card">
        <div class="layui-card-header">
            <b>最新动态</b>
        </div>
        <div class="layui-card-body full-height" data-free="91">
            <table id="dataTable" lay-filter="dataTable"></table>
        </div>
    </div>

    <!--搜索栏-->
    <script type="text/html" id="search-toolbar">
        <form class="search-form layui-form" for-table-filter="dataTable">
            <div class="layui-show-xs-block hidden">
                <a href="javascript:void(0)" class="layui-btn layui-btn-sm layui-btn-primary toolbar-search-show">搜索</a>
            </div>

            <div class="layui-hide-xs toolbar-search-area">
                <div class="layui-inline">
                    <input type="text" name="order_no" placeholder="动态编号"
                           class="layui-input">
                </div>

                <div class="layui-inline">
                    <input type="text" name="remark" placeholder="描述"
                           class="layui-input">
                </div>

                <div class="layui-inline">
                    <input type="text" id="created_at" name="created_at" placeholder="动态时间"
                           class="layui-input" readonly>
                </div>

                <div class="layui-inline">
                    <a href="javascript:;" class="layui-btn layui-btn-sm layui-btn-normal toolbar-search-btn">
                        <i class="layui-icon layui-icon-search"></i>搜索
                    </a>

                    <a href="javascript:;" class="layui-btn layui-btn-sm layui-btn-normal toolbar-search-reset">
                        <i class="layui-icon layui-icon-refresh-1"></i>重置搜索条件
                    </a>
                </div>
            </div>
        </form>
    </script>

    <!--操作栏-->
    <script type="text/html" id="dataTableHandle">
        {{# if(d.url){ }}
        <a class="layui-btn layui-btn-xs layui-btn-normal iframe-layer"
           data-url="{{= d.url }}"
           data-reload="dataTable" data-title="查看">查看</a>
        {{# } }}
    </script>
</div>

<?php $this->beginBlock('js') ?>
<script>
    layui.table.render({
        where: {},
        elem: '#dataTable',
        url: "<?= Url::to(['list', 'data' => 1, 'search_type' => $search_type, 'product_id' => $product_id]) ?>", //数据接口
        method: "post",
        page: true,
        customHeight: "100%",
        limit: 20,
        toolbar: '#search-toolbar',
        defaultToolbar: [],
        smartReloadModel: true,
        cols: [[
            {
                fixed: 'handle',
                width: 60,
                title: "操作",
                align: 'center',
                toolbar: '#dataTableHandle'
            },
            {field: 'order_no', width: 120, title: "动态编号"},
            {field: 'remark', minWidth: 220, title: "描述"},
            {field: 'created_at', width: 180, title: "动态时间"},
        ]]
    });

    layui.laydate.render({
        elem: '#created_at'
        , type: 'datetime'
        , range: '~' //或 range: '~' 来自定义分割字符
    });
</script>
<?php $this->endBlock() ?>
