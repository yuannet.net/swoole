<?php

use yii\helpers\Url;

?>

<div class="layui-fluid layui-anim layui-anim-fadein">
    <div class="layui-card">
        <div class="layui-card-header">
            <b>提备忘录</b>
        </div>
        <div class="layui-card-body">
            <form class="layui-form" action="" method="post">
                <div class="layui-form-item">
                    <label class="layui-form-label must">备忘名称</label>
                    <div class="layui-input-block">
                        <input type="text" class="layui-input" id="memo_title" name="title" lay-verify="required">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">描述</label>
                    <div class="layui-input-block">
                        <textarea class="layui-textarea" name="remark"></textarea>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">优先级</label>
                    <div class="layui-input-block">
                        <select name="level" id="">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3" selected>3</option>
                            <option value="4">4</option>
                        </select>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">时间</label>
                    <div class="layui-input-block">
                        <input type="text" class="layui-input auto-date" name="plan_date" date-type="datetime"
                               readonly>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">附件</label>
                    <div class="layui-input-block">
                        <a href="javascript:void(0)" class="layui-btn layui-btn-sm layui-btn-primary upload-file-btn"
                           data-name="attachment">
                            <i class="layui-icon layui-icon-addition"></i>添加文件
                        </a>
                    </div>
                </div>

                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <div class="layui-block">
                            <button class="layui-btn layui-btn-normal layui-btn-sm ajax-submit" lay-submit
                                    lay-filter="submit" after-success="close">立即提交
                            </button>
                        </div>
                        <div class="layui-form-mid layui-word-aux">备忘录只能由发起者管理/查看</div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<?php $this->beginBlock('js') ?>
<script>
    function chooseUserCall(checked, get_params) {
        $('#develop_user').val(checked[0].realname ? checked[0].realname : checked[0].username);
        $('#develop_user_id').val(checked[0].id);
        layui.layer.close(layui.layer.index)
    }
</script>
<?php $this->endBlock() ?>
