<?php

?>
<style>
    .upload-file-remove-btn {
        display: none
    }
</style>

<div class="layui-fluid layui-anim layui-anim-fadein">
    <div class="layui-card">
        <div class="layui-card-header">
            <b>查看备忘录</b>
        </div>
        <div class="layui-card-body">
            <form class="layui-form" action="" method="post">
                <div class="layui-form-item">
                    <label class="layui-form-label must">备忘名称</label>
                    <div class="layui-input-block">
                        <input type="text" class="layui-input" id="memo_title" name="title" lay-verify="required"
                               value="<?= $model->title ?>" disabled>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">描述</label>
                    <div class="layui-input-block">
                        <textarea class="layui-textarea" name="remark" disabled><?= $model->remark ?></textarea>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">优先级</label>
                    <div class="layui-input-block">
                        <select name="level" id="" disabled>
                            <option value="1" <?= $model->level == 1 ? 'selected' : '' ?>>1</option>
                            <option value="2" <?= $model->level == 2 ? 'selected' : '' ?>>2</option>
                            <option value="3" <?= $model->level == 3 ? 'selected' : '' ?>>3</option>
                            <option value="4" <?= $model->level == 4 ? 'selected' : '' ?>>4</option>
                        </select>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">时间</label>
                    <div class="layui-input-block">
                        <input type="text" class="layui-input auto-date" name="plan_date" date-type="datetime"
                               value="<?= $model->plan_date ? date('Y-m-d H:i:s', $model->plan_date) : '' ?>"
                               readonly disabled>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">附件</label>
                    <div class="layui-input-block">
                        <a href="javascript:void(0)" class="layui-btn layui-btn-sm layui-btn-primary upload-file-btn layui-hide"
                           data-name="attachment" data-id="<?= $model->id ?>" data-module="memo">
                            <i class="layui-icon layui-icon-addition"></i>添加文件
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php $this->beginBlock('js') ?>
<?php $this->endBlock() ?>
