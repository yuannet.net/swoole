<?php

use yii\helpers\Url;

$product_id = Yii::$app->request->get('product_id', 0);
$search_type = Yii::$app->request->get('search_type', 2);
?>

<style>
    .layui-btn-primary {
        font-size: 14px;
        margin-top: -3px;
    }
</style>

<div class="layui-fluid layui-anim layui-anim-fadein layui-col-md-offset1 layui-col-md10">
    <div class="layui-card">
        <div class="layui-card-header">
            <b>备忘录</b>
        </div>
        <div class="layui-card-body full-height" data-free="91">
            <table id="dataTable" lay-filter="dataTable"></table>
        </div>
    </div>

    <!--搜索栏-->
    <script type="text/html" id="search-toolbar">
        <form class="search-form layui-form" for-table-filter="dataTable">
            <div class="layui-show-xs-block hidden">
                <a href="javascript:void(0)" class="layui-btn layui-btn-sm layui-btn-primary toolbar-search-show">搜索</a>
            </div>

            <div class="layui-hide-xs toolbar-search-area">
                <div class="layui-inline">
                    <input type="text" name="order_no" placeholder="备忘录编号"
                           class="layui-input">
                </div>

                <div class="layui-inline">
                    <input type="text" name="title" placeholder="备忘录名称"
                           class="layui-input">
                </div>

                <div class="layui-inline">
                    <a href="javascript:;" class="layui-btn layui-btn-sm layui-btn-normal toolbar-search-btn">
                        <i class="layui-icon layui-icon-search"></i>搜索
                    </a>

                    <a href="javascript:;" class="layui-btn layui-btn-sm layui-btn-normal toolbar-search-reset">
                        <i class="layui-icon layui-icon-refresh-1"></i>重置搜索条件
                    </a>

                    <a href="javascript:;" class="layui-btn layui-btn-sm layui-btn-normal iframe-layer"
                       data-url="<?= Url::to(['/mzone/memo/create', 'product_id' => $product_id]) ?>"
                       data-title="提备忘录" data-reload="dataTable">
                        <i class="layui-icon layui-icon-addition"></i>新增备忘录
                    </a>
                </div>
            </div>
        </form>
    </script>

    <!--操作栏-->
    <script type="text/html" id="dataTableHandle">
        <a class="layui-btn layui-btn-xs layui-btn-normal iframe-layer"
           data-url="<?= Url::to(['/mzone/memo/view']) ?>&id={{= d.id }}"
           data-reload="dataTable" data-title="详情">详情</a>

        {{# if(d.canEdit){ }}
        <a class="layui-btn layui-btn-xs layui-btn-warm iframe-layer"
           data-url="<?= Url::to(['/mzone/memo/update']) ?>&id={{= d.id }}" data-title="编辑"
           data-reload="dataTable">编辑</a>
        {{# } }}

        {{# if(d.canDel){ }}
        <a class="layui-btn layui-btn-xs layui-btn-danger confirm-layer"
           data-url="<?= Url::to(['/mzone/memo/delete']) ?>&id={{= d.id }}" data-reload="dataTable">删除</a>
        {{# } }}
    </script>
</div>

<?php $this->beginBlock('js') ?>
<script>
    layui.table.render({
        where: {},
        elem: '#dataTable',
        url: "<?= Url::to(['list', 'data' => 1]) ?>", //数据接口
        method: "post",
        page: true,
        customHeight: "100%",
        limit: 20,
        toolbar: '#search-toolbar',
        defaultToolbar: [],
        smartReloadModel: true,
        cols: [[
            {
                fixed: 'handle',
                width: 130,
                title: "操作",
                align: 'center',
                toolbar: '#dataTableHandle'
            },
            {
                field: 'level', width: 65, title: "优先级", align: "center", templet: function (d) {
                    var className = 'layui-btn layui-btn-xs ';
                    switch (d.level) {
                        case 1:
                            className += 'layui-btn-danger';
                            break;
                        case 2:
                            className += 'layui-btn-warm';
                            break;
                        case 3:
                            className += 'layui-btn-normal';
                            break;
                        case 4:
                            className += '';
                            break;
                    }
                    var html = "<a class='" + className + "' style='border-radius: 20px;padding: 0px 8px;font-weight: bold;'>" + d.level + "</a>";
                    return html;
                }
            },
            {field: 'order_no', width: 120, title: "备忘录编号"},
            {field: 'title', width: 180, title: "备忘录名称"},
            {field: 'remark', minWidth: 220, title: "描述"},
            {field: 'create_user', width: 120, title: "创建者"},
            {field: 'plan_date', width: 180, title: "计划时间"},
            {field: 'created_at', width: 180, title: "创建时间"},
        ]]
    });
</script>
<?php $this->endBlock() ?>
