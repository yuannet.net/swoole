<div class="layui-card">
    <div class="layui-card-header">
        <b>产品统计</b>
    </div>
    <div class="layui-card-body">
        <div class="layui-row layui-col-space20">
            <div class="layui-col-sm4" style="padding-right: 0px;max-height:250px;overflow-y: auto">
                <ul class="produce-list">
                    <?php foreach ($product_info as $key => $val): ?>
                        <li class="<?= $key == 0 ? 'li_this' : '' ?>"
                            for="product_<?= $key ?>"><?= $val['title'] ?></li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="layui-col-sm8" id="product_detail" style="border-left: 1px solid #F0F0F0;min-height:250px">
                <?php foreach ($product_info as $key => $val): ?>
                    <div class="layui-row product-detail-view <?= $key != 0 ? 'hidden' : '' ?>"
                         id="product_<?= $key ?>">
                        <div class="layui-col-sm6">
                            <p style="font-size: 18px">进行需求</p>
                            <p class="layuiadmin-big-font" style="color: #000000"><?= $val['demand']['total']?></p>
                            <p>
                                <a class="layui-btn layui-btn-sm layui-btn-normal iframe-layer" style="border-radius: 50px" data-title="进行需求"
                                   data-url="<?= \yii\helpers\Url::to(['/product/demand/list','product_id'=>$val['id'],'search_type'=>2])?>">查看全部<i class="layui-icon layui-icon-right"></i></a>
                            </p>
                            <div class="layui-row layui-col-space5" style="margin-top: 15px">
                                <div class="layui-col-xs3">
                                    <p>待指派</p>
                                    <p><?= $val['demand']['step0']?></p>
                                </div>
                                <div class="layui-col-xs3">
                                    <p>待确认</p>
                                    <p><?= $val['demand']['step1']?></p>
                                </div>
                                <div class="layui-col-xs3">
                                    <p>开发中</p>
                                    <p><?= $val['demand']['step2']?></p>
                                </div>
                                <div class="layui-col-xs3">
                                    <p>已完成</p>
                                    <p><?= $val['demand']['step3']?></p>
                                </div>
                            </div>
                        </div>
                        <div class="layui-col-sm6">
                            <p style="font-size: 18px">进行Bug</p>
                            <p class="layuiadmin-big-font" style="color: #000000"><?= $val['bug']['total']?></p>
                            <p>
                                <a class="layui-btn layui-btn-sm layui-btn-normal  iframe-layer" style="border-radius: 50px" data-title="进行Bug"
                                   data-url="<?= \yii\helpers\Url::to(['/test/bug/list','product_id'=>$val['id'],'search_type'=>2])?>">查看全部<i class="layui-icon layui-icon-right"></i></a>
                            </p>
                            <div class="layui-row layui-col-space5" style="margin-top: 15px">
                                <div class="layui-col-xs3">
                                    <p>待指派</p>
                                    <p><?= $val['bug']['step0']?></p>
                                </div>
                                <div class="layui-col-xs3">
                                    <p>待确认</p>
                                    <p><?= $val['bug']['step1']?></p>
                                </div>
                                <div class="layui-col-xs3">
                                    <p>修复中</p>
                                    <p><?= $val['bug']['step2']?></p>
                                </div>
                                <div class="layui-col-xs3">
                                    <p>已关闭</p>
                                    <p><?= $val['bug']['step3']?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>

        </div>
    </div>
</div>