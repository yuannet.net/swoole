<?php

use yii\helpers\Url;

?>

<div class="layui-card">
    <div class="layui-card-body layui-row">
        <div class="layui-col-sm4">
            <b style="color: #000000"><?= Yii::$app->user->identity->realname ? Yii::$app->user->identity->realname : Yii::$app->user->identity->username ?>
                ，<?= \app\utils\Utils::getHaloWord() ?></b>
            <div class="layui-block">
                <a href="javascript:void(0)" style="color: #01AAED" class="iframe-layer" data-title="加产品"
                   data-url="<?= Url:: to(['/product/product/create']) ?>">
                    1.加产品
                </a>
            </div>
            <div class="layui-block">
                <a href="javascript:void(0)" style="color: #01AAED" class="iframe-layer" data-title="提需求"
                   data-url="<?= Url:: to(['/product/demand/create']) ?>">
                    2.提需求
                </a>
            </div>
            <div class="layui-block">
                <a href="javascript:void(0)" style="color: #01AAED" class="iframe-layer" data-title="提Bug"
                   data-url="<?= Url:: to(['/test/bug/create']) ?>">
                    3.提Bug
                </a>
            </div>
        </div>

        <div class="layui-col-sm8 layui-col-space10">
            <div class="layui-col-sm12">
                <?= date('Y年m月d日') ?>&nbsp;&nbsp;&nbsp;&nbsp;<span
                        style="color: #000000">当前工作总计</span>
            </div>

            <div class="layui-col-sm12" style="border-left: 1px solid #F0F0F0">
                <div class="layui-col-xs6 layui-col-sm4" style="text-align: center">
                    <p>我的产品</p>
                    <p class="layuiadmin-big-font" style="color: #000000">
                        <a href="<?= \yii\helpers\Url::to(['/product/product/index']) ?>"><?= $product_overview['product_total'] ?></a>
                    </p>
                </div>
                <div class="layui-col-xs6 layui-col-sm4" style="text-align: center">
                    <p>我的待办需求</p>
                    <p class="layuiadmin-big-font" style="color: #000000">
                        <a href="<?= \yii\helpers\Url::to(['/product/demand/list', 'search_type' => 4]) ?>"><?= $product_overview['demand_total'] ?></a>
                    </p>
                </div>
                <div class="layui-col-xs6 layui-col-sm4" style="text-align: center">
                    <p>我的待办Bug</p>
                    <p class="layuiadmin-big-font" style="color: #000000">
                        <a href="<?= \yii\helpers\Url::to(['/test/bug/list', 'search_type' => 4]) ?>"><?= $product_overview['bug_total'] ?></p></a>
                </div>
            </div>
        </div>
    </div>
</div>