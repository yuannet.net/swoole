<?php

use yii\helpers\Url;

$this->title = Yii::$app->params['title'];
?>
<style>
    a {
        cursor: pointer
    }
</style>

<div class="layui-layout layui-layout-admin" style="position: relative">
    <div class="layui-row layui-bg-blue" id="LAY-app-nav">
        <div class="layui-col-xs12 layui-col-md3">
            <ul class="layui-nav layui-bg-blue">
                <li class="layui-nav-item" style="margin-right: 3px">
                    <i class="layui-icon layui-icon-shrink-right layui-show-xs-inline layui-show-sm-inline layui-hide-md layui-hide-bg"
                       id="nav-area-btn"></i>
                </li>
                <li class="layui-nav-item layui-hide-xs">
                    <?= Yii::$app->params['title']?>
                </li>

                <li class="layui-nav-item layui-hide layui-show-xs-inline">
                    产品管理系统
                </li>

                <li class="layui-nav-item layui-hide-md layui-hide-bg layui-show-xs-block layui-show-sm-block"
                    lay-unselect="" style="float: right;display: none;">
                    <a href="javascript:;"><?= Yii::$app->user->identity->realname ? Yii::$app->user->identity->realname : Yii::$app->user->identity->username ?></a>
                    <dl class="layui-nav-child">
                        <dd><a layui-href="<?= Url::to(['/site/update-password']) ?>">修改信息</a></dd>
                        <dd><a href="<?= Url::to(['/site/logout']) ?>">退出</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item layui-hide-md layui-hide-bg layui-show-xs-block layui-show-sm-block"
                    style="float: right;display: none;" lay-unselect="">
                    <a href="javascript:void(0)" onclick="openRecentNews()" style="padding: 0">
                        <i class="layui-icon layui-icon-notice"></i>
                    </a>
                </li>
            </ul>
        </div>

        <div class="layui-col-xs12 layui-col-md6" id="nav-area">
            <ul class="layui-nav layui-bg-blue">
                <?php foreach (\app\services\Menu::getMenu() as $key => $item): ?>
                    <li class="layui-nav-item <?= $key == 0 ? 'layui-this' : '' ?>">
                        <a <?php if ($item['url']): ?>layui-href="<?= $item['url'] ?>"<?php endif; ?>>
                            <?= $item['title'] ?>
                            <?php if (isset($item['badge']) && !empty($item['badge'])): ?>
                                <span class="layui-badge" data-url="<?= $item['badge'] ?>"
                                      title="<?= $item['badge_desc'] ?>"><i
                                            class="layui-anim layui-anim-rotate layui-anim-loop layui-icon layui-icon-loading"></i></span>
                            <?php endif; ?>
                        </a>
                        <?php if (isset($item['child'])): ?>
                            <dl class="layui-nav-child"> <!-- 二级菜单 -->
                                <?php foreach ($item['child'] as $child): ?>
                                    <dd>
                                        <a <?php if ($child['url']): ?>layui-href="<?= $child['url'] ?>"<?php endif; ?>><?= $child['title'] ?></a>
                                    </dd>
                                <?php endforeach; ?>
                            </dl>
                        <?php endif; ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>

        <div class="layui-col-md3 layui-hide-xs layui-hide-sm layui-show-md-block layui-show-bg-block">
            <ul class="layui-nav layui-bg-blue">

                <li class="layui-nav-item" lay-unselect="" style="float: right">
                    <a href="javascript:;"><?= Yii::$app->user->identity->realname ? Yii::$app->user->identity->realname : Yii::$app->user->identity->username ?></a>
                    <dl class="layui-nav-child">
                        <dd><a href="javascript:;" layui-href="<?= Url::to(['/site/update-password']) ?>">修改信息</a></dd>
                        <dd><a href="<?= Url::to(['/site/logout']) ?>">退出</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item" lay-unselect="" style="float: right;">
                    <a href="javascript:void(0)" onclick="openRecentNews()">
                        <i class="layui-icon layui-icon-notice"></i>
                        <span class="layui-badge" data-url="<?=  Url::to(['/common/badge/notice-count'])?>"
                              title="最新通知数"><i
                                    class="layui-anim layui-anim-rotate layui-anim-loop layui-icon layui-icon-loading"></i></span>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <!-- 主体内容-->
    <div class="layui-body" id="LAY_app_body" style="z-index: 0;">
        <div class="layadmin-tabsbody-item layui-show">
            <iframe src="<?= Url::to(['main']) ?>" frameborder="0" class="layadmin-iframe"></iframe>
        </div>
    </div>
</div>

<?php $this->beginBlock('js') ?>
<script>
    /**
     * 最新消息通知
     */
    function openRecentNews() {
        layui.layer.open({
            type: 2,
            id: 'LAY_adminPopupR',
            anim: -1,
            title: false,
            closeBtn: false,
            offset: 'r',
            shade: 0.1,
            shadeClose: true,
            skin: 'layui-anim layui-anim-rl layui-layer-adminRight layui-col-xs12 layui-col-sm6 layui-col-md4 layui-col-lg3',
            maxWidth: 450,
            content: "<?= Url::to(['/common/side/notice'])?>"
        });
    }

    /**
     * 兼容手机端尺寸，重计算body距离头部导航的偏移量
     */
    $(function () {
        $('#LAY_app_body').css('top', $('#LAY-app-nav').height());
    })

    $('#LAY-app-nav').resize(function () {
        $('#LAY_app_body').css('top', $('#LAY-app-nav').height());
    })

    /**
     * 点击菜单后重定向iframe body
     */
    $('a').click(function () {
        var url = $(this).attr('layui-href');
        if (url) {
            $('.layadmin-iframe').attr('src', url);
        }
    })

    /**
     * 点击隐藏菜单
     */
    $('#nav-area-btn').click(function () {
        $('#nav-area').toggleClass('hide');
        $(this).toggleClass('layui-icon-spread-left');
        $(this).toggleClass('layui-icon-shrink-right');
    })

    /**
     * 滑屏隐藏菜单栏，增加可视高度
     */
    // $(".layadmin-iframe").load(function () {
    //     var iframe = $(".layadmin-iframe").contents();
    //     $(iframe).bind('touchstart', function (e) {
    //         startX = e.originalEvent.changedTouches[0].pageX;
    //         startY = e.originalEvent.changedTouches[0].pageY;
    //     })
    //     $(iframe).bind('touchmove', function (e) {
    //         //获取滑动屏幕时的X,Y
    //         endX = e.originalEvent.changedTouches[0].pageX;
    //         endY = e.originalEvent.changedTouches[0].pageY;
    //         //获取滑动距离
    //         distanceX = endX - startX;
    //         distanceY = endY - startY;
    //         if (window_width < 992) {
    //             //判断滑动方向
    //             if (Math.abs(distanceX) > Math.abs(distanceY) && distanceX > 0) {
    //                 //向右滑动
    //             } else if (Math.abs(distanceX) > Math.abs(distanceY) && distanceX < 0) {
    //                 //向左滑动
    //             } else if (Math.abs(distanceX) < Math.abs(distanceY) && distanceY < 0) {
    //                 //向上滑动
    //                 $('#nav_area').hide();
    //             } else if (Math.abs(distanceX) < Math.abs(distanceY) && distanceY > 0) {
    //                 //向下滑动
    //                 $('#nav_area').show();
    //             } else {
    //                 //点击未滑动
    //             }
    //         }
    //     });
    // });

    /**
     * 每10秒拉取一次导航徽章状态
     */
    setInterval(function () {
        getBadge();
    }, 10000)

    getBadge();

    function getBadge() {
        var notice_count = 0;
        $('.layui-badge').each(function () {
            var that = $(this);
            $.ajax({
                type: "GET",
                url: that.data('url'),
                async: false,
                success: function (res) {
                    if (res.error == 1) {
                        that.html('?');
                    } else {
                        notice_count += res.data.count;
                        if (res.data.count > 99) {
                            that.html('99+');
                        } else {
                            that.html(res.data.count);
                        }
                    }
                }
            });
        })

        if (notice_count > 0) {
            document.title = '您有新的消息待确认';
        } else {
            document.title = '<?= Yii::$app->params['title']?>';
        }
    }
</script>
<?php $this->endBlock() ?>
