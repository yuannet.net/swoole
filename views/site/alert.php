<div class="error-page">
    <img class="error-page-img" src="<?= Yii::$app->params['imagePath'] ?>/<?= $code ?: '404' ?>.png">
    <div class="error-page-info">
        <!--        <h1>--><? //= $code ?><!--</h1>-->
        <h1 style="font-size:3em"><?= $msg ?></h1>
    </div>
</div>
