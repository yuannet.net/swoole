<?php
?>

<style>
    .layui-card-body {
        padding: 15px;
    }

    .produce-list li {
        height: 30px;
        line-height: 30px;
        padding-left: 10px;
        border-left: 3px solid rgba(30, 159, 255, 0);
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
        cursor: pointer;
    }

    .li_this {
        border-left: 3px solid #1E9FFF !important;
        background: #F0F0F0;
    }

    .product-detail-view {
        padding-top: 20px;
        text-align: center;
    }
</style>

<div class="layui-fluid layui-anim layui-anim-fadein layui-col-md-offset1 layui-col-md10">
    <div class="layui-row layui-col-space10">
        <!--左边栏-->
        <div class="layui-col-sm8">
            <!--概况-->
            <?= $this->render('plug/overview', ['product_overview' => $product_overview]) ?>

            <!--产品统计-->
            <?= $this->render('plug/prod_statistics', ['product_info' => $product_info]) ?>
        </div>

        <!--右边栏-->
        <div class="layui-col-sm4">
            <!--最新动态-->
            <?= \app\components\RecentNewsWidget::widget() ?>

            <!--备忘录-->
            <?= \app\components\MemoWidget::widget() ?>

            <!--我的需求-->
            <?= \app\components\MyDemandWidget::widget() ?>

            <!--我的Bug-->
            <?= \app\components\MyBugWidget::widget() ?>
        </div>
    </div>
</div>

<?php $this->beginBlock('js') ?>
<script>
    $('.produce-list li').mouseover(function () {
        console.log($(this).attr('for'));
        $(this).siblings().removeClass('li_this');
        $(this).addClass('li_this');
        $('#product_detail').find('.product-detail-view').hide();
        $('#product_detail').find('#' + $(this).attr('for')).show();
    })
</script>
<?php $this->endBlock() ?>
