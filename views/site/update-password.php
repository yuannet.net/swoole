<?php

?>

<div class="layui-fluid layui-anim layui-anim-fadein">
    <div class="layui-card">
        <div class="layui-card-header">
            <b>修改信息</b>
        </div>
        <div class="layui-card-body">
            <form class="layui-form" action="" method="post">
                <div class="layui-row">
                    <div class="layui-col-sm6">
                        <div class="layui-form-item">
                            <label class="layui-form-label must">姓名</label>
                            <div class="layui-input-block">
                                <input type="text" name="realname" lay-verify="required" placeholder="请输入姓名"
                                       autocomplete="off" class="layui-input" value="<?= $model->realname ?>">
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm6">
                        <div class="layui-form-item">
                            <label class="layui-form-label">手机号</label>
                            <div class="layui-input-block">
                                <input type="text" name="mobile" placeholder="请输入手机号"
                                       autocomplete="off" class="layui-input" value="<?= $model->mobile ?>">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="layui-row">
                    <div class="layui-col-sm6">
                        <div class="layui-form-item">
                            <label class="layui-form-label">QQ号</label>
                            <div class="layui-input-block">
                                <input type="text" name="qq" placeholder="请输入QQ号"
                                       autocomplete="off" class="layui-input" value="<?= $model->qq ?>">
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-sm6">
                        <div class="layui-form-item">
                            <label class="layui-form-label">微信号</label>
                            <div class="layui-input-block">
                                <input type="text" name="wechat" placeholder="请输入微信号"
                                       autocomplete="off" class="layui-input" value="<?= $model->wechat ?>">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">邮箱</label>
                    <div class="layui-input-block">
                        <input type="text" name="email" placeholder="请输入邮箱（邮箱用于任务指派通知）"
                               autocomplete="off" class="layui-input" value="<?= $model->email ?>">
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">住址</label>
                    <div class="layui-input-block">
                        <textarea name="address" class="layui-textarea"
                                  placeholder="请输入住址"><?= $model->address ?></textarea>
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">密码</label>
                    <div class="layui-input-block">
                        <input type="password" name="password_hash" placeholder="若为空则不修改密码"
                               autocomplete="off" class="layui-input">
                    </div>
                </div>

                <div class="layui-form-item">
                    <label class="layui-form-label">确认密码</label>
                    <div class="layui-input-block">
                        <input type="password" name="password_confirm" placeholder="若为空则不修改密码"
                               autocomplete="off" class="layui-input">
                    </div>
                </div>

                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <button class="layui-btn layui-btn-normal layui-btn-sm ajax-submit" lay-submit
                                lay-filter="submit" after-success="jump">立即提交
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php $this->beginBlock('js') ?>

<?php $this->endBlock() ?>
