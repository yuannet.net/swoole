<?php
$this->title = \Yii::$app->params['title'];
use yii\helpers\Url;

?>
    <link href="src/style/login.css" rel="stylesheet">
    <style>
        .layadmin-user-login {
            background: url('/images/background.jpg') no-repeat;
            background-size: cover;
        }

        @media screen and (max-width: 768px) {
            .layadmin-user-login-main {
                width: 300px;
                border: 0px;
                background: none;
            }
        }
    </style>

    <div class="layadmin-user-login layadmin-user-display-show">
        <div class="layadmin-user-login-main">
            <div class="layadmin-user-login-box layadmin-user-login-header">
                <h2>产品管理系统</h2>
                <p><?= \Yii::$app->params['title'] ?></p>
            </div>
            <form class="layui-form" action="<?= Url::to(['']) ?>" method="post">
                <div class="layadmin-user-login-box layadmin-user-login-body layui-form">
                    <div class="layui-form-item">
                        <label class="layadmin-user-login-icon layui-icon layui-icon-username"></label>
                        <input type="text" name="username" lay-verify="required" placeholder="用户名" class="layui-input">
                    </div>
                    <div class="layui-form-item">
                        <label class="layadmin-user-login-icon layui-icon layui-icon-password"
                               for="LAY-user-login-password"></label>
                        <input type="password" name="password" lay-verify="required" placeholder="密码"
                               class="layui-input">
                    </div>
                    <div class="layui-form-item">
                        <a href="javascript:;" class="layui-btn layui-btn-normal layui-btn-fluid ajax-submit" lay-submit
                           lay-filter="submit"
                           after-success="jump">登 入</a>
                    </div>
                </div>
            </form>
        </div>
    </div>

<?php $this->beginBlock('js') ?>
    <script type="text/javascript">
    </script>
<?php $this->endBlock() ?>