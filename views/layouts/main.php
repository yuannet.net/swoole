<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\assets\AppAsset;
use yii\helpers\Html;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<?= $content ?>
<?php $this->endBody() ?>
<script>
    var layuiConfig = layui.config({
        base: '/src/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        xmSelect: 'lib/extend/xm-select',
        treeTable: 'lib/extend/treeTable',
    }).use(['index']);
</script>
<?= $this->blocks['js'] ?>
</body>
</html>
<?php $this->endPage() ?>
