<?php

?>

<div class="layui-card">
    <div class="layui-card-header">
        <b>我的备忘录</b>
        <div class="layui-inline" style="float: right">
            <a href="javascript:void(0)" class="layui-btn layui-btn-xs layui-btn-primary iframe-layer"
               data-title="我的待办Bug"
               data-url="<?= \yii\helpers\Url::to(['/mzone/memo/list']) ?>">MORE</a>
        </div>
    </div>
    <div class="layui-card-body">
        <div class="layui-row">
            <input type="text" class="layui-input" id="new_memo_input">
            <div class="layui-col-xs12 layui-bg-white layui-hide layui-anim layui-anim-fadein" id="new_memo_panel"
                 style="position: relative;top:-30px;z-index: 1000000">
                <form action="<?= \yii\helpers\Url::to(['/mzone/memo/create']) ?>" class="layui-form" method="post"
                      id="memo_form">
                    <div class="layui-form-item">
                        <label class="layui-form-label must">备忘名称</label>
                        <div class="layui-input-block">
                            <input type="text" class="layui-input" id="memo_title" name="title" lay-verify="required">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">描述</label>
                        <div class="layui-input-block">
                            <textarea class="layui-textarea" name="remark"></textarea>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">优先级</label>
                        <div class="layui-input-block">
                            <select name="level" id="">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3" selected>3</option>
                                <option value="4">4</option>
                            </select>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">时间</label>
                        <div class="layui-input-block">
                            <input type="text" class="layui-input auto-date" name="plan_date" date-type="datetime"
                                   readonly>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">附件</label>
                        <div class="layui-input-block">
                            <a href="javascript:void(0)"
                               class="layui-btn layui-btn-sm layui-btn-primary upload-file-btn"
                               data-name="attachment">
                                <i class="layui-icon layui-icon-addition"></i>添加文件
                            </a>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <div class="layui-block">
                                <button class="layui-btn layui-btn-normal layui-btn-md ajax-submit" after-success="call"
                                        lay-submit>保存
                                </button>
                                <a href="javascript:void(0)" class="layui-btn layui-btn-primary layui-btn-md"
                                   id="cls_new_memo"
                                   lay-filter="submit">关闭
                                </a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <?php if ($model): ?>
            <table class="layui-table" lay-even lay-skin="nob" lay-size="sm">
                <colgroup>
                    <col width="50">
                    <col>
                    <col width="105">
                </colgroup>
                <thead>
                <tr>
                    <td>L</td>
                    <td>名称</td>
                    <td>计划时间</td>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($model as $key => $val): ?>
                    <tr>
                        <td>
                            <a href="javascript:void(0)" class="layui-btn layui-btn-xs <?= $val->levelColor ?>"
                               style="border-radius: 20px;padding: 0px 8px;font-weight: bold;">
                                <?= $val->level ?>
                            </a>
                        </td>
                        <td title="<?= $val->remark ?>">
                            <a href="javascript:void(0)" class="iframe-layer" data-title="备忘录"
                               data-url="<?= \yii\helpers\Url::to(['/mzone/memo/view','id'=>$val->id]) ?>"><?= $val->title ?>
                            </a>
                        </td>
                        <td><?= date('m月d日 H:i', $val->plan_date) ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        <?php endif; ?>
    </div>
</div>

<script>
    window.onload = function () {
        $('#new_memo_input').click(function () {
            $('#new_memo_panel').toggleClass('layui-hide')
            $('#memo_title').focus();
        })

        $('#cls_new_memo').click(function () {
            $('#new_memo_panel').toggleClass('layui-hide')
        })
    }

    function submitCall() {
        $('#memo_form')[0].reset();
        $('#memo_form').find('.upload-file-item').remove();
        $('#memo_form').find('.ajax-submit').removeClass('layui-btn-disabled');
        $('#memo_form').find('.ajax-submit').removeAttr('disabled');
        $('#new_memo_panel').toggleClass('layui-hide')
    }
</script>

