<?php

?>

<div class="layui-card">
    <div class="layui-card-header">
        <b>我的待办Bug</b>
        <div class="layui-inline" style="float: right">
            <a href="javascript:void(0)" class="layui-btn layui-btn-xs layui-btn-primary iframe-layer" data-title="我的待办Bug"
               data-url="<?= \yii\helpers\Url::to(['/test/bug/list', 'search_type' => 4]) ?>">MORE</a>
        </div>
    </div>
    <div class="layui-card-body">
        <?php if ($model): ?>
            <table class="layui-table" lay-even lay-skin="nob" lay-size="sm">
                <colgroup>
                    <col width="50">
                    <col>
                    <col width="100">
                    <col width="105">
                </colgroup>
                <thead>
                <tr>
                    <td>L</td>
                    <td>名称</td>
                    <td>产品</td>
                    <td>创建时间</td>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($model as $key => $val): ?>
                    <tr>
                        <td>
                            <a href="javascript:void(0)" class="layui-btn layui-btn-xs <?= $val->levelColor ?>"
                               style="border-radius: 20px;padding: 0px 8px;font-weight: bold;">
                                <?= $val->level ?>
                            </a>
                        </td>
                        <td title="<?= $val->title ?>">
                            <a href="javascript:void(0)" class="iframe-layer" data-title="详情"
                               data-url="<?= \yii\helpers\Url::to(['/product/demand/view', 'id' => $val->id]) ?>">
                                <?= $val->title ?>
                            </a>
                        </td>
                        <td title="<?= $val->product->title ?>">
                            <?= $val->product->title ?>
                        </td>
                        <td><?= date('m月d日 H:i', $val->created_at) ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        <?php else: ?>
            <div style="text-align: center">暂无待办Bug...</div>
        <?php endif; ?>
    </div>
</div>


<?php $this->beginBlock('js') ?>
<?php $this->endBlock() ?>
