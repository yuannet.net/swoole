<?php

?>

<div class="layui-card">
    <div class="layui-card-header">
        <b>最新动态</b>
        <div class="layui-inline" style="float: right">
            <a href="javascript:void(0)" class="layui-btn layui-btn-xs layui-btn-primary iframe-layer" data-title="最新动态"
               data-url="<?= \yii\helpers\Url::to(['/mzone/recent-news/list']) ?>">MORE</a>
        </div>
    </div>
    <div class="layui-card-body" style="max-height:140px;overflow-y: auto" id="recent-news-panel">
        <?php if ($model): ?>
            <table class="layui-table" lay-even lay-skin="nob" lay-size="sm">
                <colgroup>
                    <col>
                    <col width="105">
                </colgroup>
                <thead>
                <tr>
                    <td>描述</td>
                    <td>动态时间</td>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($model as $key => $val): ?>
                    <tr>
                        <td title="<?= $val->createUser->name ?> <?= $val->remark ?>">
                            <?php if ($val->url): ?>
                                <a href="javascript:void(0)" class="iframe-layer" style="color: #01AAED" data-title="最新动态"
                                   data-url="<?= $val->url ?>">
                                    <?= $val->createUser->name ?> <?= $val->remark ?>
                                </a>
                            <?php else: ?>
                                <span>
                                <?= $val->createUser->name ?> <?= $val->remark ?>
                                </span>
                            <?php endif; ?>
                        </td>
                        <td><?= date('m月d日 H:i', $val->created_at) ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        <?php else: ?>
            <div style="text-align: center">暂无动态...</div>
        <?php endif; ?>
    </div>
</div>
</script>
