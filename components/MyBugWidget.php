<?php

namespace app\components;


use app\services\Bug;
use yii\base\Widget;

/**
 * 我的Bug小组件
 * Class ProcessWidget
 * @package app\components
 */
class MyBugWidget extends Widget
{

    //拉取数据条数
    public $limit = 1000;

    public function run()
    {
        $model = Bug::find()->where(['develop_user_id' => \Yii::$app->user->identity->id, 'status' => 10, 'step' => [1, 2]])->orderBy('id desc')->limit($this->limit)->all();
        return $this->render('my-bug/index', ['model' => $model]);
    }
}