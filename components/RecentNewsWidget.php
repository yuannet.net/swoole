<?php

namespace app\components;

use app\services\OperationLog;
use yii\base\Widget;

/**
 * 最新动态小组件
 * Class ProcessWidget
 * @package app\components
 */
class RecentNewsWidget extends Widget
{
    //最近天数 0为当天
    public $recent_days = 0;
    //拉取数据条数
    public $limit = 1000;

    public function run()
    {
        $begin_time = strtotime(date('Y-m-d', strtotime("-{$this->recent_days} day")));
        $model = OperationLog::find()->where(['is_show' => 1, 'status' => 10])->andWhere(['>=', 'created_at', $begin_time])->orderBy('id desc')->limit($this->limit)->all();
        return $this->render('recent-news/index', ['model' => $model]);
    }
}