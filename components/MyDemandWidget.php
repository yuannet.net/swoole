<?php

namespace app\components;


use app\services\Demand;
use yii\base\Widget;

/**
 * 我的需求小组件
 * Class ProcessWidget
 * @package app\components
 */
class MyDemandWidget extends Widget
{

    //拉取数据条数
    public $limit = 1000;

    public function run()
    {
        $model = Demand::find()->where(['develop_user_id' => \Yii::$app->user->identity->id, 'status' => 10, 'step' => [1, 2]])->orderBy('id desc')->limit($this->limit)->all();
        return $this->render('my-demand/index', ['model' => $model]);
    }
}