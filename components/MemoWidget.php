<?php

namespace app\components;

use yii\base\Widget;

/**
 * 我的备忘录小组件
 * Class ProcessWidget
 * @package app\components
 */
class MemoWidget extends Widget
{

    //拉取数据条数
    public $limit = 1000;

    public function run()
    {
        $model = \app\services\Memo::find()->where(['created_by' => \Yii::$app->user->identity->id, 'status' => 10])->andWhere(['>=', 'plan_date', time()])->orderBy('plan_date asc')->limit($this->limit)->all();
        return $this->render('memo/index', ['model' => $model]);
    }
}